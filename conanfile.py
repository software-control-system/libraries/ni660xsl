﻿from conan import ConanFile

class ni660xslRecipe(ConanFile):
    name = "ni660xsl"
    version = "1.5.2"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"
    
    license = "GPL-2"
    author = "Florent Langlois"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/libraries/ni660xsl"
    description = "NI660Xsl support library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**", "include/**"

    def requirements(self):
        self.requires("nidaqmx/8.7.1@soleil/stable")
        self.requires("ace/[>=1.0]@soleil/stable")
