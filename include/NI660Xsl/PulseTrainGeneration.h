// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    PulseTrainGeneration.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

#ifndef _PULSE_TRAIN_GENERATION_H_
#define _PULSE_TRAIN_GENERATION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>

#include <NI660Xsl/OutputOperation.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
//!  ni660XSL base class for pulse train generation.
// ============================================================================
//!  
//! 
//! 
// ============================================================================
    class NI660XSL_EXPORT PulseTrainGeneration : public OutputOperation
{

public:
    PulseTrainGeneration(void);
    virtual ~PulseTrainGeneration(void);
    /**
    * Set a start trigger.
    * @param _trigger_source The source of the trigger.
    * @param _active_edge The active edge of the trigger.
    */
    void set_start_trigger(std::string _trigger_source, ni::EdgeType _active_edge) throw (ni660Xsl::DAQException);
    
     /**
     * Configure hardware with all predefine parameters
     */
     virtual void configure (void) = 0;

protected:
    ni::TriggerType trig_mode_;
    std::string start_trigger_source_;
    ni::EdgeType active_edge_;
};

}//namespace

#endif // !defined(_PULSE_TRAIN_GENERATION_H_)
