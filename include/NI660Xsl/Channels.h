// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   Channels.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================



#ifndef _CHANNELS_H_
#define _CHANNELS_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>

#include <NI660Xsl/Export.h>
#include <NI660Xsl/NI.h>


namespace ni660Xsl
{
// ============================================================================
//! The ni660XSL channel base class. 
// ============================================================================
//!  
//! Base class for all channels of counter based operations.
//! 
// ============================================================================
    class NI660XSL_EXPORT Chan
    {
    public:
        /**
        * A string indicating the board and the counter used. ex: "/Dev1/ctr0"
        */
        std::string chan_name;
        Chan()
            :chan_name("/Dev1/ctr0")
        {};
        /**
        * copy ctr
        */
        Chan(const Chan& _src)
            :chan_name( _src.chan_name)
        {};
        virtual ~Chan(void){};
    };
// ============================================================================
//!  The ni660XSL output channel class. 
// ============================================================================
//!  
//! Base class for all output channels used for output operations.
//! 
// ============================================================================
    class NI660XSL_EXPORT OutChan :  public Chan
    {
    public:	
        /**
        * The resting state of the output signal (high or low).
        */
        ni::LevelType idle_state;
        /**
        * The ouput terminal (only used to defined a different one from the default one).
        */
        std::string output_terminal;
        
        OutChan()
            :Chan(),
            idle_state (ni::low),
            output_terminal("")
        {};
        /**
        * copy ctr
        */
        OutChan(const OutChan& _src)
            :Chan(_src),
            idle_state (_src.idle_state),
            output_terminal(_src.output_terminal)
        {};
        virtual ~OutChan(void){};
    };
// ============================================================================
//!  The ni660XSL output frequency channel.
// ============================================================================
//!  
//! Class to configure an output channel with frequency parameters.
//! 
// ============================================================================
    class NI660XSL_EXPORT OutFreqChan : public OutChan
    {
    public:
        /**
        * Initial delay in seconds (in the retriggerable case the initial delay will only be used the first time).
        */
        double initial_delay;
        /**
        * Frequency in Hertz.
        */
        double frequency;
        /**
        * The duty cycle. 
        */
        double duty_cycle;
        
        OutFreqChan(void)
            : OutChan(),
            initial_delay(0.0),
            frequency(10.0),
            duty_cycle(0.5)
        {/*noop*/};
        /**
        * copy ctr
        */
        OutFreqChan(const OutFreqChan& _src)
            : OutChan(_src),
            initial_delay(_src.initial_delay),
            frequency(_src.frequency),
            duty_cycle(_src.duty_cycle)
        {};
        /**
        * operator =.
        */
        OutFreqChan& operator=(const OutFreqChan& _src)
        {
            if (&_src == this) 
                return *this;
            chan_name = _src.chan_name;
            idle_state = _src.idle_state;
            initial_delay = _src.initial_delay;
            frequency = _src.frequency;
            duty_cycle = _src.duty_cycle;
            return *this;
            
        };
        /**
        * Destructor.
        */
        virtual ~OutFreqChan (void) {/*noop*/};
        
    };
// ============================================================================
//!  The ni660XSL output time channel.
// ============================================================================
//!  
//! Class to configure an output channel with time parameters.
//! 
// ============================================================================
    class NI660XSL_EXPORT OutTimeChan : public OutChan
    {
    public:
        /**
        * Initial delay in seconds (in the retriggerable case the initial delay will only be used the first time).
        */
        double initial_delay;
        /**
        * Time in seconds when the ouput signal is low.
        */
        double low_time;
        /**
        * Time in seconds when the ouput signal is high.
        */
        double high_time;
        /**
        *
        */
        OutTimeChan(void)
            : OutChan(),
            initial_delay(0.0),
            low_time(0.5),
            high_time(0.5)
        {};
        /**
        * copy ctr
        */
        OutTimeChan(const OutTimeChan& _src)
            :OutChan(_src),
            initial_delay(_src.initial_delay),
            low_time(_src.low_time),
            high_time(_src.high_time)
        {};
        /**
        * operator =.
        */
        OutTimeChan& operator=(const OutTimeChan& _src)
        {
            if (&_src == this) 
                return *this;
            chan_name = _src.chan_name;
            idle_state = _src.idle_state;
            initial_delay = _src.initial_delay;
            low_time = _src.low_time;
            high_time = _src.high_time;
            return *this;
            
        };
        /**
        * Destructor.
        */
        virtual ~OutTimeChan (void) {/*noop*/};
        
    };
// ============================================================================
//! The ni660XSL output clock ticks channel.
// ============================================================================
//!  
//! Class to configure an output channel with parameters specified in clock ticks.
//! 
// ============================================================================
    class NI660XSL_EXPORT OutClockTicksChan: public OutChan
    {
    public:
        /**
        * The pin where is connected the sample clock.
        */
        std::string clk_source;     
        /**
        * Initial delay in seconds (in the retriggerable case the initial delay will only be used the first time).
        */
        long initial_delay;
        /**
        * Number of clock ticks when the ouput signal is low.
        */
        long low_ticks;
        /**
        * Number of clock ticks when the ouput signal is high.
        */
        long high_ticks;
        /**
        *
        */
        OutClockTicksChan(void)
            : OutChan(),
            clk_source("/Dev1/PFI39"),
            initial_delay(0),
            low_ticks(10),
            high_ticks(5)
        {/*noop*/};
        /**
        * copy ctr
        */
        OutClockTicksChan(const OutClockTicksChan& _src)
            :OutChan(_src),
            clk_source (_src.clk_source),
            initial_delay(_src.initial_delay),
            low_ticks(_src.low_ticks),
            high_ticks(_src.high_ticks)
        {};
        /**
        * operator =.
        */
        OutClockTicksChan& operator=(const OutClockTicksChan& _src)
        {
            if (&_src == this) 
                return *this;
            chan_name = _src.chan_name;
            clk_source = _src.clk_source;
            idle_state = _src.idle_state;
            initial_delay = _src.initial_delay;
            low_ticks = _src.low_ticks;
            high_ticks = _src.high_ticks;
            return *this;
            
        };
        /**
        * Destructor.
        */
        virtual ~OutClockTicksChan (void) {/*noop*/};
        
    };
// ============================================================================
//!  The ni660XSL input channel class. 
// ============================================================================
//!  
//! Base class for all input channels used for input operations.
//! 
// ============================================================================
    class NI660XSL_EXPORT InChan :  public Chan
    {        
    public:
        InChan(void)
            :Chan()      
        {};
        /**
        * copy ctr
        */
        InChan(const InChan& _src)
            :Chan(_src)
        {};      
        virtual ~InChan(void){};
    };
// ============================================================================
//! The ni660XSL event counting channel.
// ============================================================================
//!  
//! Class to configure an intput channel with parameters for event counting.
//! @see SimpleEventCounting and BufferedEventCounting.
// ============================================================================
    class NI660XSL_EXPORT EventCountChan : public InChan
    {
    public:
        /**
        * On which edge to increment or decrement counter.
        */
        ni::EdgeType edge;
        /**
        * The value from which to start counting.
        */
        long initial_count;
        /**
        * The count direction.
        */
        ni::DirectionType count_direction;
        /**
        * The input terminal (in case of a different input from the default one).
        */
        std::string input_terminal;
       
        
        EventCountChan ()
            :InChan(),
            edge(ni::rising_edge),
            initial_count(0),
            count_direction(ni::count_up),
            input_terminal("")
        {};
        EventCountChan(const EventCountChan& _src)
            :InChan(_src),
            edge(_src.edge),
            initial_count(_src.initial_count),
            count_direction(_src.count_direction),
            input_terminal(_src.input_terminal)
        {};
        /**
        * operator =.
        */
        EventCountChan& operator=(const EventCountChan& _src)
        {
            if (&_src == this) 
                return *this;
            chan_name = _src.chan_name;
            input_terminal = _src.input_terminal;
            edge = _src.edge;
            initial_count = _src.initial_count;
            count_direction = _src.count_direction;
            return *this;
            
        };
        virtual ~EventCountChan(void){};
    };
// ============================================================================
//! The ni660XSL channel base class for measure of motor position. 
// ============================================================================
//!  
//! Base class for all input channels used for measuring motor position.
//! 
// ============================================================================
    class NI660XSL_EXPORT PositionChan : public InChan
    {
    public:
        /**
        * The decoding type.
        */
        ni::DecodingType decoding_type;
        /**
        * Enable the z indexing for the measurement.
        */
        bool z_idx_enable;
        /**
        * The value in units, to which to reset the measurement
		* when Z is high and A and B are in state specified with z_idx_phase.
        */
        double z_idx_val;
        /**
		* Possible states of channel A and B for Z indexing.
		*/
        ni::PhaseType z_idx_phase;  

        PositionChan ()
            :InChan(),
            decoding_type(ni::x1),
            z_idx_enable(false),
            z_idx_val(10),
            z_idx_phase(ni::AHigh_BHigh)
        {};
        PositionChan(const PositionChan& _src)
            :InChan(_src),
            decoding_type(_src.decoding_type),
            z_idx_enable(_src.z_idx_enable),
            z_idx_val(_src.z_idx_val),
            z_idx_phase(_src.z_idx_phase)
        {};
        virtual ~PositionChan(void){};
    };
// ============================================================================
//! The ni660XSL channel class for measuring motor position. 
// ============================================================================
//!  
//! Channel used for measuring motor position with angular encoder.
//! @see SimplePositionMeasurement and BufferedPositionMeasurement
// ============================================================================
    class NI660XSL_EXPORT AngularEncoderChan : public PositionChan
    {
    public:
    /**
    * The unit used.
        */
        ni::AngleUnitsType units;
        /**
        * The number of pulses the encoder generates per revolution (from either A or B signal). value in units.
        */
        unsigned long pulse_per_revolution;
        /**
        * The position of the encoder when the measurement begins.
        */
        double initial_angle;
        
        AngularEncoderChan ()
            :PositionChan(),
            units(ni::degrees),
            pulse_per_revolution(27),
            initial_angle(0)
        {
        };
        AngularEncoderChan(const AngularEncoderChan& _src)
            :PositionChan(_src),
            units(_src.units),
            pulse_per_revolution(_src.pulse_per_revolution),
            initial_angle(_src.initial_angle)
        {};
        /**
        * operator =.
        */
        AngularEncoderChan& operator=(const AngularEncoderChan& _src)
        {
            if (&_src == this) 
                return *this;
            chan_name = _src.chan_name;
            units = _src.units;
            pulse_per_revolution = _src.pulse_per_revolution;
            initial_angle = _src.initial_angle;
            return *this;
            
        };
        virtual ~AngularEncoderChan(void){};
    };
   
// ============================================================================
//! The ni660XSL channel class for measuring motor position. 
// ============================================================================
//!  
//! Channel used for measuring motor position with linear encoder.
//! @see SimplePositionMeasurement and BufferedPositionMeasurement
// ============================================================================
    class NI660XSL_EXPORT LinearEncoderChan : public PositionChan
    {
    public:
        /**
        * The unit used.
        */
        ni::DistanceUnitsType units;
        /**
        * The distance measured for each pulse the encoder generates. value in units.
        */
        double distance_per_pulse;
        /**
        * The position of the encoder when the measurement begins.
        */
        double initial_position;
        
        LinearEncoderChan ()
            :PositionChan(),
            units(ni::meters),
            distance_per_pulse(1),
            initial_position(0)
        {};
        LinearEncoderChan(const LinearEncoderChan& _src)
            :PositionChan(_src),
            units(_src.units),
            distance_per_pulse(_src.distance_per_pulse),
            initial_position(_src.initial_position)
        {};
        /**
        * operator =.
        */
        LinearEncoderChan& operator=(const LinearEncoderChan& _src)
        {
            if (&_src == this) 
                return *this;
            chan_name = _src.chan_name;
            units = _src.units;
            distance_per_pulse = _src.distance_per_pulse;
            initial_position = _src.initial_position;
            return *this;
            
        };
        virtual ~LinearEncoderChan(void){};
    };
    

// ============================================================================
//! The ni660XSL delta time channel.
// ============================================================================
//!  
//! Class to configure an intput channel with parameters for delta time measurement.
//! @see SimpleDTMeasurement and BufferedDTMeasurement.
// ============================================================================
    class NI660XSL_EXPORT DeltaTimeChan : public InChan
    {
    public:
        /**
        *  On which edge of the first signal to start each measurement.
        */
        ni::EdgeType first_edge;
        /**
        *  On which edge of the first signal to stop each measurement.
        */
        ni::EdgeType second_edge;
        /**
        * The unit used.
        */
        ni::TimeUnitsType units;
        /**
        * The input terminal (in case of a different input from the default one).
        */
        std::string input_terminal;
       
        
        DeltaTimeChan ()
            :InChan(),
            first_edge(ni::rising_edge),
            second_edge(ni::falling_edge),
            units(ni::seconds),
            input_terminal("")
        {};
        DeltaTimeChan(const DeltaTimeChan& _src)
            :InChan(_src),
            first_edge(_src.first_edge),
            second_edge(_src.second_edge),
            units(_src.units),
            input_terminal(_src.input_terminal)
        {};
        /**
        * operator =.
        */
        DeltaTimeChan& operator=(const DeltaTimeChan& _src)
        {
            if (&_src == this) 
                return *this;
            chan_name = _src.chan_name;
            input_terminal = _src.input_terminal;
            first_edge = _src.first_edge;
            second_edge = _src.second_edge;
            units = _src.units;
            return *this;
        };
        virtual ~DeltaTimeChan(void){};
    };

// ============================================================================
//! The ni660XSL period channel.
// ============================================================================
//!  
//! Class to configure an intput channel with parameters for signal period measurement.
//! @see SimplePeriodMeasurement and BufferedPeriodMeasurement.
// ============================================================================
    class NI660XSL_EXPORT PeriodChan : public InChan
    {
    public:
        /**
        *  On which edge of the signal to start each measurement.
        */
        ni::EdgeType edge;
        /**
        * The unit used.
        */
        ni::TimeUnitsType units;
        /**
        * The input terminal (in case of a different input from the default one).
        */
        std::string input_terminal;
       
        
        PeriodChan ()
            :InChan(),
            edge(ni::rising_edge),
            units(ni::seconds),
            input_terminal("")
        {};
        PeriodChan(const PeriodChan& _src)
            :InChan(_src),
            edge(_src.edge),
            units(_src.units),
            input_terminal(_src.input_terminal)
        {};
        /**
        * operator =.
        */
        PeriodChan& operator=(const PeriodChan& _src)
        {
            if (&_src == this) 
                return *this;
            chan_name = _src.chan_name;
            input_terminal = _src.input_terminal;
            edge = _src.edge;
            units = _src.units;
            return *this; 
        };
        virtual ~PeriodChan(void){};
    };

}//namespace ni660Xsl
#endif //#ifndef _CHANNELS_H_
