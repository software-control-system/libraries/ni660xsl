// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   BufferedPositionMeasurement.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================


#ifndef _BUFFERED_POSITION_MEASUREMENT_H_
#define _BUFFERED_POSITION_MEASUREMENT_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/PositionMeasurement.h>
#include <NI660Xsl/Exception.h>
#include <NI660Xsl/InData.h>
#include <NI660Xsl/BufferedAcquisition.h>
#include <NI660Xsl/PulsedTask.h>

namespace ni660Xsl
{

  class TimeoutCallerPos;

// ============================================================================
//!  Perform buffered position measurement.
// ============================================================================
//!  
//! Buffered position measurement is similar to simple position measument (SimplePositionMeasurement)
//! except that the counter values are saved in a buffer. 
//! The rate at which data is saved is determined by a sample clock.
//! 
// ============================================================================   
    class NI660XSL_EXPORT BufferedPositionMeasurement:  public PositionMeasurement, 
                                                        public BufferedAcquisition
    {
      public:
        BufferedPositionMeasurement();
        virtual ~BufferedPositionMeasurement(); 

        /**
        * Configure hardware with all predefined parameters.
        */
        virtual void configure (void) 
          throw (ni660Xsl::DAQException);

        /**
        * This function must be implemented by user. 
        * Will be called each time a overrun occurred.
        */
        virtual void handle_data_lost(void) = 0;

        /**
        * This function must be implemented by user. 
        * Will be called each time a timeout occurred.
        */
        virtual void handle_timeout(void) = 0;

        /**
        * This function must be implemented by user. 
        * Will be called each time a raw buffer has been received.
        * <b>Don't forget to delete received buffer!!!!!!!!!</b>
        * @param buffer The received buffer.
        * @param _samples_read The actual number of samples read from each channel(returned value).
        */
        virtual void handle_raw_buffer(ni660Xsl::InRawBuffer* buffer, long& _samples_read) = 0;

        /**
        * This function must be implemented by user. 
        * Will be called each time a scaled buffer has been received.
        * <b>Don't forget to delete received buffer!!!!!!!!!</b>
        * @param buffer The received buffer.
        * @param _samples_read The actual number of samples read from each channel(returned value).
        */
        virtual void handle_scaled_buffer(ni660Xsl::InScaledBuffer* buffer, long& _samples_read) = 0;

        //- Redefine start, stop & abort functions for timeout management in callback mode
			  /**
			  * Start the operation.
			  */
			  void start (void) throw (ni660Xsl::DAQException);
  			
			  /**
			  * Stop the operation.
			  */
			  void stop (void) throw (ni660Xsl::DAQException);
			  /**
			  * Abort the operation.
			  */
			  void abort (void) throw (ni660Xsl::DAQException);

			  /**
			  * Abort the operation & release counter.
			  */
			  void abort_and_release (void) throw (ni660Xsl::DAQException);

        // Timeout stuff - consider this member as private one. Do not call directly.
        // Timeout callback, for callback mode only
        void handle_tmo_cb(void);

        // timeout caller
        TimeoutCallerPos * tmo_caller_;

		// stop flag (to avoid race condition between stop & callback in buffered mode)
		bool stop_flag_;
		
		// Mutex to protect stop flag
		ACE_Thread_Mutex stop_lock_;
    };

// ============================================================================
// TimeoutCallerPos : a ni660Xsl::PulsedTask to manage timeout function in 
// callback mode, for PositionMeasurement class
// ============================================================================
class TimeoutCallerPos : public PulsedTask
{
public:

  TimeoutCallerPos (void) 
  {
    /*noop*/
  };

  virtual ~TimeoutCallerPos () 
  {
    /*noop*/
  };

  virtual int pulsed (void* arg) 
  {
    BufferedPositionMeasurement * acq = reinterpret_cast<BufferedPositionMeasurement *>(arg);
    if (acq)
      acq->handle_tmo_cb();
    return 0;
  }
};

}//namespace

#endif // !defined(_BUFFERED_POSITION_MEASUREMENT_H_)
