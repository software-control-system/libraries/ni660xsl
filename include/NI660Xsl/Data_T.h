// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    Data_T.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _DATA_T_H_
#define _DATA_T_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/Data.h>
// ============================================================================
// FORWARD DECLARATIONS
// ============================================================================
class ACE_Allocator;

namespace ni660Xsl {

// ============================================================================
//! The ni660XSL generic container.  
// ============================================================================
//!  
//! This class provides a way to encapsulate any type of data into a Message.
//! 
// ============================================================================
template <class T> 
class Container : public Data
{
public:

  /**
   * Constructor.
   * @param _data the data to encapsulate.
   */
  Container (T& _data);
  
  /**
   * Destructor.
   */
  virtual ~Container(void);

  /**
   * Return the size of the encapsulated data in bytes.
   * @return size of the encapsulated data in bytes
   */
  virtual size_t size(void) const;

  /**
   * Return the container's content.
   * @return a reference to the container's content
   */
  T& content(void)
      {
  return this->data_;
};

private:

  /**
   * the encapsulated data.
   */
  T& data_;
};
// ============================================================================
//! The ni660XSL buffer abstraction.  
// ============================================================================
//!  
//! This template class provides a buffer abstraction. 
//! <operator=> must be defined for template parameter T.
//! 
// ============================================================================
template <class T> 
class Buffer : public Data
{
public:

  /**
   * Constructor. 
   * @param  depth the maximum number of element of type T 
   *         that can be stored into the buffer 
   * @param  alloc an ACE allocator, use default ACE_Allocator::instance 
   *        otherwise
   */
 Buffer (unsigned long depth, ACE_Allocator *alloc = 0);

    
 
  /**
   * Memory copy constructor. Memory is copied from _base to _base + _depth.
   * @param  depth the maximum number of element of type T 
   *         that can be stored into the buffer. 
   * @param  base address of the block to copy.
   * @param  alloc an ACE allocator, use default ACE_Allocator::instance. 
   *         otherwise
   */
 Buffer (unsigned long depth, T *base, ACE_Allocator *alloc = 0);

  /**
   * Copy constructor. Use allocator associated with the source buffer.
   * @param  buf the source buffer.
   */
  Buffer (const Buffer<T> &buf, ACE_Allocator *_alloc);

  /**
   * Destructor. Release resources.
   */
  virtual ~Buffer (void);

  /**
   * operator= 
   */
  Buffer<T>& operator= (const Buffer<T> &src)
  {
      if (&src == this)
          return *this;
      
      u_long cpy_depth = (src.depth() < this->depth_) ? src.depth() : this->depth_; 
      
      memcpy(this->base_, src.base(), cpy_depth * sizeof(T));
      
      return *this;
  };

  /**
   * operator=. Memory is copied from base to base + Buffer::depth_. 
   * @param base address of the block to copy.
   */
  Buffer<T>& operator= (const T *base)
  {
      if (base == this->base_)
          return *this;
      
      memcpy(this->base_, _src, this->depth_ * sizeof(T));
      return *this;
  };

    /**
    * operator=. Fill the buffer with a specified value.
    * @param val the value.
    */
  Buffer<T>& operator= (const T &val)
  {
      for (int i=0; i < this->depth_; ++i) {
          *(this->base_ + i) = val;
      }
      return *this;
  };
   
  /**
   * Fills the buffer with a specified value.
   * @param val the value.
   */
  void fill (const T& val)
  {
    *this = val;
  };
  
  /**
   * Clears buffer's content. This is a low level clear: set memory
   * from Buffer::base_ to Buffer::base_ + Buffer::depth_ to 0.
   */
  void clear (void)
  {
    memset(this->base(), 0, this->size());
  };

  /**
   * Returns a reference to the _ith element. No bound error checking.
   * @param i index of the element to return.
   * @return a reference to the ith element.
   */
  T& operator[] (unsigned long i)
  {
    /* !! no bound error check !!*/
    return this->base_[i];
  };

  /**
   * Returns a copy of the _ith element. No bound error checking.
   * @param i index of the element to return.
   * @return a copy of to the _ith element.
   */
  T operator[] (unsigned long i) const
  {
      /* !! no bound error check !!*/
      return this->base_[i];
  };

  /**
   * Returns the size of each element in bytes.
   * @return sizeof(T).
   */
  size_t elem_size (void) const
  {
      return sizeof(T);
  };

  /**
   * Returns the actual size of the buffer in bytes. 
   * @return the buffer size in bytes.
   */
  virtual size_t size (void) const
  {
      return this->depth_ * sizeof(T);
  };

  /**
   * Returns the maximum number of element that can be stored into 
   * the buffer. 
   * @return the buffer depth. 
   */
  unsigned long depth (void) const
  {
      return this->depth_;
  };
  
  /**
   * Returns the buffer base address. 
   * @return the buffer base address. 
   */
  T * base (void) const
  {
      return this->base_;
  };
  
  /**
   * Returns the buffer allocator. 
   * @return the buffer allocator. 
   */
  
  ACE_Allocator * allocator (void) const
  {
      return this->alloc_;
  };

private:

  /**
   * the buffer base address. 
   */
  T * base_;

  /**
   * maximum number of element of type T.
   */
  unsigned long depth_;

  /**
   * underlying allocator. 
   */
 ACE_Allocator * alloc_;
};

} // namespace ni660Xsl


#include "Data_T.cpp"

/*
#if defined (NI660XSL_BUILD)
# pragma implementation ("../src/Data_T.cpp")
#else
# pragma implementation ("../src/Data_T.cpp")
#endif
*/

#endif // _DATA_T_H_

