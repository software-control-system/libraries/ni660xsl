// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    NI.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

#ifndef _NI_H_
#define _NI_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>
//include the driver file
#include <NIDAQmx.h>


// ============================================================================
//!  Namespace for all parameters of the NIDAQmx driver.
// ============================================================================
namespace ni{
    
/**
* A handle to a DAQmx task.
*/
    typedef TaskHandle DAQTaskHandle;
    
/**
* Logic levels.
*/
    typedef enum
    {
        /**
        * High logic level.
        */
        high = DAQmx_Val_High,
        /**
        * Low logic level.
        */
        low = DAQmx_Val_Low
    }LevelType;
    
/**
* Units for time specification.
*/
    typedef enum
    {
        /**
        * To use hetz. 
        */
        hertz = DAQmx_Val_Hz,
        /**
        * To use seconds.
        */
        seconds = DAQmx_Val_Seconds,
        /**
        * To use ticks.
        */
        time_ticks = DAQmx_Val_Ticks
    }TimeUnitsType;
    
/**
* Units for distance specification.
*/
    typedef enum
    {
        /**
        * To use meters.
        */
        meters = DAQmx_Val_Meters,
        /**
        * To use inches.
        */
        inches = DAQmx_Val_Inches,
        /**
        * To use timebase ticks.
        */
        dist_ticks = DAQmx_Val_Ticks
    }DistanceUnitsType;
    
/**
* Units for angle specification.
*/
    typedef enum
    {
        /**
        * To use degrees. 
        */
        degrees = DAQmx_Val_Degrees,
        /**
        * To use radians. 
        */
        radians = DAQmx_Val_Radians,
        /**
        * To use meters. 
        */
        ang_ticks = DAQmx_Val_Ticks
    }AngleUnitsType;
    
/**
* Type of acquisition.
*/
    typedef enum
    {
        /**
        * Perform a finite operation.
        */
        finite = DAQmx_Val_FiniteSamps,
        /**
        * Perform a continuous operation.
        */
         continuous = DAQmx_Val_ContSamps
    }ModeType;
    
/**
* Type of active edge.
*/
    typedef enum
    {
        /**
        * The active edge is a rising edge.
        */
        rising_edge = DAQmx_Val_Rising,
        /**
        * The active edge is a falling edge.
        */
         falling_edge = DAQmx_Val_Falling
    }EdgeType;
   
/**
* Type of trigger.
*/
    typedef enum
    {
        /**
        * No trigger is used.
        */
        no_trigger,
        /**
        * A start trigger is used.
        */
        start_trigger,
        /**
        * A pause trigger is used.
        */
        pause_trigger
    }TriggerType;
    
/**
* Count direction.
*/
    typedef enum
    {
    /**
    * Increment the count register on each edge.
        */
        count_up = DAQmx_Val_CountUp,
        /**
        * Decrement the count register on each edge.
        */
        count_down = DAQmx_Val_CountDown,
        /**
        * The state of the digital controls the count direction.
        */
        external_control = DAQmx_Val_ExtControlled
    }DirectionType;
    
/**
* Decoding type for position measurement. 
*/
    typedef enum
    {
        /**
        * If signal A leads signal B, count the rising edges of signal A.
        * If signal B leads signal A, count the rising edges of signal B.
        */
        x1 = DAQmx_Val_X1,
        /**
        * Count the rising and falling edges of signal A.
        */
        x2 = DAQmx_Val_X2,
        /**
        * Increment the rising and falling edges of both signal A and signal B.
        */
        x4 = DAQmx_Val_X4,
        /**
        * Increment the count on rising edges of signal A.
        * Decrement the count on rising edges of signal B.
        */
        two_pulse = DAQmx_Val_TwoPulseCounting
    }   DecodingType;
    
/**
* Z phase.
*/
    typedef enum
    {
        /**
        * Reset the measurement when both signal A and B are high.
        */
        AHigh_BHigh = DAQmx_Val_AHighBHigh,
        /**
        * Reset the measurement when signal A is high and B is low.
        */
        AHigh_BLow = DAQmx_Val_AHighBLow,
        /**
        * Reset the measurement when signal A is low and B is high.
        */
        ALow_BHigh = DAQmx_Val_ALowBHigh,
        /**
        * Reset the measurement when both signal A and B are low.
        */
        ALow_BLow = DAQmx_Val_ALowBLow
    }  PhaseType;
    
    
/**
* Read mode.
*/
    typedef enum
    {
        /**
        * Check for available samples when the system receives an interrupt service request. 
        * This mode is the most CPU efficient, but results in lower possible sampling rates.
        */
        interrupt = DAQmx_Val_WaitForInterrupt,
        /**
        * Repeatedly check for available samples as fast as possible. 
        * This mode allows for the highest sampling rates at the expense of CPU efficiency.
        */
        poll = DAQmx_Val_Poll,
        /**
        * Repeatedly check for available samples, but yield control to other threads after each check. 
        * This mode offers a balance between sampling rate and CPU efficiency.
        */
        yield = DAQmx_Val_Yield
    } ReadModeType;

/**
* Overrun strategy.
*/  
    typedef enum
    {
        /**
        * User ignore that data have been lost.
        */
        ignore,
        /**
        * Data is deleted and user is notify.
        */
        notify,
        /**
        * Data is deleted, user is notify and acquisition is aborted.
        */
        abort,
        /**
        * Data is delete, user is notify and acquisition is restated.
        */
        restart,
        /**
        * Data is deleted and user is not notified.
        */
        trash
    }StrategyType;
    
/**
* Specifies the point in the buffer at which to begin a read operation. 
*/
    typedef enum
    {
        /**
        * Start reading samples relative to the first sample acquired.
        */
        first_sample = DAQmx_Val_FirstSample,
        /**
        * Start reading samples relative to the last sample returned by the previous read. 
        * For the first read operation, this position is the first sample acquired or the first pretrigger sample if you configured a reference trigger for the task.
        */
        current_read_position = DAQmx_Val_CurrReadPos,
        /**
        * Start reading samples relative to the first sample after the reference trigger occurred.
        */
        reference_trigger = DAQmx_Val_RefTrig,
        /**
        * Start reading samples relative to the first pretrigger sample. 
        * You specify the number of pretrigger samples to acquire when you configure a reference trigger.
        */
        first_pre_trigger_sample = DAQmx_Val_FirstPretrigSamp,
        /**
        * Start reading samples relative to the next sample acquired. 
        * For example, use this value and set Offset to -1 to read the last sample acquired.
        */
        most_recent_sample = DAQmx_Val_MostRecentSamp
    }StartPointType;

/**
* Data transfer method.
*/
    typedef enum
    {
        /**
        * Direct Memory Access. Data transfers take place independently from the application.
        */
        dma = DAQmx_Val_DMA,
        /**
        * Data transfers take place independently from the application. 
        * Using interrupts increases CPU usage because the CPU must service interrupt requests. 
        * Typically, you should use interrupts if the device is out of DMA channels.
        */
        interrupts = DAQmx_Val_Interrupts,
        /**
        * Data transfers take place when you call a read function.
        */
        programmedIO = DAQmx_Val_ProgrammedIO
    }DataTranferType;
    
/**
* Behavior of an output signal.
*/
    typedef enum
    {
        /**
        * Send a pulse to the output.
        */
        pulse = DAQmx_Val_Pulse,
        /**
        * Toggle the state of the terminal from low to high or from high to low.
        */
        toggle = DAQmx_Val_Toggle
    }BehaviorType;
    
/**
* Event type for callback function.
*/
    typedef enum
    {
        /**
        * Acquisition event type.
        */
        intoBuffer = DAQmx_Val_Acquired_Into_Buffer,
        /**
        * Generation event type.
        */
        fromBuffer = DAQmx_Val_Transferred_From_Buffer 
    }CBEventType;

}//namespace ni

#endif // _NI_H_
