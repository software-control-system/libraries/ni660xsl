// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   SimpleEventCounting.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================


#ifndef _SIMPLE_EVENT_COUNTING_H_
#define _SIMPLE_EVENT_COUNTING_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/EventCounting.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
//!  Perform simple event counting with one counter.
// ============================================================================
//!  
//! The counter counts events on the SOURCE input after the counter has been armed.
//! The counter can be armed via software command (start()) or upon receiving  a start trigger (set_start_trigger()). \n \n
//! Be aware that is only possible to use a start trigger (set_start_trigger()) <b>or</b> a pause trigger(set_pause_trigger())
//! since the GATE is used for both fonctionalities.
//! 
// ============================================================================
class NI660XSL_EXPORT SimpleEventCounting: public EventCounting
{
public:
	SimpleEventCounting(void);
	virtual ~SimpleEventCounting(void);
	/**
	* Set the timeout (time to for data wait when entering get_buffer )
	* @param _timeout The time to wait in seconds.
	*/
	virtual void set_timeout(double _timeout);
	/**
	* Set a pause trigger.
	* @param _trigger_source The trigger source.
	* @param _pause_when The state of pause trigger that will pause the acquisition.
	*/
	void set_pause_trigger(std::string _trigger_source, ni::LevelType _pause_when) throw (ni660Xsl::DAQException);
	/**
	* Configure the hardware with all preconfigured parameters.
	*/
	void configure(void) throw (ni660Xsl::DAQException);
	/**
	* Get the current value of the counter.
	* @param _timeout The time to wait for the value.
	* @return The current value.
	*/
	unsigned long get_current_raw_value() throw (ni660Xsl::DAQException);
	
private:
    bool use_pause_trig_;
    std::string pause_trigger_source_;
    ni::LevelType pause_when_;
	  double timeout_;
	
	
};

}//namespace


#endif //#ifndef _SIMPLE_EVENT_COUNTING_H_
