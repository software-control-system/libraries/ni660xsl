// ============================================================================
//
// = CONTEXT
//    TANGO Project - NI660X Support Library
//
// = FILENAME
//    Export.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _EXPORT_H_
#define _EXPORT_H_

#if defined (WIN32)
# pragma warning (disable : 4786)
# pragma warning (disable : 4251)
#endif

#if defined(WIN32)
# if defined(NI660XSL_HAS_DLL) 
#   if defined (NI660XSL_BUILD)
#     define NI660XSL_EXPORT __declspec(dllexport)
#   else
#     define NI660XSL_EXPORT __declspec(dllimport)
#   endif
# else
#   define NI660XSL_EXPORT
# endif
#else
# define NI660XSL_EXPORT
#endif

#endif // _EXPORT_H_



