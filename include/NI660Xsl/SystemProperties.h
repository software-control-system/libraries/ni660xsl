// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    SystemProperties.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

#ifndef _SYSTEM_PROPERTIES_H_
#define _SYSTEM_PROPERTIES_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>

#include <NI660Xsl/NI660XSLConfig.h>
#include <NI660Xsl/Exception.h>


namespace ni660Xsl
{
	// ============================================================================
	//!  To get some properties of the system from the NI driver.
	// ============================================================================
	//!  
	//! 
	//! 
	// ============================================================================  
    class NI660XSL_EXPORT SystemProperties
    {	
    public:
		   
    /**
    * Get the driver version.
    * @return The driver version. 
	*/
	static std::string get_driver_version(void) throw (ni660Xsl::DAQException);
	/**
	* Get the product type.
	* @param dev The name of the device.
	* @return The type of the device.
	*/
	static std::string get_product_type(std::string dev) throw (ni660Xsl::DAQException);
	/**
	* Get all the devices (that use NIDAQ driver) located in the cPCI crate.
	* @return A string containing all the devices names.
	*/
	static std::string get_all_devices_names(void) throw (ni660Xsl::DAQException);
	/**
	* Create a route between the source and the destination. (carry signals like triggers, clocks, events...) \n
	* To remove the routes, reset the device.
	* @param _source The originating terminal of the route.
	* @param _destination The receiving terminal of the route.
	* @param _invert_polarity If true the signal from the source will be inverted. (only in certain cases).
	*/
	static void route_terminals (std::string _source, std::string _destination, bool _invert_polarity)
	    throw (ni660Xsl::DAQException);
	/**
	* Reset a physical device (ex: "Dev1").
	* @param _dev the name of the device.
	*/
	static void reset_device (std::string _dev) throw (ni660Xsl::DAQException);
	/**
	* Get warning string.
	* @return The string corresponding to the last warning ocurred.
	*/
	static std::string get_warn_string (void);
	/**
	* @return true if a warning occured.
	*/
	static bool warn_occured(void);
    private:
	// Return a string corresponding to the last error occured.
	static std::string get_string_error(void) throw (ni660Xsl::DAQException);
	//
	static std::string warn_;
	//
	static bool warn_occured_;
	
	SystemProperties(void);
	virtual ~SystemProperties(void);
 
    };
    
}//namespace

#endif // #ifndef _SYSTEM_PROPERTIES_H_
