// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    RetriggerablePulseTrainGeneration.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

#ifndef _RETRIGERABLE_PULSE_TRAIN_GENERATION_H_
#define _RETRIGERABLE_PULSE_TRAIN_GENERATION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/OutputOperation.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
//! Perform retriggerable pulse-train generation
// ============================================================================
//!  
//! Generates a certain number of pulses after each trigger.
//! This application uses two counters if the number of pulses specified with 
//! set_nb_pulses() is strictly superior to 1.
//! The pairs of counters used are:
//! - ctr0 and ctr1
//! - ctr2 and ctr3
//! - ctr4 and ctr5
//! - ctr6 and ctr7
// ============================================================================
    class NI660XSL_EXPORT RetriggerablePulseTrainGeneration : public OutputOperation
    {
    public:
	RetriggerablePulseTrainGeneration(void);
	virtual ~RetriggerablePulseTrainGeneration(void);
	/**
	* Set the pin of the trigger.
	* @param _trigger_source The pin of the trigger.
	* @param _active_edge The edge on which to detect a trigger ( rising of falling).
	*/
	void set_trigger_source(std::string _trigger_source, ni::EdgeType _active_edge);
	/**
	* Set the number of pulses to generate after each trigger.
	* @param The number of pulses to generate.
  */
  void set_nb_pulses(int _nb_pulses);
	/**
  * Change on fly the chan parameters (frequency and duty cycle)
  */
  virtual void change_freq_values(ni660Xsl::OutFreqChan _chan) 
    throw (ni660Xsl::DAQException);
  /**
  * Change on fly the chan parameters.
	* @param _chan The channel with new parameters (high time et low time).
  */
  virtual void change_time_values(ni660Xsl::OutTimeChan _chan)
    throw (ni660Xsl::DAQException);
  /**
  * Change on the fly the chan parameters. 
	* @param _chan The channel with new parameters (high ticks and low ticks).
  */
  virtual void change_clock_ticks_values(ni660Xsl::OutClockTicksChan _chan) 
    throw (ni660Xsl::DAQException);
  /**
  * Configure hardware with all predefine parameters.
  */
	void configure(void) throw (ni660Xsl::DAQException);
    private:
	int nb_pulses_;
	std::string trigger_source_;
		    ni::EdgeType active_edge_;

    };
}

#endif // ifndef _RETRIGERABLE_PULSE_TRAIN_GENERATION_H_
