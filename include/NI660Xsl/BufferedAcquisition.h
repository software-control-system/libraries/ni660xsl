// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   BufferedAcquisition.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================


#ifndef _BUFFERED_ACQUISITION_H_
#define _BUFFERED_ACQUISITION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/Export.h>
#include <NI660Xsl/NI.h>
#include <NI660Xsl/CounterBasedOperation.h>
#include <NI660Xsl/InData.h>

namespace ni660Xsl
{
// ============================================================================
//!  Base class for buffered input operation.
// ============================================================================
//!  
//! 
//! 
//! 
// ============================================================================
  class NI660XSL_EXPORT BufferedAcquisition :public virtual CounterBasedOperation
  {
    public: 

        BufferedAcquisition(void);
        virtual ~BufferedAcquisition(void);

        /**
        * Configure the sample clock (rate at which data is saved in the buffer)
        * @param _sample_clk The pin of the sample clock.
        * @param _active_edge The active edge of the clock.
        * @param _max_rate The maximum expected rate of the clock in samples per seconds.
        */
        virtual void set_sample_clock(std::string _sample_clk, ni::EdgeType _active_edge, double _max_rate);

        /**
        * Set the buffer depth.
        * @param _nb_samples The buffer depth in samples per channel.
        */
        virtual void set_buffer_depth(unsigned long _nb_samples);

        /**
        * Set the total number of samples to receive.
        * @param _nb_samples Total number of samples per channel.
        */		
		virtual void set_total_nb_pts(unsigned long _nb_samples);
		
        /**
        * Set the overrun strategy
        * @param _strategy The strategy to apply.
        */
        virtual void set_overrun_strategy(ni::StrategyType _strategy);

        /**
        * Set the timeout (time to for data wait when entering get_buffer )
        * @param _timeout The time to wait in seconds.
        */
        virtual void set_timeout(double _timeout);

        /**
        * Get the timeout (time to for data wait when entering get_buffer )
        * @return The time to wait in seconds.
        */
        virtual double get_timeout();

        /**
        * Set the timing to be finite or continuous.
        * If the chosen mode is finite, the acquisition will fill one buffer.
        * Otherwise, the acquisition will fill buffers continuously.
        * @param _mode The mode to use.
        */
        virtual void set_timing_mode(ni::ModeType _mode);

        /**
        * Specifies the point in the buffer at which to begin a read operation.
        * @param _start_point The start point.
        * @param _offset The offset to apply to _start_point.
        */
        virtual void set_read_start_point(ni::StartPointType _start_point, unsigned long _offset);

        /**
        * Set the callback mode for buffered acquisition (default = disabled).
        * @param _enabled True if callback mechanism is to be enabled.
        */
        virtual void set_callback_mode(bool _enabled);

        /**
        * Start the acquisition of a raw buffer (polling mecanism).
        */
        virtual void get_raw_buffer(void) throw (ni660Xsl::DAQException);

        /**
        * Start the acquisition of a scaled buffer.
        */
        virtual void get_scaled_buffer(void) throw (ni660Xsl::DAQException);

        /**
        * Force the acquisition of the current raw buffer (trigger listener option).
        * @param _exp_nb expected number of samples to read
        */
        virtual void get_last_raw_buffer(unsigned long _exp_nb) throw (ni660Xsl::DAQException);

        /**
        * Force the acquisition of the current scaled buffer (trigger listener option).
        * @param _exp_nb expected number of samples to read
        */
        virtual void get_last_scaled_buffer(unsigned long _exp_nb) throw (ni660Xsl::DAQException);

        /**
        * This function must be implemented by user. Will be called each time an overrun occurred.
        */
        virtual void handle_data_lost(void) = 0;

        /**
        * This function must be implemented by user. Will be called each time a timeout occurred.
        */
        virtual void handle_timeout(void) = 0;

        /**
        * This function must be implemented by user. 
        * Will be called each time a raw buffer has been received.
        * <b>Don't forget to delete received buffer!!!!!!!!!</b>
        * @param buffer The received buffer.
        * @param _samples_read The actual number of samples read from each channel (returned value).
        */
        virtual void handle_raw_buffer(ni660Xsl::InRawBuffer* buffer, long& _samples_read) = 0;

        /**
        * This function must be implemented by user. 
        * Will be called each time a raw buffer has been received.
        * <b>Don't forget to delete received buffer!!!!!!!!!</b>
        * @param buffer The received buffer.
        * @param _samples_read The actual number of samples read from each channel (returned value).
        */
        virtual void handle_scaled_buffer(ni660Xsl::InScaledBuffer* buffer, long& _samples_read) = 0;
        
    protected:
        std::string sample_clk_;
        ni::EdgeType active_edge_;
        double max_rate_;
        unsigned long nb_samples_; // intermediate buffer depth
        ni::StrategyType strategy_;
        ni::StartPointType start_point_;
        unsigned long offset_;
        double timeout_;
        ni::ModeType mode_;
        bool use_callback_;
		unsigned long total_nb_pts_; // total number of samples
    };

}//namespace


#endif //#ifndef _BUFFERED_ACQUISITION_H_
