// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    PulsedTaskManager.h
//
// = AUTHOR
//    S. Minolli (fully inspired from N. Leclercq ASL PulseTaskManager class)
//
// ============================================================================

#ifndef _PULSED_TASK_MANAGER_H_
#define _PULSED_TASK_MANAGER_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <ace/Reactor.h>
#include <NI660Xsl/NI660XSLConfig.h>
#include <NI660Xsl/Thread.h>
#include <NI660Xsl/PulsedTask.h>

namespace ni660Xsl {

// ============================================================================
//! The NI660XSL pulsed task manager abstraction class.  
// ============================================================================
//!  
//! detailed description to be defined
//! 
// ============================================================================
class NI660XSL_EXPORT PulsedTaskManager : public Thread
{
public:

  /**
   * Get unique instance (singleton).
   */
  static PulsedTaskManager * instance ();

  /**
   * Release resources.
   */
  virtual ~PulsedTaskManager ();

  /**
   * Register a PulsedTask.
   */
  int register_task (PulsedTask * t, void * arg, ACE_Time_Value& intv);

  /**
   * Remove a PulsedTask.
   */
  int remove_task (PulsedTask * t);

  /**
   * ACE internal cooking. Consider as private.
   */
  const ACE_TCHAR * dll_name ();

  /**
   * ACE internal cooking. Consider as private.
   */
  const ACE_TCHAR * name ();

  /**
   * ACE internal cooking. Consider as private.
   */
  static void close_singleton ();

protected:

  /**
   *  Entry point implementation.
   */
  virtual ACE_THR_FUNC_RETURN svc (void* arg);

private:

  /**
   * Initialization. 
   */
  PulsedTaskManager ();
  
  /**
   * The underlying ACE Reactor.
   */
  ACE_Reactor * reactor_;

  /**
   * A mutex to protect the singleton against race conditions.
   */
  static ACE_Recursive_Thread_Mutex instance_lock;

  /**
   * The PulsedTaskManager singleton
   */
  static PulsedTaskManager * manager;

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(PulsedTaskManager& operator= (const PulsedTaskManager&))
  ACE_UNIMPLEMENTED_FUNC(PulsedTaskManager(const PulsedTaskManager&))
};

} // namespace ni660Xsl

#endif // _PULSED_TASK_MANAGER_H_

