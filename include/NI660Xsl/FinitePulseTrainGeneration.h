// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    FinitePulseTrainGeneration.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

#ifndef _FINITE_PULSE_TRAIN_GENERATION_H_
#define _FINITE_PULSE_TRAIN_GENERATION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/PulseTrainGeneration.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
//!  Perform finite pulse-train generation
// ============================================================================
//!  
//! Generate a specified number of pulses. 
//! This application uses two counters if the number of pulses specified with 
//! set_nb_pulses() is strictly superior to 1.
//! The pairs of counters used are:
//! - ctr0 and ctr1
//! - ctr2 and ctr3
//! - ctr4 and ctr5
//! - ctr6 and ctr7
// ============================================================================
    class NI660XSL_EXPORT FinitePulseTrainGeneration: public PulseTrainGeneration
    {
    public: 
	FinitePulseTrainGeneration(void);
	virtual ~FinitePulseTrainGeneration(void);
	/**
	* Set the number of pulses to generate after each trigger.
	* @param _nb_pulses The number of pulses.
	*/
	virtual void set_nb_pulses(int _nb_pulses);
	/**
	* Configure hardware with all predefine parameters
	*/
	virtual void configure(void) throw (ni660Xsl::DAQException);
    private:
			// The number of pulses.
	int nb_pulses_;
    };
}

#endif // ifndef _FINITE_PULSE_TRAIN_GENERATION_H_
