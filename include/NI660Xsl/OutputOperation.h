// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    OutputOperation.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================


#ifndef _OUTPUT_OPERATION_H_
#define _OUTPUT_OPERATION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <vector>

#include <NI660Xsl/CounterBasedOperation.h>
#include <NI660Xsl/Channels.h>
#include <NI660Xsl/Exception.h>


namespace ni660Xsl
{
    
    typedef std::vector<ni660Xsl::OutFreqChan> OutFreqChans;
    typedef std::vector<ni660Xsl::OutTimeChan> OutTimeChans;
    typedef std::vector<ni660Xsl::OutClockTicksChan> OutClockTicksChans;

// ============================================================================
//!  ni660XSL base class for all output operations.
// ============================================================================
//!  
//! Implements all common methods for output operations with counters.
//! 
// ============================================================================   
class NI660XSL_EXPORT OutputOperation : public CounterBasedOperation  
{
    public:
	OutputOperation(void);
	virtual ~OutputOperation(void);
	/**
	* Add an output channel with frequency parameters. (The sample clock is internal) 
	* @param _chan A frequency channel.
	*/
	void add_frequency_channel(ni660Xsl::OutFreqChan _chan) ;
	/**
	* Remove the last channel added throw add_frequency_channel
	*/
	void remove_last_added_frequency_channel();
	/**
	* Add an output channel with time parameters. (The sample clock is internal)
	* @param _chan A time channel.
	*/
	void add_time_channel(ni660Xsl::OutTimeChan _chan) ;
	/**
	* Remove the last channel added throw add_time_channel
	*/
	void remove_last_added_time_channel();
	/**
	* Add an output channel with clock ticks parameters. (The clock can be external)
	* @see set_sample_clock()
	* @param _chan A clock ticks channel.  
	*/
	void add_clock_ticks_channel(ni660Xsl::OutClockTicksChan _chan);
	/**
	* Remove the last channel added throw add_clock_ticks_channel
	*/
	void remove_last_added_clock_ticks_channel();
	/**
	* Wait until operation is finished.
	* @param _time_to_wait The time to wait in seconds (-1 indicates to wait indifinitly). 
	* @return True if this fonction has returned before the task was finished. Indicates
		* that the task could not be finished within _time_to_wait.
	*/
	virtual bool wait_finished(double _time_to_wait) throw (ni660Xsl::DAQException);
	/**
	* 
	* @param 
	* @return 
		* that 
	*/
	virtual bool is_done(bool& _is_done) throw (ni660Xsl::DAQException);
	/**
	* Configure hardware with all predefined parameters
	*/
	virtual void configure (void) = 0;	  
	
    protected:
	//vector of freq chan
	OutFreqChans freq_chans_;
	//vector of time chan
	OutTimeChans time_chans_;
	//vector of clock ticks chan
	OutClockTicksChans clk_ticks_chans_;
	//configure the channels on the hw
	virtual void configure_channels(void) throw (ni660Xsl::DAQException);
	

};

}//namespace

#endif // !defined(_OUTPUT_OPERATION_H_)
