// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    PulsedTask.h
//
// = AUTHOR
//    S. Minolli (fully inspired from N. Leclercq ASL PulseTask class)
//
// ============================================================================

#ifndef _PULSED_TASK_H_
#define _PULSED_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <ace/Event_Handler.h>
#include <NI660Xsl/NI660XSLConfig.h>

//#include <iostream>

namespace ni660Xsl {

// ============================================================================
//! The NI660XSL pulsed task abstraction class [ABSTRACT].  
// ============================================================================
//!  
//! detailed description to be defined
//! 
// ============================================================================
class NI660XSL_EXPORT PulsedTask : public ACE_Event_Handler 
{
   typedef ACE_Event_Handler inherited;

   friend class PulsedTaskManager;

public:
  /**
   * Initialization. 
   */
  PulsedTask ();

  /**
   * Release resources.
   */
  virtual ~PulsedTask ();

  /**
   * User defined behaviour. Executed when Task is pulsed.
   *
   * \param arg The PulsedTask argument (pointer to user data)
   */
  virtual int pulsed (void* arg) = 0;

  /**
   * Start the PulsedTask.
   *
   * \param arg The task generic argument (passed to the user defined 
   *  PulsedTask::pulsed).
   *
   * \param pulse_interval The pulse period in milliseconds (defaults to 
   *  1 second).
   *
   * \param pulse_count The number of times the Task should be executed. 
   *  Pass 0 for infinite (re)trigger (the default).
   *
   * \return -1 on failure, 0 otherwise.
   */
  int start (void *arg, 
             unsigned long pulse_interval = 1000,
             unsigned long pulse_count = 0);

  /**
   * Stop the PulsedTask.  
   *
   * \return -1 on failure, 0 otherwise.
   */
  int stop ();

  //- ACE internal cooking. 
  //- Consider this member as private and do not call it directly.
  int handle_timeout (const ACE_Time_Value&, const void* = 0);

private:

  /**
   * The number of times the task should be executed.
   */
   unsigned long max_count_;

  /**
   * The number of times the task has been be executed.
   */
   unsigned long cur_count_;

  /**
   * The timer identifier.
   */
   long timer_;

  /**
   * Protect the task against race consition
   */
   ACE_Thread_Mutex lock_;

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(PulsedTask& operator= (const PulsedTask&))
  ACE_UNIMPLEMENTED_FUNC(PulsedTask(const PulsedTask&))
};

} // namespace ni660Xsl

#endif // _PULSED_TASK_H_

