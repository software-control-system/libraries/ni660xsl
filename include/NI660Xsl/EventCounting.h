// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   EventCounting.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================


#ifndef _EVENT_COUNTING_H_
#define _EVENT_COUNTING_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>

#include <NI660Xsl/InputOperation.h>
#include <NI660Xsl/Channels.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
//!  ni660XSL base class for event counting.
// ============================================================================
//!  
//!  
//! 
// ============================================================================
class NI660XSL_EXPORT EventCounting: public InputOperation
{
  public:

	  EventCounting(void);
    virtual ~EventCounting(void);

    /**
    * Add an an input channel to an event counting operation. \n
	  * It is only possible to add one channel.
	  * @param _chan The channel to add.
    */
	  virtual void add_input_channel(ni660Xsl::EventCountChan _chan);

    /**
    * Configure the hardware.
    */
	  virtual void configure(void) = 0;

  protected:
	  std::string chan_name_;
	  ni::EdgeType edge_;
	  long initial_count_;
	  ni::DirectionType count_direction_;
	  int added_channels;
    std::string input_terminal_;
	  
    // Configure the input channel.
    virtual void configure_channel(void) throw (ni660Xsl::DAQException);
};

}//namespace


#endif //#ifndef _EVENT_COUNTING_H_
