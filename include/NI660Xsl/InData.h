// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    AIData.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================

#ifndef _AI_DATA_H_
#define _AI_DATA_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/Data_T.h>

namespace ni660Xsl {

// ============================================================================
// Create a dedicated type for raw data (i.e numeric values)
// ============================================================================
	/**
	* Buffer of unsigned long.
	*/
typedef Buffer<unsigned long> InRawBuffer;

// ============================================================================
// Create a dedicated type for scaled data (i.e. expressed in volts)  
// ============================================================================
/**
* Buffer of double.
*/
typedef Buffer<double> InScaledBuffer;

} // namespace ni660Xsl

#endif // _AI_DATA_H_

