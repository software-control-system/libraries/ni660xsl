// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    Exception.h
//
// = AUTHORS
//    G.Abeille & N.Leclercq 
//
// ============================================================================

#ifndef _EXCEPTION_H_
#define _EXCEPTION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>
#include <vector>

#include <NI660Xsl/Export.h>

namespace ni660Xsl
{

typedef enum {
  WARN,
  ERR, 
  PANIC
} ErrorSeverity;
// ============================================================================
//!  error occurring in DAQException 
// ============================================================================
//!  
//! 
//! 
// ============================================================================
class NI660XSL_EXPORT Error
{
public:

    Error (void);
    
    Error (const char *reason,
        const char *desc,
        const char *origin,
        int err_code = -1, 
        int severity = ERR);
    
    
    Error (const std::string& reason,
        const std::string& desc,
        const std::string& origin, 
        int err_code = -1, 
        int severity = ni660Xsl::ERR);

  /**
   * Copy constructor. 
   */
  Error (const Error& src);
  /**
   * Error details: code 
   */
  virtual ~Error (void);
  /**
   * operator= 
   */
  Error& operator= (const Error& _src);
  /**
   * Error details: reason 
   */
  std::string reason;
  /**
   * Error details: description 
   */
  std::string desc;
  /**
   * Error details: origin 
   */
  std::string origin;
  /**
   * Error details: code 
   */
  int code;
  /**
   * Error details: severity 
   */
  int severity;

};

typedef std::vector<Error> ErrorList;
// ============================================================================
//!  Exception that can occurs while using ni660XSL
// ============================================================================
//!  
//! 
//! 
// ============================================================================
class NI660XSL_EXPORT DAQException
{
public:

 
    DAQException (void);
    DAQException (const char *reason,
        const char *desc,
        const char *origin,
        int err_code = -1, 
        int severity = ni660Xsl::ERR);
    
    DAQException (const std::string& reason,
        const std::string& desc,
        const std::string& origin, 
        int err_code = -1, 
        int severity = ni660Xsl::ERR);
  
  DAQException (const Error& error);
  /**
   * Copy constructor. 
   */
  DAQException (const DAQException& src);
  /**
   * operator=
   */
  DAQException& operator= (const DAQException& _src); 
  /**
   * Release resources.
   */
  virtual ~DAQException (void);
  /**
   * Push the specified error into the errors list.
   */
  void push_error (const char *reason,
      const char *desc,
      const char *origin, 
      int err_code = -1, 
      int severity = ni660Xsl::ERR);
  /**
   * Push the specified error into the errors list.
   */
  void push_error (const std::string& reason,
      const std::string& desc,
      const std::string& origin, 
      int err_code = -1, 
      int severity = ni660Xsl::ERR);
  /**
   * Push the specified error into the errors list.
   */
  void push_error (const Error& error);
  /**
   * The errors list
   */
   ErrorList errors;
  
};

}//namespace

#endif // _EXCEPTION_H_
