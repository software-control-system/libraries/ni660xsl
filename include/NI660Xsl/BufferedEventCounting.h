// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   BufferedEventCounting.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================


#ifndef _BUFFERED_EVENT_COUNTING_H_
#define _BUFFERED_EVENT_COUNTING_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>

#include <NI660Xsl/InData.h>
#include <NI660Xsl/Exception.h>
#include <NI660Xsl/EventCounting.h>
#include <NI660Xsl/BufferedAcquisition.h>
#include <NI660Xsl/PulsedTask.h>

namespace ni660Xsl
{

  class TimeoutCallerEvt;

// ============================================================================
//!  Perform buffered event counting
// ============================================================================
//!  
//! Buffered event counting is similar to simple event counting (SimpleEventCounting) 
//! except that the counter values are saved in a buffer. 
//! The rate at which data is saved is determined by a sample clock.
//! 
// ============================================================================
    class NI660XSL_EXPORT BufferedEventCounting:  public EventCounting, 
                                                  public BufferedAcquisition
    {
      public:

        BufferedEventCounting();
        virtual ~BufferedEventCounting();
        
        /**
        * Configure the hardware with all preconfigured parameters.
        */
        virtual void configure(void) 
          throw (ni660Xsl::DAQException);
        
        /**
        * This function must be implemented by user. 
        * Will be called each time a overrun occurred.
        */
        virtual void handle_data_lost() = 0;

        /**
        * This function must be implemented by user. 
        * Will be called each time a timeout occurred.
        */
        virtual void handle_timeout() = 0;

        /**
        * This function must be implemented by user. 
        * Will be called each time a buffer has been received.
        * <b>Don't forget to delete received buffer!!!!!!!!!</b>
        * @param buffer The received buffer.
        * @param _samples_read The actual number of samples read from each channel(returned value).
        */
        virtual void handle_raw_buffer(ni660Xsl::InRawBuffer* buffer, long& _samples_read) = 0;

        //- Redefine start, stop & abort functions for timeout management in callback mode
			  /**
			  * Start the operation.
			  */
			  void start (void) throw (ni660Xsl::DAQException);
  			
			  /**
			  * Stop the operation.
			  */
			  void stop (void) throw (ni660Xsl::DAQException);
			  /**
			  * Abort the operation.
			  */
			  void abort (void) throw (ni660Xsl::DAQException);

			  /**
			  * Abort the operation & release counter.
			  */
			  void abort_and_release (void) throw (ni660Xsl::DAQException);

        // Timeout stuff - consider this member as private one. Do not call directly.
        // Timeout callback, for callback mode only
        void handle_tmo_cb(void);

        // timeout caller
        TimeoutCallerEvt * tmo_caller_;

		// stop flag (to avoid race condition between stop & callback in buffered mode)
		bool stop_flag_;
		
		// Mutex to protect stop flag
		ACE_Thread_Mutex stop_lock_;
    };

// ============================================================================
// TimeoutCallerEvt : a ni660Xsl::PulsedTask to manage timeout function in 
// callback mode, for EventCounting class
// ============================================================================
class TimeoutCallerEvt : public PulsedTask
{
public:

  TimeoutCallerEvt (void) 
  {
    /*noop*/
  };

  virtual ~TimeoutCallerEvt () 
  {
    /*noop*/
  };

  virtual int pulsed (void* arg) 
  {
    BufferedEventCounting * acq = reinterpret_cast<BufferedEventCounting *>(arg);
    if (acq)
      acq->handle_tmo_cb();
    return 0;
  }
};

}//namespace


#endif //#ifndef _BUFFERED_EVENT_COUNTING_H_
