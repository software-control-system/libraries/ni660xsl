//-----------------------------------------------------------------------------
// WINDOWS PRAGMA
//-----------------------------------------------------------------------------
#if defined (WIN32)
# pragma warning (disable : 4286)
#endif

//=============================================================================
// _NI660XSL_TO_TANGO_EXCEPTION MACRO
//=============================================================================
#define _NI660XSL_TO_TANGO_EXCEPTION(_ni660Xsl_ex, _tango_ex) \
  Tango::DevErrorList error_list(_ni660Xsl_ex.errors.size()); \
  error_list.length(_ni660Xsl_ex.errors.size()); \
  for (int _ii = 0; _ii < _ni660Xsl_ex.errors.size(); _ii++) \
  { \
    error_list[_ii].reason = CORBA::string_dup(_ni660Xsl_ex.errors[_ii].reason.c_str()); \
    error_list[_ii].desc   = CORBA::string_dup(_ni660Xsl_ex.errors[_ii].desc.c_str()); \
    error_list[_ii].origin = CORBA::string_dup(_ni660Xsl_ex.errors[_ii].origin.c_str()); \
    switch (_ni660Xsl_ex.errors[_ii].severity) \
    { \
      case ni660Xsl::WARN: \
        error_list[_ii].severity = Tango::WARN; \
        break; \
      case ni660Xsl::PANIC: \
        error_list[_ii].severity = Tango::PANIC; \
        break; \
      case ni660Xsl::ERR: \
      default: \
        error_list[_ii].severity = Tango::ERR; \
        break; \
    } \
  } \
  Tango::DevFailed _tango_ex(error_list)

//=============================================================================
// _HANDLE_NI660XSL_EXCEPTION MACRO
//=============================================================================
#define _HANDLE_NI660XSL_EXCEPTION(_cmd, _origin) \
  catch (const ni660Xsl::DAQException& _ni660Xsl_ex) \
  { \
    _NI660XSL_TO_TANGO_EXCEPTION(_ni660Xsl_ex, _tango_ex); \
    ERROR_STREAM << _tango_ex << std::endl; \
    TangoSys_OMemStream d; \
    d << "ni660Xsl::DAQException caught while trying to execute " \
      << _cmd \
      << std::ends; \
    TangoSys_OMemStream o; \
    o << _origin << " [" << __FILE__ << "::" << __LINE__ << "]" << std::ends; \
    Tango::Except::re_throw_exception(_tango_ex, \
                   static_cast<const char*>("NI660XSL_ERROR"), \
				           static_cast<const char*>(d.str().c_str()), \
				           static_cast<const char*>(o.str().c_str())); \
  } \
  catch (...) \
  { \
    TangoSys_OMemStream d; \
    d << "unknown exception caught while trying to execute " \
      << _cmd \
      << std::ends; \
    TangoSys_OMemStream o; \
    o << _origin << " [" << __FILE__ << "::" << __LINE__ << "]" << std::ends; \
    Tango::DevErrorList errors(1); \
    errors.length(1); \
    errors[0].severity = Tango::ERR; \
    errors[0].reason = CORBA::string_dup("UNKNOWN_ERROR"); \
    errors[0].desc = CORBA::string_dup(d.str().c_str()); \
    errors[0].origin = CORBA::string_dup(o.str().c_str()); \
    Tango::DevFailed _tango_ex(errors); \
    ERROR_STREAM << _tango_ex << std::endl; \
    throw _tango_ex; \
  }

//=============================================================================
// _NI660XSL_TRY MACRO
//=============================================================================
#define _NI660XSL_TRY(_invoke, _cmd, _origin) \
  try \
  { \
    _invoke; \
  } \
  _HANDLE_NI660XSL_EXCEPTION(_cmd, _origin)

//=============================================================================
// _NI660XSL_TRY_ACTION MACRO
//=============================================================================
#define _NI660XSL_TRY_ACTION(_invoke, _cmd, _origin, _action) \
  try { \
    _NI660XSL_TRY(_invoke, _cmd, _origin) \
    _action; \
  } \
  catch (...) { \
    _action; \
    throw; \
  }

//=============================================================================
// _NI660XSL_TRY_REACTION MACRO
//=============================================================================
#define _NI660XSL_TRY_REACTION(_invoke, _cmd, _origin, _reaction) \
  try { \
    _NI660XSL_TRY(_invoke, _cmd, _origin) \
  } \
  catch (...) { \
    _reaction; \
    throw; \
  }

