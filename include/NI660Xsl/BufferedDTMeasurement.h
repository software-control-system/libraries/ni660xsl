// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   BufferedDTMeasurement.h
//
// = AUTHORS
//    S. Minolli 
//
// ============================================================================


#ifndef _BUFFERED_DT_MEASUREMENT_H_
#define _BUFFERED_DT_MEASUREMENT_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>

#include <NI660Xsl/InData.h>
#include <NI660Xsl/Exception.h>
#include <NI660Xsl/DTMeasurement.h>
#include <NI660Xsl/BufferedAcquisition.h>
#include <NI660Xsl/PulsedTask.h>

namespace ni660Xsl
{

  class TimeoutCallerDt;

// ============================================================================
//!  Perform buffered delta time measurement
// ============================================================================
//!  
//! Buffered dt measurement is similar to simple dt measurement (SimpleDTMeasurement) 
//! except that the counter values are saved in a buffer. 
//! The rate at which data is saved is determined by a sample clock.
//! 
// ============================================================================
    class NI660XSL_EXPORT BufferedDTMeasurement:  public DTMeasurement, 
                                                  public BufferedAcquisition
    {
      public:

        BufferedDTMeasurement();
        virtual ~BufferedDTMeasurement();
        
        /**
        * Configure the hardware with all preconfigured parameters.
        */
        virtual void configure(void) 
          throw (ni660Xsl::DAQException);
        
        /**
        * This function must be implemented by user. 
        * Will be called each time a overrun occurred.
        */
        virtual void handle_data_lost() = 0;

        /**
        * This function must be implemented by user. 
        * Will be called each time a timeout occurred.
        */
        virtual void handle_timeout() = 0;

        /**
        * This function must be implemented by user. 
        * Will be called each time a buffer has been received.
        * <b>Don't forget to delete received buffer!!!!!!!!!</b>
        * @param buffer The received buffer.
        * @param _samples_read The actual number of samples read from each channel(returned value).
        */
        virtual void handle_raw_buffer(ni660Xsl::InRawBuffer* buffer, long& _samples_read) = 0;

        //- Redefine start, stop & abort functions for timeout management in callback mode
			  /**
			  * Start the operation.
			  */
			  void start (void) throw (ni660Xsl::DAQException);
  			
			  /**
			  * Stop the operation.
			  */
			  void stop (void) throw (ni660Xsl::DAQException);
			  /**
			  * Abort the operation.
			  */
			  void abort (void) throw (ni660Xsl::DAQException);

			  /**
			  * Abort the operation & release counter.
			  */
			  void abort_and_release (void) throw (ni660Xsl::DAQException);

        // Timeout stuff - consider this member as private one. Do not call directly.
        // Timeout callback, for callback mode only
        void handle_tmo_cb(void);

        // timeout caller
        TimeoutCallerDt * tmo_caller_;

		// stop flag (to avoid race condition between stop & callback in buffered mode)
		bool stop_flag_;
		
		// Mutex to protect stop flag
		ACE_Thread_Mutex stop_lock_;
    };

// ============================================================================
// TimeoutCallerDt : a ni660Xsl::PulsedTask to manage timeout function in 
// callback mode, for delta time counting class
// ============================================================================
class TimeoutCallerDt : public PulsedTask
{
public:

  TimeoutCallerDt (void) 
  {
    /*noop*/
  };

  virtual ~TimeoutCallerDt () 
  {
    /*noop*/
  };

  virtual int pulsed (void* arg) 
  {
    BufferedDTMeasurement * acq = reinterpret_cast<BufferedDTMeasurement *>(arg);
    if (acq)
      acq->handle_tmo_cb();
    return 0;
  }
};

}//namespace


#endif //#ifndef _BUFFERED_DT_MEASUREMENT_H_
