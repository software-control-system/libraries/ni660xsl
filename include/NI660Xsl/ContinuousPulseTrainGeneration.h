// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   ContinuousPulseTrainGeneration.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

#ifndef _CONTINUOUS_PULSE_TRAIN_GENERATION_H_
#define _CONTINUOUS_PULSE_TRAIN_GENERATION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/Exception.h>
#include <NI660Xsl/PulseTrainGeneration.h>

namespace ni660Xsl
{
// ============================================================================
//!  Perfom continuous pulse train generation
// ============================================================================
//!  
//! Generate (a) pulse(s) train continuously. Parameters for the train can be specified in
//! terms of:
//!	- frequency with add_frequency_channel()
//!	- time with add_time_channel()
//!	- clock ticks of an internal or external clock add_clock_ticks_channel()
//! 
//! This parameters can be then changed while the train is ouput with:
//!	- change_freq_values()
//!	- change_time_values()
//!	- change_clock_ticks_values()
// ============================================================================
    class NI660XSL_EXPORT ContinuousPulseTrainGeneration: public PulseTrainGeneration
    {

    public:
	ContinuousPulseTrainGeneration(void);
	virtual ~ContinuousPulseTrainGeneration(void);
	/**
	* Set a pause trigger. Only in continuous mode.
	* @param _trigger_source The source of the trigger.
	* @param _pause_when The active level of the pause trigger.
	*/
	virtual void set_pause_trigger(std::string _trigger_source, ni::LevelType _pause_when) ;
	/**
	* Change on fly the chan parameters (frequency and duty cycle)
	*/
	virtual void change_freq_values(ni660Xsl::OutFreqChan _chan) 
	    throw (ni660Xsl::DAQException);
	/**
	* Change on fly the chan parameters.
				* @param _chan The channel with new parameters (high time et low time).
	*/
	virtual void change_time_values(ni660Xsl::OutTimeChan _chan)
	    throw (ni660Xsl::DAQException);
	/**
	* Change on the fly the chan parameters. 
				* @param _chan The channel with new parameters (high ticks and low ticks).
	*/
	virtual void change_clock_ticks_values(ni660Xsl::OutClockTicksChan _chan) 
	    throw (ni660Xsl::DAQException);
	/**
	* Configure hardware with all predefine parameters
	*/
	virtual void configure(void) 
	    throw (ni660Xsl::DAQException);

    private:
	std::string pause_trigger_source_;
	ni::LevelType pause_when_;
    };
}//namesapce


#endif // ifndef _CONTINUOUS_PULSE_TRAIN_GENERATION_H_
