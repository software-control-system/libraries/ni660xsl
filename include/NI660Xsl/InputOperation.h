// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   InputOperation.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================


#ifndef _INPUT_OPERATION_H_
#define _INPUT_OPERATION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/CounterBasedOperation.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
//!  ni660XSL base class for all input operations.
// ============================================================================
//!  
//! Implements all common methods for input operations with counters.
//! 
// ============================================================================   
class NI660XSL_EXPORT InputOperation : public virtual CounterBasedOperation  
{
    public:
	InputOperation(void);
	virtual ~InputOperation(void);	 
	/**
	* Set a start trigger.
	* @param _trigger_source The source of the trigger.
	* @param _active_edge The active edge of the trigger.
	*/
	virtual void set_start_trigger(std::string _trigger_source, ni::EdgeType _active_edge);
		/**
		* Set how to wait for data when reading. (interrupt, yiedl, poll)
		* @param _read_mode 
		*/
		virtual void set_read_mode(ni::ReadModeType _read_mode);
		/**
		* Set the data transfer
		* @param The data tranfer mechanism (dma, interrupts,..)
		*/
		void set_data_tranfer_mechanism(ni::DataTranferType _data_transfer);
		/**
		* Set the type of outputed signal when counter reaches its terminal count.
		* @param _terminal The terminal where the signal is outputed.
		* @param _behavior The behavior of the output signal (pulse or toggle).
		*/
		virtual void set_terminal_count(std::string _terminal, ni::BehaviorType _behavior);
    /**
    * Set minimum pulse width
    * @param _min_pulse_width the minimum pulse width recognized, in seconds
    */
    virtual void set_min_pulse_width( double _min_pulse_width_seconds );

		/**
		* Configure hardware with all predefined parameters.
		*/
		virtual void configure (void) = 0;
    
		protected:
			bool use_start_trig_;
			std::string start_trigger_source_;
			ni::EdgeType trig_active_edge_;
			ni::ReadModeType read_mode_;
			std::string terminal_;
			ni::BehaviorType behavior_;
			ni::DataTranferType data_transfer_;
      bool enable_min_pulse_width_;
      double min_pulse_width_seconds_;
};

}//namespace

#endif // !defined(_INPUT_OPERATION_H_)
