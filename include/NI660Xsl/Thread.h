// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660Xsl Support Library
//
// = FILENAME
//    Thread.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _THREAD_H_
#define _THREAD_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/NI660XSLConfig.h>
#include <ace/Thread_Manager.h>


namespace ni660Xsl {

// ============================================================================
//! The NI660Xsl thread abstraction base class [ABSTRACT].  
// ============================================================================
//!  
//! detailed description to be defined
//! 
// ============================================================================
class NI660XSL_EXPORT Thread
{
public:

  /**
   * Initialization. 
   * @param joinable True if joinable (the default), false otherwise.
   */
  Thread (bool joinable = true);
  
  /**
   * Release resources.
   */
  virtual ~Thread (void);

  /**
   * Run the thread.
   * @param arg Thread entry point's argument.
   * @param prio Thread priority (defaults to ACE_DEFAULT_THREAD_PRIORITY).
   * @return -1 on failure, 0 otherwise.
   */
  int run (void* arg = 0, long prio = ACE_DEFAULT_THREAD_PRIORITY);

  /**
   * Ask the Thread to quit.
   * @return -1 on failure, 0 otherwise.
   */
  int quit (void);

  /**
   * Join with the Thread. Thread must be joinable.
   * @param status The thread result.
   * @return -1 on failure, 0 otherwise.
   */
  int join (ACE_THR_FUNC_RETURN *status = 0);

  /**
   * Ask the Thread to quit then join with the Thread. 
   * The Thread must be joinable.
   * @param status The thread result.
   * @return -1 on failure, 0 otherwise.
   */
  int quit_and_join (ACE_THR_FUNC_RETURN *status = 0);

  /**
   * Returns the Thread identifier
   * @return The Thread id.
   */
  ACE_thread_t id (void) const
  {
	  return this->self_idt_;
  };



protected:

  /**
   * The thread entry point.
   * @param arg The thread argument (see run).
   * @return generic thread result.
   */
  virtual ACE_THR_FUNC_RETURN svc (void* arg) = 0;
  
  /**
   * Returns true if an "external entity" asked this thread to quit, 
   * false otherwise. Can be used in a the user defined "while not 
   * quit_resquested" loop for the implementation of the pure virtual 
   * member svc. 
   * @return the "quit requested" status. 
   */
  bool quit_requested (void) const
  {
	  //std::cout<<"go_on:"<<go_on_<<std::endl;
	  return this->go_on_ ? false : true;
  };

  /**
   * Thread argument
   */
  void* arg_;

private:

  /**
   * Internal thread spawner.
   */
  static ACE_THR_FUNC_RETURN spawner (void* arg);

  /**
   * Internal implementation of the <quit> member.
   * @return -1 on failure, 0 otherwise.
   */
  int quit_i (void);

  /**
   * Internal implementation of the <join> member.
   * @param status The thread result.
   * @return -1 on failure, 0 otherwise.
   */
  int join_i (ACE_THR_FUNC_RETURN *status = 0);

  /**
   * The thread identifier.
   */
  ACE_thread_t self_idt_;

  /**
   * The thread handle.
   */
  ACE_hthread_t self_hdl_;

  /**
   * The thread group id.
   */
  int grp_id_; 

  /**
   * True if the Thread is joinable, false otherwise.
   */
  bool join_;

  /**
   * True until "someone" ask the Thread to quit.
   */
  bool go_on_;

  /**
   * A thread mutex.
   */
  ACE_Thread_Mutex lock_;



  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(Thread& operator= (const Thread&))
  ACE_UNIMPLEMENTED_FUNC(Thread(const Thread&))
};

} // namespace ni660Xsl



#endif // _THREAD_H_

