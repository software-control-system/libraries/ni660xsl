// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    Data_T.cpp
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _DATA_T_CPP_
#define _DATA_T_CPP_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <ace/OS_NS_string.h>
#include <ace/Malloc_Allocator.h>
#include <NI660Xsl/Data_T.h>

namespace ni660Xsl {

// ============================================================================
// Class : Container
// ============================================================================
// ============================================================================
// Container::Container
// ============================================================================
template <class T> 
Container<T>::Container(T& _data)
 : data_ (_data)
{

}

// ============================================================================
// Container::~Container
// ============================================================================
template <class T> 
Container<T>::~Container(void)
{

}

// ============================================================================
// Container::size
// ============================================================================
template <class T> 
size_t Container<T>::size(void) const
{
  return sizeof(T);
}

// ============================================================================
// Class : Buffer
// ============================================================================

// ============================================================================
// Buffer::Buffer
// ============================================================================
template <class T>
Buffer<T>::Buffer(unsigned long _depth, ACE_Allocator *_alloc) 
{
  // Check input.
  ACE_ASSERT(_depth > 0);

  // Use the user specified allocator or the default singleton one.
  this->alloc_ = (_alloc == 0) ? ACE_Allocator::instance() : _alloc;

  // Set buffer depth to 0 in case of ENOMEM error during ACE_ALLOCATOR call.
  this->depth_ = 0;

  // Allocate memory (will return on ENOMEM error).
  ACE_ALLOCATOR(this->base_, (T*)this->alloc_->malloc(_depth * sizeof(T)));

  // Set buffer depth.
  this->depth_ = _depth;
}

// ============================================================================
// Buffer::Buffer
// ============================================================================
template <class T>
Buffer<T>::Buffer(unsigned long _depth, T* _base, ACE_Allocator *_alloc)
{
  // Check input.
  ACE_ASSERT(_depth > 0);

  // Use the user specified allocator or the default singleton one.
  this->alloc_ = (_alloc == 0) ? ACE_Allocator::instance() : _alloc;

  // Set buffer depth to 0 in case of ENOMEM error during ACE_ALLOCATOR call.
  this->depth_ = 0;

  // Allocate memory (will return on ENOMEM error).
  ACE_ALLOCATOR(this->base_, (T*)this->alloc_->malloc(_depth * sizeof(T)));

  // Set buffer depth.
  this->depth_ = _depth;

  // Copy from source to destination using <Buffer::operator=>.
  *this = _base;
}
// ============================================================================
// Buffer::Buffer
// ============================================================================
template <class T> 
Buffer<T>::Buffer(const Buffer<T>& _src, ACE_Allocator *_alloc)
{
  // Get src allocator.
  this->alloc_ = _src.allocator();
  
  // Set buffer depth to 0 in case of ENOMEM error during ACE_ALLOCATOR call.
  this->depth_ = 0;

  // Allocate memory (will return on ENOMEM error).
  //ACE_ALLOCATOR(this->base_, (T*)this->alloc_->malloc(_src.size()));

  // Set buffer depth.
  this->depth_ = _src.depth();

  // Copy from source to destination using <Buffer::operator=>.
  *this = _src;
}

// ============================================================================
// Buffer::~Buffer
// ============================================================================
template <class T> 
Buffer<T>::~Buffer(void)
{
  // Release memory (it is safe to free a null base addr).
  this->alloc_->free(this->base_);
}

} // namespace ni660Xsl 

#endif // _DATA_T_CPP_

