// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    Data.h
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

#ifndef _DATA_H_
#define _DATA_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/ni660XSLConfig.h>

namespace ni660Xsl {

// ============================================================================
//! The ni660XSL data abstraction base class.  
// ============================================================================
//!  
//! Base class of any ni660XSL or user defined data encapsulated in a Message.
//! 
// ============================================================================
class NI660XSL_EXPORT Data
{
public:

  Data (void);
  
  virtual ~Data (void);

  /**
   * Return next data in the continuation field, 0 otherwise. 
   * The current implementation is not thread safe.
   * @return next Data in the continuation field. 
   */
  Data* cont (void) const
  {
      return this->cont_;
  };
  /**
   * Set next data in the continuation field. 
   * The current implementation is not thread safe.
   * @param cont next Data in the continuation field.
   */
  void cont (Data *cont)
  {
      this->cont_ = cont;
  };

  /**
   * Return the size (in bytes) of the actual data. 
   * Any derivated class must implement this pure virtual method. 
   * @return size of underlying data in bytes.
   */
  virtual size_t size (void) const = 0;

  /**
   * Return the total number of bytes in the continuation field.
   * @return number of bytes in the continuation field.
   */
  size_t total_size (void) const
  {
      return this->size() 
          +
          (this->cont_ ? this->cont_->total_size() : 0);
  };

private:

  /**
   * Continuation field : chains together composite data.
   */
  Data * cont_;

  // = Disallow these operations.
  //--------------------------------------------
  ACE_UNIMPLEMENTED_FUNC(Data& operator= (const Data&))
  ACE_UNIMPLEMENTED_FUNC(Data(const Data&))
};

} // namespace ni660Xsl


#endif // _DATA_H_

