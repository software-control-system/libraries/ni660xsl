// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   SimpleDTMeasurement.h
//
// = AUTHORS
//    S. Minolli 
//
// ============================================================================


#ifndef _SIMPLE_DT_MEASUREMENT_H_
#define _SIMPLE_DT_MEASUREMENT_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/DTMeasurement.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
//!  Perform simple delta time measurement with one counter.
// ============================================================================
//!  
//! The counter counts delta time between two edges on the SOURCE input after the counter has been armed.
//! The counter can be armed via software command (start()) or upon receiving  a start trigger (set_start_trigger()). \n \n
//! Be aware that is only possible to use a start trigger (set_start_trigger()) <b>or</b> a pause trigger(set_pause_trigger())
//! since the GATE is used for both fonctionalities.
//! 
// ============================================================================
class NI660XSL_EXPORT SimpleDTMeasurement: public DTMeasurement
{
public:
	SimpleDTMeasurement(void);
	virtual ~SimpleDTMeasurement(void);
	/**
	* Set the timeout (time to wait for data when entering get_buffer )
	* @param _timeout The time to wait in seconds.
	*/
	virtual void set_timeout(double _timeout);
	/**
	* Set a pause trigger.
	* @param _trigger_source The trigger source.
	* @param _pause_when The state of pause trigger that will pause the acquisition.
	*/
	void set_pause_trigger(std::string _trigger_source, ni::LevelType _pause_when) throw (ni660Xsl::DAQException);
	/**
	* Configure the hardware with all preconfigured parameters.
	*/
	void configure(void) throw (ni660Xsl::DAQException);
	/**
	* Get the current value of the counter in units specified in channel counter (seconds, ticks).
	* @param _timeout The time to wait for the value.
	* @return The current value.
	*/
	double get_current_scaled_value() throw (ni660Xsl::DAQException);
	
private:
    bool use_pause_trig_;
    std::string pause_trigger_source_;
    ni::LevelType pause_when_;
	  double timeout_;
	
	
};

}//namespace


#endif //#ifndef _SIMPLE_DT_MEASUREMENT_H_
