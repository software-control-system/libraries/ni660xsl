// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   PeriodMeasurement.h
//
// = AUTHORS
//    S. Minolli 
//
// ============================================================================


#ifndef _PERIOD_MEASUREMENT_H_
#define _PERIOD_MEASUREMENT_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>

#include <NI660Xsl/InputOperation.h>
#include <NI660Xsl/Channels.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
//!  ni660XSL base class for signal period measurement.
// ============================================================================
//!  
//!  
//! 
// ============================================================================
class NI660XSL_EXPORT PeriodMeasurement: public InputOperation
{
  public:

	  PeriodMeasurement(void);
    virtual ~PeriodMeasurement(void);

    /**
    * Add an an input channel to a period measurement operation. \n
	  * It is only possible to add one channel.
	  * @param _chan The channel to add.
    */
	  virtual void add_input_channel(ni660Xsl::PeriodChan _chan);

    /**
    * Configure the hardware.
    */
	  virtual void configure(void) = 0;

  protected:
	  std::string chan_name_;
	  ni::EdgeType edge_;
    ni::TimeUnitsType units_;
	  int added_channels;
    std::string input_terminal_;
	  
    // Configure the input channel.
    virtual void configure_channel(void) throw (ni660Xsl::DAQException);
};

}//namespace


#endif //#ifndef _PERIOD_MEASUREMENT_H_
