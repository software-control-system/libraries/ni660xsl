// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   PositionMeasurement.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================


#ifndef _POSITION_MEASUREMENT_H_
#define _POSITION_MEASUREMENT_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <vector>

#include <NI660Xsl/InputOperation.h>
#include <NI660Xsl/Channels.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
    typedef std::vector<ni660Xsl::LinearEncoderChan> LinearEncoderChans;
    typedef std::vector<ni660Xsl::AngularEncoderChan> AngularEncoderChans;
// ============================================================================
//!  ni660XSL base class for position measurement.
// ============================================================================
//!  
//! 
//! 
// ============================================================================
    class NI660XSL_EXPORT PositionMeasurement: public InputOperation
    {
    public:
	PositionMeasurement(void);
	virtual ~PositionMeasurement(void); 
	/**
	* Add an input channel to use a linear encoder. \n
				* Only one encoder (linear or angular) can be added to an operation.
				* @param _chan The channel to add.
	*/
	virtual void add_linear_encoder(ni660Xsl::LinearEncoderChan _chan) throw (ni660Xsl::DAQException);
	/**
	* Add an input channel to use a angular encoder. \n
				* Only one encoder (linear or angular) can be added to an operation.
				* @param _chan The channel to add.
	*/
	virtual void add_angular_encoder(ni660Xsl::AngularEncoderChan _chan) throw (ni660Xsl::DAQException);
	/**
	* Configure hardware with all predefined parameters.
	*/
	virtual void configure (void) = 0;
    protected:
	//vector of linear chan
	LinearEncoderChans lin_chans_;
	//vector of angular chan
	AngularEncoderChans ang_chans_;
	int added_channels;
	virtual void configure_channel(void) throw (ni660Xsl::DAQException);

    };
    
}//namespace

#endif // !defined(_POSITION_MEASUREMENT_H_)
