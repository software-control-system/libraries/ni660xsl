// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    CounterBasedOperation.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================



#ifndef _COUNTER_BASED_OPERATION_H_
#define _COUNTER_BASED_OPERATION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>

#include <NI660Xsl/NI.h>
#include <NI660Xsl/Export.h>
#include <NI660Xsl/SystemProperties.h>
#include <NI660Xsl/Exception.h>


// ============================================================================
//!  Namespace for fonctionnalities belonging to ni660X Support Library.
// ============================================================================
namespace ni660Xsl
{
// ============================================================================
//!  The ni660XSL base class for all counter based operations.
// ============================================================================
//!  
//! Implements all common methods for counter based operations.
//! The minimum steps used for all counter based operations are: 
//! - init()
//! - configure()
//! - start()
//! - stop()
//! - release() \n \n
//! Then all inherited classes will add their own methods to this steps.
// ============================================================================  
    class NI660XSL_EXPORT CounterBasedOperation  
    {
			
    public:
			/**
			* The possible states of the Counter Based Operation.
			*/
	enum 
	{
	    UNKNOWN,
	    INIT,
	    STANDBY,
	    RUNNING    
	}states;
			
			CounterBasedOperation(void);
			virtual ~CounterBasedOperation(void);
			/**
			* Initialize the operation.
			*/
			virtual void init () throw (ni660Xsl::DAQException);
			/**
			* Release the initialized operation.
			*/
			virtual void release (void) throw (ni660Xsl::DAQException);
			/**
			* Start the operation.
			*/
			virtual void start (void) throw (ni660Xsl::DAQException);
			
			/**
			* Stop the operation.
			*/
			virtual void stop (void) throw (ni660Xsl::DAQException);
			/**
			* Abort the operation.
			*/
			virtual void abort (void) throw (ni660Xsl::DAQException);
			/**
			* Get current state of the operation.
			* @return The current state. 
			*/
			int state (void) const
			{
				return this->state_;
			};
			/**
			* Configure hardware with all predefined parameters
			*/
			virtual void configure (void) = 0;
			/**
			* Get warning string.
			* @return The string corresponding to the last warning ocurred.
			*/
			std::string get_warn_string (void)
			{
				return warn_;
			};
			/**
			* @return true if a warning occured.
			*/
			bool warn_occured(void);
			
    protected:
			// Handle to the current operation.
			ni::DAQTaskHandle task_handle_; 
			// The current state of the Counter Based Operation
			int state_;
			// Prepare the task so that the operation is ready to start.	
			virtual void prepare_task(void) throw (ni660Xsl::DAQException);
			// Return a string corresponding to the last error occured.
			std::string get_string_error(void) throw (ni660Xsl::DAQException);
			//
			std::string warn_;
			//
			bool warn_occured_;
			
    };
    
}//namespace

#endif // !defined(_COUNTER_BASED_OPERATION_H_)
