// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   SimplePositionMeasurement.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================


#ifndef _SIMPLE_POSITION_MEASUREMENT_H_
#define _SIMPLE_POSITION_MEASUREMENT_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>

#include <NI660Xsl/InData.h>
#include <NI660Xsl/Exception.h>
#include <NI660Xsl/PositionMeasurement.h>


namespace ni660Xsl
{
// ============================================================================
//!  Perform simple position measurement with one counter.
// ============================================================================
//!  
//! The ni660XSL can perform position measurement on signals from 2 types of motion encoder:
//! - Quadrature encoders: \n
//!	A quadrature encoder can have up to 3 channels: A, B and Z. When channel A leads channel B
//!	in a quadrature cycle, the counter increments. When channel B leads channel A in a quadrature cycle,
//!	the counter decrements. The amount of increments of decrements depends on the type of encoding: X1, X2, or X4
//!	Some quadrature encoders have a third channel, channel Z, which is also referred to as an index channel. 
//!	
//! - Two-pulse encoders: \n
//!	Two-pulse encoding supports 2 channels: channel A and B. A pulse on channel A causes the counter to increment
//!	on its rising edge. A pulse on channel B causes the counter to decrement on its rising edge.
// ============================================================================
	class NI660XSL_EXPORT SimplePositionMeasurement: public PositionMeasurement
	{
	public:
		SimplePositionMeasurement(void);
		virtual ~SimplePositionMeasurement(void);
		/**
		* Set the timeout (time to for data wait when entering get_buffer )
		* @param _timeout The time to wait in seconds.
		*/
		virtual void set_timeout(double _timeout);
		/**
		* Get the current value of the position in units specified in channel counter (degrees, ...).
		* @param _timeout The time to wait for the value.
		* @return The current value.
		*/
		double get_current_scaled_value() throw (ni660Xsl::DAQException);
		/**
		* Get the current value of the counter.
		* @param _timeout The time to wait for the value.
		* @return The current value.
		*/
		unsigned long get_current_raw_value() throw (ni660Xsl::DAQException);
		/**
		* Configure the hardware with all preconfigured parameters.
		*/
		void configure(void) throw (ni660Xsl::DAQException);
	private:
		double timeout_;

		
	};

}//namespace

#endif //#ifndef _SIMPLE_POSITION_MEASUREMENT_H_
