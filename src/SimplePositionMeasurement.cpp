// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   SimplePositionMeasurement.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/SimplePositionMeasurement.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
// SimplePositionMeasurement::SimplePositionMeasurement
// ============================================================================
SimplePositionMeasurement::SimplePositionMeasurement()
:PositionMeasurement(),
timeout_(1.0)
{
	this->data_transfer_ = ni::programmedIO;
	//std::cout<<<<"SimplePositionMeasurement::SimplePositionMeasurement"<<std::endl;
}
// ============================================================================
// SimplePositionMeasurement::~SimplePositionMeasurement
// ============================================================================   
SimplePositionMeasurement::~SimplePositionMeasurement()
{
}
// ============================================================================
// SimplePositionMeasurement::set_timeout
// ============================================================================
void SimplePositionMeasurement::set_timeout(double _timeout)
{
	this->timeout_ = _timeout;
}
// ============================================================================
// SimplePositionMeasurement::get_current_scaled_value
// ============================================================================   
double	SimplePositionMeasurement::get_current_scaled_value() 
    throw (ni660Xsl::DAQException)
{
    double value;
    int err = DAQmxReadCounterScalarF64(this->task_handle_, this->timeout_, &value, NULL);
    if(err < 0)
    {
       this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("get value failed",
	    this->get_string_error(),
	    "SimplePositionMeasurement::get_current_scaled_value",
	    err);
    }
    else if (err > 0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
    return value;

}

// ============================================================================
// SimplePositionMeasurement::get_current_raw_value
// ============================================================================ 
unsigned long  SimplePositionMeasurement::get_current_raw_value() 
    throw (ni660Xsl::DAQException)
{
    unsigned long value;
    int err = DAQmxReadCounterScalarU32(this->task_handle_, this->timeout_, &value, NULL);
    if(err < 0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("get value failed",
	    this->get_string_error(),
	    "SimplePositionMeasurement::get_current_raw_value",
	    err);
    }
    else if (err > 0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
    return value;
}
    
// ============================================================================
// SimplePositionMeasurement::configure
// ============================================================================   
void SimplePositionMeasurement::configure(void) throw (ni660Xsl::DAQException)
{
    //----------------------config channel-------------------------------------
    try
    {
	this->configure_channel();
    }
    catch (const DAQException&)
    {
	throw;
    }
    catch (...) 
    {
	throw DAQException();
    }
    //---------------------config autostart------------------------------------
    //to call read() does not start the counter
    int err = DAQmxSetReadAutoStart(this->task_handle_, FALSE);
    if(err < 0)
    {
	throw ni660Xsl::DAQException("configuration failed",
	    this->get_string_error(),
	    "SimplePositionMeasurement::configure",
	    err);
    }
    else if (err > 0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
	//-------------------------config trigger------------------------------------------
    if (this->use_start_trig_)
    {
	//config trigger to be arm start trig
	int err = DAQmxSetArmStartTrigType(this->task_handle_, DAQmx_Val_DigEdge);
	if(err < 0)
	{
	    throw ni660Xsl::DAQException("configuration failed",
		this->get_string_error(),
		"SimplePositionMeasurement::configure",
		err);
	}
	else if(err > 0)
	{
	    this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
	}
	
	//config trigger pin
	const char* s =  this->start_trigger_source_.c_str();
	err = DAQmxSetDigEdgeArmStartTrigSrc(this->task_handle_, s);
	if(err < 0)
	{
	    throw ni660Xsl::DAQException("configuration failed",
		this->get_string_error(),
		"SimplePositionMeasurement::configure",
		err);
	}
	else if(err > 0)
	{
	    this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
	}
	
	//config edge 
	err = DAQmxSetDigEdgeArmStartTrigEdge(this->task_handle_, this->trig_active_edge_);
	if(err < 0)
	{
	    throw ni660Xsl::DAQException("configuration failed",
		this->get_string_error(),
		"SimplePositionMeasurement::configure",
		err);
	}
	else if(err > 0)
	{
	    this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
	}
    }
	//---------------------config read mode-----------------------------------------------
	err = DAQmxSetReadWaitMode(this->task_handle_, this->read_mode_);
	if(err < 0)
    {
	throw ni660Xsl::DAQException("configuration failed",
	    this->get_string_error(),
	    "SimplePositionMeasurement::configure",
	    err);
    }
    else if(err > 0)
    {
	this->warn_ = this->get_string_error();
       this->warn_occured_ = true;
    }
		//-------------------------config tc------------------------------------------
	if( this->terminal_.size() != 0)
	{ 
		const char* term =  this->terminal_.c_str();
		err = DAQmxSetExportedCtrOutEventOutputTerm(this->task_handle_, term);
		if(err < 0)
		{
			throw ni660Xsl::DAQException("configuration failed",
				this->get_string_error(),
				"SimplePositionMeasurement::configure",
	err);
		}
		else if(err > 0)
		{
			this->warn_ = this->get_string_error();
			this->warn_occured_ = true;
		}
		err = DAQmxSetExportedCtrOutEventOutputBehavior(this->task_handle_, this->behavior_);
		if(err < 0)
		{
			throw ni660Xsl::DAQException("configuration failed",
				this->get_string_error(),
				"SimplePositionMeasurement::configure",
	err);
		}
		else if(err > 0)
		{
			this->warn_ = this->get_string_error();
			this->warn_occured_ = true;
		}
	}
    //---------------------verify parameters------------------------------------
    try
    {
	this->prepare_task();
    }
    catch (const DAQException&)
    {
	throw;
    }
    catch (...) 
    {
	throw DAQException();
    }
}

}//namespace
