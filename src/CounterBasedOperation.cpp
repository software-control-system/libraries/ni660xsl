// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660XSupport Library
//
// = FILENAME
//    CounterBasedOperation.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================

//#include <iostream>

#include <NI660Xsl/CounterBasedOperation.h>
#include <NI660Xsl/Exception.h>
#include <NI660Xsl/Export.h>

namespace ni660Xsl
{
// ============================================================================
// CounterBasedOperation::CounterBasedOperation
// ============================================================================
CounterBasedOperation::CounterBasedOperation()
: state_(UNKNOWN),
warn_(""),
warn_occured_(false)
{
  //  std::cout<<"CounterBasedOperation::CounterBasedOperation<-"<<std::endl;	
  this->task_handle_ = 0;
 //   std::cout<<"CounterBasedOperation::CounterBasedOperation->"<<std::endl;
		
}
// ============================================================================
// CounterBasedOperation::~CounterBasedOperation
// ============================================================================
CounterBasedOperation::~CounterBasedOperation()
{
	
    //std::cout<<"CounterBasedOperation::~CounterBasedOperation<-"<<std::endl;

   // std::cout<<"CounterBasedOperation::~CounterBasedOperation->"<<std::endl;

}
// ============================================================================
// CounterBasedOperation::init
// ============================================================================
void CounterBasedOperation::init()
    throw (ni660Xsl::DAQException)
{

    //std::cout<<"CounterBasedOperation::init<-"<<std::endl;

    
    if(this->state_ != UNKNOWN)
    {
	 throw ni660Xsl::DAQException("initialization failed",
	    "this task has already been configured",
	    "CounterBasedOperation::init");
    }

    //create task
    int err = DAQmxCreateTask("",&this->task_handle_);
    if(err < 0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("initialization failed",
	    this->get_string_error(),
		"CounterBasedOperation::init",
		err);
    }
    else if (err > 0)
    {
		this->warn_ = this->get_string_error();
		this->warn_occured_ = true;
    }
    this->state_ = INIT;

    //std::cout<<"CounterBasedOperation::init->"<<std::endl;

}
// ============================================================================
// CounterBasedOperation::release
// ============================================================================
void CounterBasedOperation::release(void) throw (ni660Xsl::DAQException)
{

	//std::cout<<"CounterBasedOperation::release<-"<<std::endl;

    
	if(this->state_ == RUNNING)
	{
		try
		{
			this->abort();
		}
		catch (const DAQException&)
		{
			throw;
		}
		catch (...) 
		{
			throw DAQException();
		}
	}

	int err = DAQmxClearTask (this->task_handle_);
	if(err<0)
	{
		this->state_ = UNKNOWN;
		throw ni660Xsl::DAQException("release failed",
			this->get_string_error(),
			"CounterBasedOperation::init",
			err);
	}
	else if (err > 0)
	{
		this->warn_ = this->get_string_error();
		this->warn_occured_ = true;
	}
	this->state_ = UNKNOWN;
	//	std::cout<<"CounterBasedOperation::release->"<<std::endl;

}
// ============================================================================
// CounterBasedOperation::start
// ============================================================================
void CounterBasedOperation::start(void)
throw (ni660Xsl::DAQException)
{

   // std::cout<<"CounterBasedOperation::start<-"<<std::endl;

    
    if(this->state_ == RUNNING)
    {
	throw ni660Xsl::DAQException("start failed",
	    "The task is already running",
	    "CounterBasedOperation::start");
    }
    /*else if(this->state_ != STANDBY)
    {
	throw ni660Xsl::DAQException("start failed",
	    "The task is not initialized and/or not configured",
	    "CounterBasedOperation::start");
    }*/

    int err = DAQmxStartTask(this->task_handle_);
    if(err<0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("start failed",
	    this->get_string_error(),
	    "CounterBasedOperation::start",
	    err);
    }
    else if (err > 0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
    
    this->state_ = RUNNING;
    

    //std::cout<<"CounterBasedOperation::start->"<<std::endl;

}

// ============================================================================
// CounterBasedOperation::stop
// ============================================================================
void CounterBasedOperation::stop(void)
throw (ni660Xsl::DAQException)
{	

    //std::cout<<"CounterBasedOperation::stop<-"<<std::endl;

    
    if(this->state_ == RUNNING)
    {
	int err = DAQmxStopTask(this->task_handle_);
	if(err == -200141 || err == -200222) 
	{
	    //ignore overrun error (for an unknown reason this driver detect 
	    //an overrun while stopping)
	}
	else if(err < 0)
	{
	    this->state_ = UNKNOWN;
	    throw ni660Xsl::DAQException("stop failed",
		this->get_string_error(),
		"CounterBasedOperation::stop",
		err);
	}
	else if (err > 0)
	{
			this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
	}
    }
    this->state_ = STANDBY;


    //std::cout<<"CounterBasedOperation::stop->"<<std::endl;

}
// ============================================================================
// CounterBasedOperation::abort
// ============================================================================
void CounterBasedOperation::abort(void)
    throw (ni660Xsl::DAQException)
{

    //std::cout<<"CounterBasedOperation::abort<-"<<std::endl;

    
    int err = DAQmxTaskControl(this->task_handle_, DAQmx_Val_Task_Abort);
    if(err<0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("abort failed",
	    this->get_string_error(),
	    "CounterBasedOperation::abort",
	    err);
    }
    else if (err > 0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
    
    this->state_ = UNKNOWN;
    

    //std::cout<<"CounterBasedOperation::abort->"<<std::endl;

}
// ============================================================================
// CounterBasedOperation::prepare_task
// ============================================================================
void CounterBasedOperation::prepare_task(void)
throw (ni660Xsl::DAQException)
{

    //std::cout<<"CounterBasedOperation::prepare_task<-"<<std::endl;

    
    if(this->state_ != INIT)
    {
	 throw ni660Xsl::DAQException("configuration failed",
	    "the task be initialized before",
	    "CounterBasedOperation::prepare_task");
    }

    int err = DAQmxTaskControl(this->task_handle_, DAQmx_Val_Task_Verify);
    if(err<0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("user configuration invalid",
	    this->get_string_error(),
	    "CounterBasedOperation::prepare_task",
	    err);
    }
    
    
    
    err = DAQmxTaskControl(this->task_handle_, DAQmx_Val_Task_Commit);
    if(err<0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("configuration failed",
	    this->get_string_error(),
	    "CounterBasedOperation::prepare_task",
	    err);
    }
    else if (err>0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
    
    this->state_ = STANDBY;
    

    //std::cout<<"CounterBasedOperation::prepare_task->"<<std::endl;

}
// ============================================================================
// CounterBasedOperation::get_string_error
// ============================================================================
std::string CounterBasedOperation::get_string_error(void)
{
  char errBuff[2048] = {'\0'};
  DAQmxGetExtendedErrorInfo(errBuff,2048);
	std::string s = errBuff;
	return s;
}
// ============================================================================
// CounterBasedOperation::warn_occured
// ============================================================================
bool CounterBasedOperation::warn_occured()
{
    if (warn_occured_)
    {
	warn_occured_ = false;
	return true;
    }
    else
    {
	return false;
    }
}

}//namespace
