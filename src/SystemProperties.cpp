// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    SystemProperties.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================
// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/NI.h>
#include <NI660Xsl/SystemProperties.h>
#include <NI660Xsl/Exception.h>
#include <NI660Xsl/Export.h>

#include <sstream>
	
	
namespace ni660Xsl
{
bool SystemProperties::warn_occured_ = false;

std::string SystemProperties::warn_ = "";
// ============================================================================
// SystemProperties::SystemProperties
// ============================================================================
SystemProperties::SystemProperties()
{}
// ============================================================================
// SystemProperties::~SystemProperties
// ============================================================================
SystemProperties::~SystemProperties()
{}
// ============================================================================
// SystemProperties::get_driver_version
// ============================================================================
std::string SystemProperties::get_driver_version(void)
{
  //std::cout<<"SystemProperties::get_driver_version<-"<<std::endl;

  unsigned long maj;

  int err = DAQmxGetSysNIDAQMajorVersion(&maj);

  if(err<0)
  {
    throw ni660Xsl::DAQException("get driver version failed",
                                 SystemProperties::get_string_error(),
                                 "SystemProperties::get_driver_version",
                                 err);
  }
  else if (err > 0)
  {
    SystemProperties::warn_ = SystemProperties::get_string_error();
    SystemProperties::warn_occured_ = true;
  }

  unsigned long min;

  err = DAQmxGetSysNIDAQMinorVersion(&min);

  if(err<0)
  {
    throw ni660Xsl::DAQException("get driver version failed",
                                 SystemProperties::get_string_error(),
                                 "SystemProperties::get_driver_version",
                                 err);
  }
  else if (err > 0)
  {
    SystemProperties::warn_ = SystemProperties::get_string_error();
    SystemProperties::warn_occured_ = true;
  }

  std::ostringstream oss;
  oss << maj << "." << min << std::ends;
  return oss.str();

  /*
	char s_maj[1];
	itoa( maj, s_maj, 10);
	char s_min[1];
	itoa( min, s_min, 10);

	std::string s1 = (std::string)s_maj;
	std::string s2 = (std::string)s_min;
	std::string version = (std::string)s_maj+"."+(std::string)s_min;
	
	//std::cout<<"SystemProperties::get_driver_version->"<<std::endl;
    
	return version;
  */
}
// ============================================================================
// SystemProperties::get_product_type
// ============================================================================
std::string SystemProperties::get_product_type(std::string dev)
{
    //std::cout<<"SystemProperties::get_product_type<-"<<std::endl;

  const char* dev_ = dev.c_str();


  char type[10] ;
  int err = DAQmxGetDevProductType(dev_, type, 10);
  if(err<0)
  {
    throw ni660Xsl::DAQException("get driver version failed",
                                 get_string_error(),
                                 "SystemProperties::get_product_type",
                                 err);
  }
  else if (err > 0)
  {
    SystemProperties::warn_ = SystemProperties::get_string_error();
    SystemProperties::warn_occured_ = true;
  }
  //std::cout<<"SystemProperties::get_product_type->"<<std::endl;
  return type;
}
// ============================================================================
// SystemProperties::get_all_devices_names
// ============================================================================
std::string SystemProperties::get_all_devices_names(void)
{
    char devices[10000];
    int err = DAQmxGetSysDevNames(devices, 10000);
    if(err<0)
    {
	throw ni660Xsl::DAQException("get driver version failed",
	                             get_string_error(),
	                             "SystemProperties::get_all_devices_names",
	                             err);
    }
    else if (err > 0)
    {
	SystemProperties::warn_ = SystemProperties::get_string_error();
	SystemProperties::warn_occured_ = true;
    }
    return devices;
    
}
// ============================================================================
// SystemProperties::reset_device
// ============================================================================
void SystemProperties::reset_device(std::string _dev)
    throw (ni660Xsl::DAQException)
{
    //std::cout<<<<"SystemProperties::::reset_device<-"<<std::endl;

  const char* dev_ = _dev.c_str();
  int err = DAQmxResetDevice(dev_);
  if(err<0)
  {
    throw ni660Xsl::DAQException("reset device failed",
                                 get_string_error(),
                                 "CounterBasedOperation::reset_device",
                                 err);
  }
  else if (err > 0)
  {
    warn_ = get_string_error();
    warn_occured_ = true;
  }


    //std::cout<<<<"SystemProperties::::reset_device->"<<std::endl;
}
// ============================================================================
// SystemProperties::route_terminals
// ============================================================================*
void SystemProperties::route_terminals(std::string _source, std::string _destination, bool _invert_polarity)
    throw (ni660Xsl::DAQException)
{
  const char* source =  _source.c_str();
  const char* destination  =	_destination.c_str();
  long invert = DAQmx_Val_DoNotInvertPolarity;
  if (_invert_polarity)
    invert = DAQmx_Val_InvertPolarity;
  int err = DAQmxConnectTerms(source, destination, invert);
  if(err<0)
  {
    throw ni660Xsl::DAQException("routing terminals",
                                 get_string_error(),
                                 "CounterBasedOperation::route_terminals",
                                 err);
  }
  else if (err > 0)
  {
    warn_ = get_string_error();
    warn_occured_ = true;
  }
}
// ============================================================================
// SystemProperties::get_string_error
// ============================================================================
std::string SystemProperties::get_string_error(void)
{
	long size = DAQmxGetExtendedErrorInfo(NULL,0);
	char* string_err = new char[size];
	DAQmxGetExtendedErrorInfo(string_err, size);
	std::string s = string_err;
	delete[]	string_err;
	return s;
}
// ============================================================================
//  SystemProperties::warn_occured
// ============================================================================
bool SystemProperties::warn_occured(void)
{
  if (SystemProperties::warn_occured_)
  {
    SystemProperties::warn_occured_ = false;
    return true;
  }
  else
  {
    return false;
  }
}
// ============================================================================
//  SystemProperties::get_warn_string
// ============================================================================
std::string SystemProperties::get_warn_string (void)
{
  return SystemProperties::warn_;
}





}//namespace
