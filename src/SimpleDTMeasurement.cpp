// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   SimpleDTMeasurement.cpp
//
// = AUTHORS
//    S. Minolli 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/SimpleDTMeasurement.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
// SimpleDTMeasurement::SimpleDTMeasurement
// ============================================================================
SimpleDTMeasurement::SimpleDTMeasurement()
:DTMeasurement(),
 use_pause_trig_(false),
 pause_trigger_source_("/Dev1/PFI38"),
 pause_when_(ni::high),
 timeout_(1.0)
{
	this->data_transfer_ = ni::programmedIO;
	//std::cout<<<<"SimpleDTMeasurement::SimpleDTMeasurement"<<std::endl;
}
// ============================================================================
// SimpleDTMeasurement::~SimpleDTMeasurement
// ============================================================================
SimpleDTMeasurement::~SimpleDTMeasurement()
{
	//std::cout<<<<"SimpleDTMeasurement::~SimpleDTMeasurement"<<std::endl;
}
// ============================================================================
// SimpleDTMeasurement::set_pause_trigger
// ============================================================================
void SimpleDTMeasurement::set_pause_trigger(std::string _trigger_source, ni::LevelType _pause_when)
  throw (ni660Xsl::DAQException)
{
  this->pause_trigger_source_ = _trigger_source;
  this->pause_when_ = _pause_when;
  this->use_pause_trig_ = true;
}
// ============================================================================
// SimpleDTMeasurement::set_timeout
// ============================================================================
void SimpleDTMeasurement::set_timeout(double _timeout)
{
	this->timeout_ = _timeout;
}
// ============================================================================
// SimpleDTMeasurement::get_current_scaled_value
// ============================================================================   
double	SimpleDTMeasurement::get_current_scaled_value() 
  throw (ni660Xsl::DAQException)
{
  double value;
  int err = DAQmxReadCounterScalarF64(this->task_handle_, this->timeout_, &value, NULL);
  if(err < 0)
  {
    this->state_ = UNKNOWN;
    throw ni660Xsl::DAQException("get value failed",
						this->get_string_error(),
						"SimpleDTMeasurement::get_current_scaled_value",
						err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }

  return value;
}
// ============================================================================
// SimpleDTMeasurement::configure
// ============================================================================
void SimpleDTMeasurement::configure(void) throw (ni660Xsl::DAQException)
{
  try
  {
    this->configure_channel();
  }
  catch (const DAQException&)
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }
  
  //-------------------------config trigger------------------------------------------
  if (this->use_start_trig_)
  {
    //config trigger to be arm start trig
    int err = DAQmxSetArmStartTrigType(this->task_handle_, DAQmx_Val_DigEdge);

    if(err<0)
    {
      throw ni660Xsl::DAQException(
        "configuration failed",
        this->get_string_error(),
        "SimpleDTMeasurement::configure",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
    
    //config trigger pin
    const char* s =  this->start_trigger_source_.c_str();
    err = DAQmxSetDigEdgeArmStartTrigSrc(this->task_handle_, s);

    if(err<0)
    {
      throw ni660Xsl::DAQException(
        "configuration failed",
        this->get_string_error(),
        "SimpleDTMeasurement::configure",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }

    //config edge 
    err = DAQmxSetDigEdgeArmStartTrigEdge(this->task_handle_, this->trig_active_edge_);
    if(err<0)
    {
      throw ni660Xsl::DAQException(
        "configuration failed",
        this->get_string_error(),
        "SimpleDTMeasurement::configure",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
  }

  //-------------------------config tc------------------------------------------
  if( this->terminal_.size() != 0)
  { 
    const char* term =  this->terminal_.c_str();
    int err = DAQmxSetExportedCtrOutEventOutputTerm(this->task_handle_, term);

    if(err < 0)
    {
      throw ni660Xsl::DAQException(
        "configuration failed",
        this->get_string_error(),
        "SimpleDTMeasurement::configure",
        err);
    }
    else if(err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }

    err = DAQmxSetExportedCtrOutEventOutputBehavior(this->task_handle_, this->behavior_);

    if(err < 0)
    {
      throw ni660Xsl::DAQException(
        "configuration failed",
        this->get_string_error(),
        "SimpleDTMeasurement::configure",
        err);
    }
    else if(err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
  }

  //---------------------config autostart------------------------------------------------
  //to call read() does not start the counter
  int err = DAQmxSetReadAutoStart(this->task_handle_, FALSE);

  if(err < 0)
  {
    throw ni660Xsl::DAQException(
      "configuration failed",
      this->get_string_error(),
      "SimpleDTMeasurement::configure",
      err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }

  //---------------------config read mode-----------------------------------------------
  err = DAQmxSetReadWaitMode(this->task_handle_, this->read_mode_);

  if(err < 0)
  {
    throw ni660Xsl::DAQException(
      "configuration failed",
      this->get_string_error(),
      "SimpleDTMeasurement::configure",
      err);
  }
  else if(err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
  
  //-------------------
  try
  {
    this->prepare_task();
  }
  catch (const DAQException&)
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }
}

}//namespace
