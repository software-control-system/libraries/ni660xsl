// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    FinitePulseTrainGeneration.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/FinitePulseTrainGeneration.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
// FinitePulseTrainGeneration::FinitePulseTrainGeneration
// ============================================================================
FinitePulseTrainGeneration::FinitePulseTrainGeneration()
:PulseTrainGeneration(),
nb_pulses_(10)
{
}
// ============================================================================
// FinitePulseTrainGeneration::~FinitePulseTrainGeneration
// ============================================================================
FinitePulseTrainGeneration::~FinitePulseTrainGeneration()
{
}
// ============================================================================
// FinitePulseTrainGeneration::set_nb_pulses
// ============================================================================
void FinitePulseTrainGeneration::set_nb_pulses(int _nb_pulses)
{
    this->nb_pulses_ = _nb_pulses;
}

// ============================================================================
// FinitePulseTrainGeneration::configure
// ============================================================================
void FinitePulseTrainGeneration::configure(void) throw (ni660Xsl::DAQException)
{
    //std::cout<<"FinitePulseTrainGeneration::configure<-"<<std::endl;
    
    //---------------------channels config---------------------------------
    try
    {
        this->configure_channels();
    }
    catch (const DAQException&)
    {
        throw;
    }
    catch (...) 
    {
        throw DAQException();
    }
    
    //---------------------timing config-----------------------------------
    //std::cout<<<<"\t configure timing"<<std::endl;
    int err = DAQmxCfgImplicitTiming(this->task_handle_,ni::finite,this->nb_pulses_); 
    if(err<0)
    {
        throw ni660Xsl::DAQException("configuration failed",
            this->get_string_error(),
            "FinitePulseTrainGeneration::configure");
    }
    else if (err > 0)
    {
        this->warn_ = this->get_string_error();
        this->warn_occured_ = true;
    }
    
    //---------------------trigger config----------------------------------
    if(this->trig_mode_ == ni::start_trigger)
    {
        // //std::cout<<"\t configure start trigger"<<std::endl;
        
        const char* s =  this->start_trigger_source_.c_str();
        err = DAQmxCfgDigEdgeStartTrig(this->task_handle_, s, this->active_edge_);
        if(err < 0)
        {
            throw ni660Xsl::DAQException("configuration failed",
                this->get_string_error(),
                "FinitePulseTrainGeneration::configure",
                err);
        }
        else if (err > 0)
        {
            this->warn_ = this->get_string_error();
            this->warn_occured_ = true;
        }
    }
    
    //-----------get the current task ready to run-------------------------------
    try
    {
        this->prepare_task();
    }
    catch (const DAQException&)
    {
        throw;
    }
    catch (...) 
    {
        throw DAQException();
    }
    
    //std::cout <<"FinitePulseTrainGeneration::configure->"<<std::endl;
}

}//namespace
