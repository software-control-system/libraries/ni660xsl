// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   SimpleEventCounting.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/SimpleEventCounting.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
// SimpleEventCounting::SimpleEventCounting
// ============================================================================
SimpleEventCounting::SimpleEventCounting()
:EventCounting(),
use_pause_trig_(false),
 pause_trigger_source_("/Dev1/PFI38"),
 pause_when_(ni::high),
 timeout_(1.0)
{
	this->data_transfer_ = ni::programmedIO;
	//std::cout<<<<"SimpleEventCounting::SimpleEventCounting"<<std::endl;
}
// ============================================================================
// SimpleEventCounting::~SimpleEventCounting
// ============================================================================
SimpleEventCounting::~SimpleEventCounting()
{
	//std::cout<<<<"SimpleEventCounting::~SimpleEventCounting"<<std::endl;
}
// ============================================================================
// SimpleEventCounting::set_pause_trigger
// ============================================================================
void SimpleEventCounting::set_pause_trigger(std::string _trigger_source, ni::LevelType _pause_when)
    throw (ni660Xsl::DAQException)
{
   /* if(this->trig_mode_ == ni::start_trigger)
    {
	throw ni660Xsl::DAQException("impossible to set pause trigger",
	    "The trigger is already used as a start trigger",
	    "InputOperation::set_start_trigger");
    }*/
    this->pause_trigger_source_ = _trigger_source;
    this->pause_when_ = _pause_when;
    this->use_pause_trig_ = true;

}
// ============================================================================
// SimpleEventCounting::set_timeout
// ============================================================================
void SimpleEventCounting::set_timeout(double _timeout)
{
	this->timeout_ = _timeout;
}
// ============================================================================
// SimpleEventCounting::get_current_raw_value
// ============================================================================
unsigned long SimpleEventCounting::get_current_raw_value()
throw (ni660Xsl::DAQException)
{
    unsigned long value;
    int err = DAQmxReadCounterScalarU32(this->task_handle_, this->timeout_, &value, NULL);
    if(err < 0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("get current value failed",
	    this->get_string_error(),
	    "SimpleEventCounting::get_current_value",
	    err);
    }
    else if (err > 0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
    
    return value;
}
// ============================================================================
// SimpleEventCounting::configure
// ============================================================================
void SimpleEventCounting::configure(void) throw (ni660Xsl::DAQException)
{
  try
  {
    this->configure_channel();
  }
  catch (const DAQException&)
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }
  
  //-------------------------config trigger------------------------------------------
  if (this->use_pause_trig_)
  {
    int err = DAQmxSetTrigAttribute(this->task_handle_,DAQmx_PauseTrig_Type,DAQmx_Val_DigLvl);
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "SimpleEventCounting::configure",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
    
    //config trigger src
    const char* s =  this->pause_trigger_source_.c_str();
    err = DAQmxSetTrigAttribute(this->task_handle_,DAQmx_DigLvl_PauseTrig_Src,s);
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "SimpleEventCounting::configure",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
    //set pause when high or low
    err = DAQmxSetTrigAttribute (this->task_handle_, DAQmx_DigLvl_PauseTrig_When,this->pause_when_);
    if(err<0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "SimpleEventCounting::configure",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
  }
  if (this->use_start_trig_)
  {
    //config trigger to be arm start trig
    int err = DAQmxSetArmStartTrigType(this->task_handle_, DAQmx_Val_DigEdge);
    if(err<0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "SimpleEventCounting::configure",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
    
    //config trigger pin
    const char* s =  this->start_trigger_source_.c_str();
    err = DAQmxSetDigEdgeArmStartTrigSrc(this->task_handle_, s);
    if(err<0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "SimpleEventCounting::configure",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
    //config edge 
    err = DAQmxSetDigEdgeArmStartTrigEdge(this->task_handle_, this->trig_active_edge_);
    if(err<0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "SimpleEventCounting::configure",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
  }
  //------------------------config timing-----------------------------------------------
  /* err = DAQmxCfgSampClkTiming(this->task_handle_, source, rate, active_edge, sample_mode,1000);
  if(err<0 || err>0)
  {
  throw ni660Xsl::DAQException("configuration failed",
  this->get_string_error(),
  "SimpleEventCounting::configure");
    }*/
  //-------------------------config tc------------------------------------------
  if( this->terminal_.size() != 0)
  { 
    const char* term =  this->terminal_.c_str();
    int err = DAQmxSetExportedCtrOutEventOutputTerm(this->task_handle_, term);;
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "SimpleEventCounting::configure",
        err);
    }
    else if(err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
    err = DAQmxSetExportedCtrOutEventOutputBehavior(this->task_handle_, this->behavior_);
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "SimpleEventCounting::configure",
        err);
    }
    else if(err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
  }
  //---------------------config autostart------------------------------------------------
  //to call read() does not start the counter
  int err = DAQmxSetReadAutoStart(this->task_handle_, FALSE);
  if(err < 0)
  {
    throw ni660Xsl::DAQException("configuration failed",
      this->get_string_error(),
      "SimpleEventCounting::configure",
      err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
  //---------------------config read mode-----------------------------------------------
  err = DAQmxSetReadWaitMode(this->task_handle_, this->read_mode_);
  if(err < 0)
  {
    throw ni660Xsl::DAQException("configuration failed",
      this->get_string_error(),
      "BufferedEventCounting::configure",
      err);
  }
  else if(err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
  
  //-------------------
  try
  {
    this->prepare_task();
  }
  catch (const DAQException&)
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }
}

}//namespace
