// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    FinitePulseTrainGenerator.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/PulseTrainGeneration.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
// PulseTrainGeneration::PulseTrainGeneration
// ============================================================================ 
   PulseTrainGeneration::PulseTrainGeneration()
	:OutputOperation(),
	trig_mode_(ni::no_trigger),
	start_trigger_source_(""),
	active_edge_(ni::rising_edge)
    {
	//std::cout<<<<"PulseTrainGeneration::PulseTrainGeneration"<<std::endl;
    }
// ============================================================================
// PulseTrainGeneration::~PulseTrainGeneration
// ============================================================================ 
	PulseTrainGeneration::~PulseTrainGeneration()
	{
		//std::cout<<<<"PulseTrainGeneration::~PulseTrainGeneration"<<std::endl;
	}

// ============================================================================
// PulseTrainGeneration::set_start_trigger
// ============================================================================
  void PulseTrainGeneration::set_start_trigger(std::string _trigger_source, ni::EdgeType _active_edge)
	throw (ni660Xsl::DAQException)
  {
      //std::cout<<<<"PulseTrainGeneration::set_start_trigger<-"<<std::endl;
      if(this->trig_mode_ == ni::pause_trigger)
      {
	  throw ni660Xsl::DAQException("impossible to set start trigger",
	    "The trigger is already used as a pause trigger",
	    "PulseTrainGeneration::set_start_trigger");
      }
      
      this->trig_mode_ = ni::start_trigger;
      this->start_trigger_source_ = _trigger_source;
      this->active_edge_ = _active_edge;
      
      //std::cout<<<<"PulseTrainGeneration::set_start_trigger->"<<std::endl;
      
  }


}//namespace
