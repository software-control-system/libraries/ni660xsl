// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   DTMeasurement.cpp
//
// = AUTHORS
//    S. Minolli 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/DTMeasurement.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
  // ============================================================================
  // DTMeasurement::DTMeasurement
  // ============================================================================
  DTMeasurement::DTMeasurement()
    :InputOperation(),
    added_channels(0),
    chan_name_("/Dev1/ctr0"),
    first_edge_(ni::rising_edge),
    second_edge_(ni::falling_edge),
    units_(ni::seconds),
    input_terminal_("")
  {
    //std::cout<<<<"DTMeasurement::DTMeasurement"<<std::endl;
  }
  // ============================================================================
  // DTMeasurement::~DTMeasurement
  // ============================================================================
  DTMeasurement::~DTMeasurement()
  {
    //std::cout<<<<"DTMeasurement::~DTMeasurement"<<std::endl;
  }
  // ============================================================================
  // DTMeasurement::add_input_channel
  // ============================================================================
  void DTMeasurement::add_input_channel(ni660Xsl::DeltaTimeChan _chan)
    throw (ni660Xsl::DAQException)
  {
    //std::cout<<<<"DTMeasurement::add_input_channel<-"<<std::endl;
    //for a input task, it is only possible to add one channel

    /*  if(added_channels >= 1)

    {

    throw ni660Xsl::DAQException("add channel failed",
    "only one channel can be used",
    "DTMeasurement::add_input_channel");
    }*/
    added_channels++;

    this->chan_name_ = _chan.chan_name;
    this->first_edge_ = _chan.first_edge;
    this->second_edge_ = _chan.second_edge;
    this->units_ = _chan.units;
    this->input_terminal_ = _chan.input_terminal;
    //std::cout<<<<"DTMeasurement::add_input_channel->"<<std::endl;

  }
  // ============================================================================
  // DTMeasurement::configure_channel
  // ============================================================================
  void DTMeasurement::configure_channel(void) 
    throw (ni660Xsl::DAQException)
  {
    //std::cout<<<<"DTMeasurement::configure_channel<-"<<std::endl;
    const char* name =	this->chan_name_.c_str();
    double minVal = 0.0;
    double maxVal = 0.0;

    // set min & max expected values to measure
    if (this->units_ == ni::seconds)
    {
      // seconds:
      minVal = 12.6e-9;
      maxVal = 53.68;
    }
    else
    {
      // ticks:
      minVal = 26e-9;
      maxVal = 53.67;
    }

    int err = DAQmxCreateCITwoEdgeSepChan(this->task_handle_, 
                                          name,
                                          "",
                                          minVal,
                                          maxVal, // The maximum value, in units, that you expect to measure
                                          this->units_,
                                          this->first_edge_,
                                          this->second_edge_,
                                          NULL);

    if(err<0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "DTMeasurement::configure_channel",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }

    //change the default input terminal (the source pin)
    if(this->input_terminal_.size() != 0)
    {
      const char* term =  this->input_terminal_.c_str();
      err = DAQmxSetCICountEdgesTerm(this->task_handle_, name, term);
      if(err<0)
      {
        throw ni660Xsl::DAQException("configuration failed",
          this->get_string_error(),
          "DTMeasurement::configure_channel",
          err);
      }
      else if (err > 0)
      {
        this->warn_ = this->get_string_error();
        this->warn_occured_ = true;
      }
    }

    //-------------------------data transfer------------------------------------------
    err = DAQmxSetCIDataXferMech(this->task_handle_, name, this->data_transfer_);
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "DTMeasurement::configure_channel",
        err);
    }
    else if(err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }

    //std::cout<<<<"DTMeasurement::configure_channel->"<<std::endl;
  }
}//namespace
