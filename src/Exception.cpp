// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    Exception.cpp
//
// = AUTHORS
//    G.Abeille & N.Leclercq
//
// ============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/Exception.h>


namespace ni660Xsl
{

// ============================================================================
// Error::Error
// ============================================================================
Error::Error (void)
  :  reason ("unknown"),
     desc ("unknown error"),
     origin ("unknown"),
     code (-1),
     severity (ERR)
{
}
// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const char *_reason,
              const char *_desc,
              const char *_origin,
              int _code, 
              int _severity)
  :  reason (_reason),
     desc (_desc),
     origin (_origin),
     code (_code),
     severity (_severity)
{
}
// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const std::string& _reason,
              const std::string& _desc,
              const std::string& _origin,
              int _code, 
              int _severity)
  :  reason (_reason),
     desc (_desc),
     origin (_origin),
     code (_code),
     severity (_severity)
{
}
// ============================================================================
// Error::Error
// ============================================================================
Error::Error (const Error& _src)
  :  reason (_src.reason),
     desc (_src.desc),
     origin (_src.origin),
     code (_src.code),
     severity (_src.severity)
{
}
// ============================================================================
// Error::~Error
// ============================================================================
Error::~Error (void)
{
}
// ============================================================================
// Error::operator=
// ============================================================================
Error& Error::operator= (const Error& _src) 
{
  //- no self assign
  if (this == &_src) 
  {
    return *this;
  }
  this->reason = _src.reason;
  this->desc = _src.desc;
  this->origin = _src.origin;
  this->code = _src.code;
  this->severity = _src.severity;

  return *this;
}
// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (void) 
  : errors(0)
{
  this->push_error(Error());
}
// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (const char *_reason,
                      const char *_desc,
                      const char *_origin,
                      int _code, 
                      int _severity) 
  : errors(0)
{
  this->push_error(Error(_reason, _desc, _origin, _code, _severity));
}
// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (const std::string& _reason,
                      const std::string& _desc,
                      const std::string& _origin,
                      int _code, 
                      int _severity) 
  : errors(0)
{
  this->push_error(_reason, _desc, _origin, _code, _severity);
}
// ============================================================================
// DAQException::DAQException
// ============================================================================
DAQException::DAQException (const DAQException& _src) 
  : errors(0)
{
  for (unsigned int i = 0; i < _src.errors.size();  i++) {
    this->push_error(_src.errors[i]);
  }
}
// ============================================================================
// Exception::Exception
// ============================================================================
DAQException& DAQException::operator= (const DAQException& _src) 
{
  //- no self assign
  if (this == &_src) {
    return *this;
  }

  this->errors.clear();

  for (unsigned int i = 0; i < _src.errors.size();  i++) {
    this->push_error(_src.errors[i]);
  }

  return *this;
}
// ============================================================================
// DAQException::~DAQException
// ============================================================================
DAQException::~DAQException (void)
{

  this->errors.clear();
}
// ============================================================================
// DAQException::push_error
// ============================================================================
void DAQException::push_error (const char *_reason,
                            const char *_desc,
                            const char *_origin, 
                            int _err_code, 
                            int _severity)
{
    this->errors.push_back(Error(_reason, _desc, _origin, _err_code, _severity));
}
// ============================================================================
// DAQException::push_error
// ============================================================================
void DAQException::push_error (const std::string& _reason,
                            const std::string& _desc,
                            const std::string& _origin, 
                            int _err_code, 
                            int _severity)
{
    this->errors.push_back(Error(_reason, _desc, _origin, _err_code, _severity));
}
// ============================================================================
// DAQException::push_error
// ============================================================================
void DAQException::push_error (const Error& _error)
{
  this->errors.push_back(_error);
}


}//namespace
