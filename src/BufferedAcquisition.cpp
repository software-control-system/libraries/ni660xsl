// ============================================================================
//
// = CONTEXT
//    TANGO Project - NI660X Support Library
//
// = FILENAME
//   BufferedAcquisition.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/BufferedAcquisition.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
// BufferedAcquisition::BufferedAcquisition
// ============================================================================
BufferedAcquisition::BufferedAcquisition()
:sample_clk_("/Dev1/PFI39"),
active_edge_(ni::rising_edge),
max_rate_(1000.0),
nb_samples_(1000),
strategy_(ni::abort),
start_point_(ni::current_read_position),
offset_(0),
timeout_(1.0),
mode_(ni::continuous),
use_callback_(false),
total_nb_pts_(1000)
{
}
// ============================================================================
// BufferedAcquisition::~BufferedAcquisition
// ============================================================================
BufferedAcquisition::~BufferedAcquisition()
{
}
// ============================================================================
// BufferedAcquisition::set_sample_clock
// ============================================================================
void BufferedAcquisition::set_sample_clock(std::string _sample_clk, ni::EdgeType _active_edge, double _max_rate) 
{
    //std::cout<<"BufferedAcquisition::set_sample_clock<-"<<std::endl;

    this->sample_clk_ = _sample_clk;
    this->active_edge_ = _active_edge;
    this->max_rate_ = _max_rate;
    
	//std::cout<<"BufferedAcquisition::set_sample_clock->"<<std::endl;
}
// ============================================================================
// BufferedAcquisition::set_buffer_depth
// ============================================================================
void BufferedAcquisition::set_buffer_depth(unsigned long _nb_samples)
{
    //std::cout<<<<"BufferedAcquisition::set_buffer_depth<-"<<std::endl;
    
    this->nb_samples_ = _nb_samples;
    
    //std::cout<<<<"BufferedAcquisition::set_buffer_depth->"<<std::endl;
}
// ============================================================================
// BufferedAcquisition::set_overrun_strategy
// ============================================================================
void BufferedAcquisition::set_overrun_strategy(ni::StrategyType _strategy)
{
    this->strategy_= _strategy;
}
// ============================================================================
// BufferedAcquisition::set_timeout
// ============================================================================
void BufferedAcquisition::set_timeout(double _timeout)
{
	this->timeout_ = _timeout;
}
// ============================================================================
// BufferedAcquisition::get_timeout
// ============================================================================
double BufferedAcquisition::get_timeout()
{
	return this->timeout_;
}
// ============================================================================
// BufferedAcquisition::set_timing_mode
// ============================================================================
void BufferedAcquisition::set_timing_mode(ni::ModeType _mode)
{
	this->mode_ = _mode;
}
// ============================================================================
// BufferedAcquisition::set_read_start_point
// ============================================================================
void BufferedAcquisition::set_read_start_point(ni::StartPointType _start_point, unsigned long _offset)
{
    this->start_point_ = _start_point;
    this->offset_ = _offset;
}
// ============================================================================
// BufferedAcquisition::set_callback_mode
// ============================================================================
void BufferedAcquisition::set_callback_mode(bool _enabled)
{
	this->use_callback_ = _enabled;
}
// ============================================================================
// BufferedAcquisition::set_total_nb_pts
// ============================================================================
void BufferedAcquisition::set_total_nb_pts(unsigned long _nb_samples)
{
  //std::cout<<<<"BufferedAcquisition::set_total_nb_pts<-"<<std::endl;

  this->total_nb_pts_ = _nb_samples;    

  //std::cout<<<<"BufferedAcquisition::set_total_nb_pts->"<<std::endl;
}
// ============================================================================
// BufferedAcquisition::get_raw_buffer
// ============================================================================
void BufferedAcquisition::get_raw_buffer(void) 
throw (ni660Xsl::DAQException)
{
  //std::cout << "In get_raw_buffer from BufferedAcquisition" << std::endl;
	ni660Xsl::InRawBuffer* rb = new ni660Xsl::InRawBuffer(this->nb_samples_);
	long _samples_read;
    int err = DAQmxReadCounterU32(this->task_handle_, this->nb_samples_, this->timeout_, 
	rb->base(), this->nb_samples_, &_samples_read, NULL);
	
	//mantis : 6348
	//----------handle acquisition end for finite mode--------------------
	if(err == -200278 && this->mode_ == ni::finite)
	{
	  this->stop();
	}
	
	//----------handle overrun depending of the chosen strategy-----------
	if(err == -200141 || err == -200222 || err == -200279)
	{
		/*std::cout << "lost data " << std::endl;
    char err_msg[2000];
    DAQmxGetErrorString(err,err_msg,2000);
    std::cout << "NI Error: " << std::string(err_msg) << std::endl;*/
		if (this->strategy_ == ni::notify)
		{
			this->handle_data_lost();
			delete rb;
		}
		else if(this->strategy_ == ni::abort)
		{
			this->handle_data_lost();
			//TODO: what occurs after when get next data
			this->abort();
			delete rb;
			throw ni660Xsl::DAQException("reading data failed",
	    "The acquisition has been aborted because of an overrun.",
	    "BufferedAcquisition::get_raw_buffer");
		}
		else if(this->strategy_ == ni::restart)
		{
			this->handle_data_lost();
			this->stop();
			delete rb;
			this->start();
		}
		else if(this->strategy_ == ni::trash)
		{
			delete rb;
		}
		else if(this->strategy_ == ni::ignore)
		{
			//send data
			this->handle_raw_buffer(rb, _samples_read);
		}
	}
	//----------handle timeout---------------------------------------------
	else if (err == -200284)
	{
		this->handle_timeout();
		delete rb;
	}
	//----------handle other errors---------------------------------------
  else if(err < 0)
  {
      delete rb;
      this->state_ = UNKNOWN;
      throw ni660Xsl::DAQException("reading data failed",
	  this->get_string_error(),
	  "BufferedAcquisition::get_raw_buffer",
	  err);
  }
	else
	{
		this->handle_raw_buffer(rb, _samples_read);
	}
	//----------handle warning---------------------------------------------
    if(err > 0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
	
  //std::cout << "Out get_raw_buffer from BufferedAcquisition" << std::endl;
  //std::cout<<"BufferedEventCounting::get_buffer->"<<std::endl;
}
// ============================================================================
// BufferedAcquisition::get_scaled_buffer
// ============================================================================
void BufferedAcquisition::get_scaled_buffer(void) 
throw (ni660Xsl::DAQException)
{
	// check samples number > 0
	if (this->nb_samples_ == 0)
	{
	  this->state_ = UNKNOWN;
	  throw ni660Xsl::DAQException("reading data failed",
			"internal error - samples number failed to null value",
			"BufferedAcquisition::get_scaled_buffer",
			-1);
	}

	ni660Xsl::InScaledBuffer* sb = new ni660Xsl::InScaledBuffer(this->nb_samples_);
	long _samples_read;
    int err = DAQmxReadCounterF64(this->task_handle_, this->nb_samples_, this->timeout_, 
	sb->base(), this->nb_samples_, &_samples_read, NULL);
	
	//mantis : 6348
	//----------handle acquisition end for finite mode--------------------
	if(err == -200278 && this->mode_ == ni::finite)
	{
	  this->stop();
	}
	
	//----------handle overrun depending of the chosen strategy-----------
	if(err == -200141 || err == -200222 || err == -200279)
	{
		
		if (this->strategy_ == ni::notify)
		{
			delete sb;
			this->handle_data_lost();
		}
		else if(this->strategy_ == ni::abort)
		{
			delete sb;
			this->handle_data_lost();
			//TODO: what occurs after when get next data
			this->abort();
			throw ni660Xsl::DAQException("reading data failed",
				"The acquisition has been aborted because of an overrun.",
				"BufferedAcquisition::get_scaled_buffer");
		}
		else if(this->strategy_ == ni::restart)
		{
			delete sb;
			this->handle_data_lost();
			this->stop();
			this->start();
		}
		else if(this->strategy_ == ni::trash)
		{
			delete sb;
		}
		else if(this->strategy_ == ni::ignore)
		{
			//send data
			this->handle_scaled_buffer(sb, _samples_read);
		}
	}
	//----------handle timeout---------------------------------------------
	else if (err == -200284)
	{
		delete sb;
		this->handle_timeout();
	}
	//----------handle others errors---------------------------------------
	else if(err < 0)
	{
		delete sb;
		this->state_ = UNKNOWN;
		throw ni660Xsl::DAQException("reading data failed",
			this->get_string_error(),
			"BufferedAcquisition::get_scaled_buffer",
			err);
	}
	else
	{
		this->handle_scaled_buffer(sb, _samples_read);
	}
	//----------handle warning---------------------------------------------
	if(err > 0)
	{
		this->warn_ = this->get_string_error();
		this->warn_occured_ = true;
	}
	
  
  //std::cout<<"BufferedAcquisition::get_scaled_buffer->"<<std::endl;
}
// ============================================================================
// BufferedAcquisition::get_last_raw_buffer
// ============================================================================
void BufferedAcquisition::get_last_raw_buffer(unsigned long _exp_nb)
  throw (ni660Xsl::DAQException)
{
  //std::cout << "In get_last_raw_buffer from BufferedAcquisition - expected samples = " << _exp_nb << std::endl;

  ni660Xsl::InRawBuffer* rb = new ni660Xsl::InRawBuffer(_exp_nb);
  rb->fill(0); // fill buffer with 0 
  long _samples_read = 0;
  int err = 0;
  bool retry = false;
  long delta = 0;
  
  // 1st read
  err = 0;
  _samples_read = 0;
  err = DAQmxReadCounterU32(this->task_handle_, -1, this->timeout_, 
                rb->base(), _exp_nb, &_samples_read, NULL);

  //----------handle overrun depending of the chosen strategy-----------
  if(err == -200141 || err == -200222 || err == -200279)
  {
	  if (this->strategy_ == ni::notify)
	  {
		  this->handle_data_lost();
		  delete rb;
	  }
	  else if(this->strategy_ == ni::abort)
	  {
		  this->handle_data_lost();
		  this->abort();
		  delete rb;
		  throw ni660Xsl::DAQException("reading data failed",
        "The acquisition has been aborted because of an overrun.",
        "BufferedAcquisition::get_last_raw_buffer");
	  }
	  else if(this->strategy_ == ni::restart)
	  {
		  this->handle_data_lost();
		  this->stop();
		  delete rb;
		  this->start();
	  }
	  else if(this->strategy_ == ni::trash)
	  {
		  delete rb;
	  }
	  else if(this->strategy_ == ni::ignore)
	  {
		  //send data
		  this->handle_raw_buffer(rb, _samples_read);
	  }
  }
  //----------handle timeout---------------------------------------------
  else if (err == -200284)
  {
	  this->handle_timeout();
	  delete rb;
  }
  //----------handle other errors---------------------------------------
  else if(err < 0)
  {
    delete rb;
    this->state_ = UNKNOWN;
    throw ni660Xsl::DAQException("reading data failed",
      this->get_string_error(),
      "BufferedAcquisition::get_last_raw_buffer",
      err);
  }
  else
  {
    delta = _exp_nb - _samples_read;
    if (delta > 0) // read less than expected -> retry
    {
      retry = true;
    }
    else
    {
      this->handle_raw_buffer(rb, _samples_read);
      retry = false;
    }
  }
  //----------handle warning---------------------------------------------
  if(err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }

  // if no retry, exit
  if (!retry)
    return;

  // retry reading:
  //std::cout << "**** BufferedAcquisition::get_last_raw_buffer() Expected samples differs from read samples by: " << delta << " - retry reading! ****" << std::endl;
  ACE_OS::sleep(ACE_Time_Value(0,100000)); // 100ms

  ni660Xsl::InRawBuffer* _next_rb = new ni660Xsl::InRawBuffer((size_t)delta);
  err = 0;
  _samples_read = 0;
  err = DAQmxReadCounterU32(this->task_handle_, -1, this->timeout_, 
                _next_rb->base(), (unsigned long)delta, &_samples_read, NULL);

  if ((err == 0) && (_samples_read == delta))
  {
    // if reading ok, put read values at the end of buffer
    for (size_t idx = 0; idx < (unsigned long)delta; idx++)
    {
      rb[_exp_nb - delta + idx] = _next_rb[idx];
    }
  }

  _samples_read = (long)_exp_nb;
  this->handle_raw_buffer(rb, _samples_read);

  //std::cout<<"BufferedAcquisition::get_last_raw_buffer->"<<std::endl;
}
// ============================================================================
// BufferedAcquisition::get_last_scaled_buffer
// ============================================================================
void BufferedAcquisition::get_last_scaled_buffer(unsigned long _exp_nb)
  throw (ni660Xsl::DAQException)
{
  //std::cout << "In get_last_scaled_buffer from BufferedAcquisition- expected samples = " << _exp_nb << std::endl;

	ni660Xsl::InScaledBuffer* sb = new ni660Xsl::InScaledBuffer(_exp_nb);
  sb->fill(0.0); // fill buffer with 0
	long _samples_read = 0;
  int err = 0;
  bool retry = false;
  long delta = 0;

  // 1st read:
  err = 0;
  _samples_read = 0;
  err = DAQmxReadCounterF64(this->task_handle_, -1, this->timeout_, 
	                sb->base(), _exp_nb, &_samples_read, NULL);
  	
  //----------handle overrun depending of the chosen strategy-----------
  if(err == -200141 || err == -200222 || err == -200279)
  {
	  if (this->strategy_ == ni::notify)
	  {
		  delete sb;
		  this->handle_data_lost();
	  }
	  else if(this->strategy_ == ni::abort)
	  {
		  delete sb;
		  this->handle_data_lost();
		  this->abort();
		  throw ni660Xsl::DAQException("reading data failed",
			  "The acquisition has been aborted because of an overrun.",
			  "BufferedAcquisition::get_last_scaled_buffer");
	  }
	  else if(this->strategy_ == ni::restart)
	  {
		  delete sb;
		  this->handle_data_lost();
		  this->stop();
		  this->start();
	  }
	  else if(this->strategy_ == ni::trash)
	  {
		  delete sb;
	  }
	  else if(this->strategy_ == ni::ignore)
	  {
		  //send data
		  this->handle_scaled_buffer(sb, _samples_read);
	  }
  }
  //----------handle timeout---------------------------------------------
  else if (err == -200284)
  {
	  delete sb;
	  this->handle_timeout();
  }
  //----------handle others errors---------------------------------------
  else if(err < 0)
  {
	  delete sb;
	  this->state_ = UNKNOWN;
	  throw ni660Xsl::DAQException("reading data failed",
		  this->get_string_error(),
		  "BufferedAcquisition::get_last_scaled_buffer",
		  err);
  }
  else
  {
    delta = _exp_nb - _samples_read;
    if (delta > 0) // read less than expected -> retry
    {
      retry = true;
    }
    else
    {
      this->handle_scaled_buffer(sb, _samples_read);
      retry = false;
    }
  }
  //----------handle warning---------------------------------------------
  if(err > 0)
  {
	  this->warn_ = this->get_string_error();
	  this->warn_occured_ = true;
  }

  // if no retry, exit
  if (!retry)
    return;

  // retry reading:
  //std::cout << "**** BufferedAcquisition::get_last_scaled_buffer() Expected samples differs from read samples by: " << delta << " - retry reading! ****" << std::endl;
  ACE_OS::sleep(ACE_Time_Value(0,100000)); // 100ms

  ni660Xsl::InScaledBuffer* _next_sb = new ni660Xsl::InScaledBuffer((size_t)delta);
  err = 0;
  _samples_read = 0;
  err = DAQmxReadCounterF64(this->task_handle_, -1, this->timeout_, 
	                _next_sb->base(), (unsigned long)delta, &_samples_read, NULL);

  if ((err == 0) && (_samples_read == delta))
  {
    // if reading ok, put read values at the end of buffer
    for (size_t idx = 0; idx < (unsigned long)delta; idx++)
    {
      sb[_exp_nb - delta + idx] = _next_sb[idx];
    }
  }

  _samples_read = (long)_exp_nb;
  this->handle_scaled_buffer(sb, _samples_read);

  //std::cout<<"BufferedAcquisition::get_last_scaled_buffer->"<<std::endl;
}

}//namespace
