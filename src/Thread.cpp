// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660Xsl Support Library
//
// = FILENAME
//    Thread.cpp
//
// = AUTHOR
//    Nicolas Leclercq
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/Thread.h>
#include <NI660Xsl/Exception.h>


namespace ni660Xsl{

// ============================================================================
// Thread::Thread
// ============================================================================
Thread::Thread (bool join)
  : arg_(0),
    self_idt_ (0),
    self_hdl_(0), 
    grp_id_(0),  
    join_ (join), 
    go_on_ (false)
{
}

// ============================================================================
// Thread::~Thread
// ============================================================================
Thread::~Thread (void)
{
  if (join_) {
    this->quit_and_join();
  }
  else {
    this->quit();
  }
}

// ============================================================================
// Thread::spawner
// ============================================================================
ACE_THR_FUNC_RETURN Thread::spawner (void* _arg) 
{
  if (!_arg) return 0; 

  Thread* t = ACE_reinterpret_cast(Thread*, _arg);

  return t->svc(t->arg_);
}

// ============================================================================
// Thread::run
// ============================================================================
int Thread::run (void* arg, long prio) 
{
	//std::cout<<<<"Thread::run<-"<<std::endl;
  //-TODO: prevent run to be executed more than once.
  if(this->go_on_ == true)
  {
	  throw ni660Xsl::DAQException("run thread failed",
	    "the thread is already running",
	    "Thread::run");
  }
	
  this->go_on_ = true;

  //- store thread arg
  this->arg_ = arg;

  //- get a ref to the default thread manager
  ACE_Thread_Manager* tm = ACE_Thread_Manager::instance();
  if (tm == 0)
  {
    return -1;
  }

  //- set spawn flags 
  long flags = this->join_ ? THR_NEW_LWP : (THR_NEW_LWP|THR_JOINABLE);

  //- spawn the thread
  this->grp_id_ = tm->spawn (Thread::spawner, 
			     this,
			     flags,
			     &this->self_idt_,
			     &this->self_hdl_, 
			     prio);
  //- check error
  if (this->grp_id_ == -1) 
  {
    return -1;
  }
	//std::cout<<"Thread::run->"<<std::endl;
  return 0;
}

// ============================================================================
// Thread::quit
// ============================================================================
int Thread::quit (void)
{
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->lock_,  -1);
  return this->quit_i();
}

// ============================================================================
// Thread::quit_i
// ============================================================================
int Thread::quit_i (void)
{
	//std::cout<<" Thread::quit_i<-"<<std::endl;
  this->go_on_ = false;
  //std::cout<<" Thread::quit_i->"<<std::endl;
  return 0;
  
}

// ============================================================================
// Thread::join
// ============================================================================
int Thread::join (ACE_THR_FUNC_RETURN *status) 
{
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->lock_,  -1);
  return this->join_i(status);
}

// ============================================================================
// Thread::join_i
// ============================================================================
int Thread::join_i (ACE_THR_FUNC_RETURN *status) 
{
  //- in case of error
  if (status) 
    *status = 0;
  //- no need to stop and/or join if not started
  if (this->self_idt_ == 0) {
    return 0;
  }
  //- get a ref to the default thread manager
  ACE_Thread_Manager* tm = ACE_Thread_Manager::instance();
  if (tm == 0) {
    return -1;
  }
  //- join 
  int result = tm->join(this->self_idt_, status);
  //- reset thread id
  this->self_idt_ = 0;
  //- return
  return result;
}

// ============================================================================
// Thread::quit_and_join
// ============================================================================
int Thread::quit_and_join (ACE_THR_FUNC_RETURN *status) 
{
  //- guard
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->lock_,  -1);
  //- ask the thread to quit
  this->quit_i();
  //- join 
  return this->join_i(status);
}
} // namespace ni660Xsl


