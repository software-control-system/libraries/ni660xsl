// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    PulsedTask.cpp
//
// = AUTHOR
//    S. Minolli (fully inspired from N. Leclercq ASL PulseTask class)
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/PulsedTaskManager.h>


namespace ni660Xsl {

// ============================================================================
// PulsedTask::PulsedTask
// ============================================================================
PulsedTask::PulsedTask ()
  : max_count_ (0),
    cur_count_ (0),
    timer_ (-1)
{
}

// ============================================================================
// PulsedTask::~PulsedTask
// ============================================================================
PulsedTask::~PulsedTask ()
{
  //- stop
  this->stop();
}

// ============================================================================
// PulsedTask::start
// ============================================================================
int PulsedTask::start (void *_arg,
                       unsigned long _pulse_interval,
                       unsigned long _pulse_count)
{
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->lock_, -1);

  //- already started?
  if (this->timer_ != -1) {
    return 0;
  }

  //- convert from ms to ACE_Time_Value
  ACE_Time_Value delay (0, (long)(1000 * _pulse_interval));

  //- store pulse count
  this->max_count_ = this->cur_count_ = _pulse_count;

  //- register self with the manager
  this->timer_ = 
    PulsedTaskManager::instance()->register_task(this, _arg, delay);

  return 0;
}

// ============================================================================
// PulsedTask::stop
// ============================================================================
int PulsedTask::stop ()
{ 
  ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, this->lock_, -1);

  //- really started?
  if (this->timer_ == -1) {
    return 0;
  }

  //- remove task from reactor
  PulsedTaskManager::instance()->remove_task(this);
 
  //- reset timer id
  this->timer_ = -1;

  return 0;
}

// ============================================================================
// PulsedTask::handle_timeout
// ============================================================================
int PulsedTask::handle_timeout (const ACE_Time_Value &current_time, 
                                const void *arg)
{
  ACE_UNUSED_ARG(current_time);

  //- stop if user returns -1
  if (this->pulsed (ACE_const_cast(void*, arg)) == -1) {
    return this->stop(); 
  }

  //- cancel timer if max count reached 
  if (this->max_count_ != 0 && --this->cur_count_ == 0) {
    return this->stop(); 
  }
  
  return 0;
}


} // namespace ni660Xsl



