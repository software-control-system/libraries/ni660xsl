// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   PositionMeasurement.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/PositionMeasurement.h>

namespace ni660Xsl
{
  // ============================================================================
  // PositionMeasurement::PositionMeasurement
  // ============================================================================
  PositionMeasurement::PositionMeasurement():
InputOperation(),
added_channels(0)
{
}
// ============================================================================
// PositionMeasurement::~PositionMeasurement
// ============================================================================
PositionMeasurement::~PositionMeasurement()
{
}
// ============================================================================
// PositionMeasurement::add_linear_encoder
// ============================================================================
void PositionMeasurement::add_linear_encoder(ni660Xsl::LinearEncoderChan _chan)
throw (ni660Xsl::DAQException)
{ 

  //std::cout<<<<"PositionMeasurement::add_linear_encoder<-"<<std::endl;
  //for a input task, it is only possible to add one channel
  /* if(added_channels >= 1)
  {
  throw ni660Xsl::DAQException("add channel failed",
  "only one channel can be used",
  "PositionMeasurement::add_linear_encoder");
  }
  added_channels++;*/
  this->lin_chans_.push_back(_chan);
  //std::cout<<<<"PositionMeasurement::add_linear_encoder->"<<std::endl;
}
// ============================================================================
// PositionMeasurement::add_angular_encoder
// ============================================================================
void PositionMeasurement::add_angular_encoder(ni660Xsl::AngularEncoderChan _chan)
throw (ni660Xsl::DAQException)
{
  //std::cout<<<<"PositionMeasurement::add_angular_encoder<-"<<std::endl;
  //for a input task, it is only possible to add one channel
  if(added_channels >= 1)
  {
    throw ni660Xsl::DAQException("add channel failed",
      "only one channel can be used",
      "PositionMeasurement::add_linear_encoder");
  }
  added_channels++;
  this->ang_chans_.push_back(_chan);
  //std::cout<<<<"PositionMeasurement::add_angular_encoder->"<<std::endl;
}
// ============================================================================
// PositionMeasurement::configure_channel
// ============================================================================
void PositionMeasurement::configure_channel(void)
throw (ni660Xsl::DAQException)
{
  const char* name = 0;

  //------------------lin channels config-----------------------
  if(this->lin_chans_.size() != 0)
  {
    //std::cout<<<<"\t config lin"<<std::endl;
    name = this->lin_chans_[0].chan_name.c_str();
    const char* scale = "";

    int err = DAQmxCreateCILinEncoderChan(this->task_handle_, 
                                          name,
                                          "",
                                          this->lin_chans_[0].decoding_type,
                                          this->lin_chans_[0].z_idx_enable,
                                          this->lin_chans_[0].z_idx_val,
                                          this->lin_chans_[0].z_idx_phase,
                                          this->lin_chans_[0].units,
                                          this->lin_chans_[0].distance_per_pulse,
                                          this->lin_chans_[0].initial_position,
                                          scale);
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "PositionMeasurement::configure_channel",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
  }
  //------------------ang channels config-----------------------
  if(this->ang_chans_.size() != 0)
  {
    //std::cout<<<<"\t config ang"<<std::endl;
    name = this->ang_chans_[0].chan_name.c_str();
    const char* scale = "";

    int err = DAQmxCreateCIAngEncoderChan(this->task_handle_, 
      name,
      "",
      this->ang_chans_[0].decoding_type,
      this->ang_chans_[0].z_idx_enable,
      this->ang_chans_[0].z_idx_val,
      this->ang_chans_[0].z_idx_phase,
      this->ang_chans_[0].units,
      this->ang_chans_[0].pulse_per_revolution,
      this->ang_chans_[0].initial_angle,
      scale);
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "PositionMeasurement::configure_channel",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
  }


  //data transfer
  int err = DAQmxSetCIDataXferMech(this->task_handle_, name, this->data_transfer_);
  if(err < 0)
  {
    throw ni660Xsl::DAQException("configuration failed",
      this->get_string_error(),
      "BufferedEventCounting::configure",
      err);
  }
  else if(err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }

  // digital filters
  if (enable_min_pulse_width_)
  {
    err = DAQmxSetCIEncoderAInputDigFltrEnable(this->task_handle_, name, true);
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "BufferedEventCounting::configure",
        err);
    }
    else if(err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }

    err = DAQmxSetCIEncoderAInputDigFltrMinPulseWidth(this->task_handle_, name, this->min_pulse_width_seconds_);
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "BufferedEventCounting::configure",
        err);
    }
    else if(err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }


    err = DAQmxSetCIEncoderBInputDigFltrEnable(this->task_handle_, name, true);
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "BufferedEventCounting::configure",
        err);
    }
    else if(err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }

    err = DAQmxSetCIEncoderBInputDigFltrMinPulseWidth(this->task_handle_, name, this->min_pulse_width_seconds_);
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "BufferedEventCounting::configure",
        err);
    }
    else if(err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
  }


}
}//namespace
