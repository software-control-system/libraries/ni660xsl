// ============================================================================
//
// = CONTEXT
//    TANGO Project - NI660X Support Library
//
// = FILENAME
//   BufferedEventCounting.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/BufferedEventCounting.h>
#include <NI660Xsl/Exception.h>
#include <iostream>

namespace ni660Xsl
{

// ============================================================================
// raw_buffer_callback_evt
//
// Callback function for EVT buffered acquisition.
// @param _taskHandle NI task handler.
// @param _evt_type Event type associated to the callback function.
// @param _samples Number of samples of the buffer.
// @param _data Callback data pointer.
// ============================================================================
int32 CVICALLBACK raw_buffer_callback_evt(ni::DAQTaskHandle _taskHandle, 
                                      int32 _evt_type, 
                                      uInt32 _samples, 
                                      void *_data)
{
  //std::cout << "raw_buffer_callback_evt() entering..." << std::endl;

  BufferedEventCounting* buff_class_ = reinterpret_cast<BufferedEventCounting*>(_data);

  if (buff_class_)
  {
    ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, buff_class_->stop_lock_, 0);
	
	  // If stop in progress, do not read data
	  if (!buff_class_->stop_flag_)
	  {

      // restart timeout
      if (buff_class_->tmo_caller_)
      {
        buff_class_->tmo_caller_->stop();
        buff_class_->tmo_caller_->start(buff_class_, 
          (unsigned long)(buff_class_->get_timeout() * 1000));
      }

      // call internal function to read data from board
      buff_class_->get_raw_buffer();
    }
  }

  return 0;
}

// ============================================================================
// BufferedEventCounting::BufferedEventCounting
// ============================================================================
BufferedEventCounting::BufferedEventCounting()
:EventCounting(),
BufferedAcquisition()
{
  tmo_caller_ = NULL; 
}

// ============================================================================
// BufferedEventCounting::~BufferedEventCounting
// ============================================================================
BufferedEventCounting::~BufferedEventCounting()
{
  //std::cout<<"BufferedEventCounting::~BufferedEventCounting"<<std::endl;

  if (tmo_caller_)
  {
    tmo_caller_->stop();
    delete tmo_caller_;
    tmo_caller_ = NULL;
  }
}

// ============================================================================
// BufferedEventCounting::configure
// ============================================================================
void BufferedEventCounting::configure(void) 
  throw (ni660Xsl::DAQException)
{
  //std::cout<<<<"BufferedEventCounting::configure<-"<<std::endl;

  //-------------------------configure channel------------------------------------------
  try
  {
    this->configure_channel();
  }
  catch (const DAQException&)
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }

  //-------------------------configure timing------------------------------------------
  const char* clk =  this->sample_clk_.c_str();

  int err = DAQmxCfgSampClkTiming(this->task_handle_, clk, this->max_rate_, 
    this->active_edge_, this->mode_, this->nb_samples_);
  
  if (err < 0)
  {
    throw ni660Xsl::DAQException("Timing configuration failed!",
	    this->get_string_error(),
	    "BufferedEventCounting::configure()",
	    err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }

  if (this->use_callback_)
  {
    //-------------------------Register callback event-------------------------------
    //- Register a callback which will be called when the number of samples will be acquired.
    err = DAQmxRegisterEveryNSamplesEvent(this->task_handle_, ni::intoBuffer, 
                this->nb_samples_, 0, raw_buffer_callback_evt, 
                static_cast<void*> (this));
    
    if (err < 0)
    {
	    throw ni660Xsl::DAQException("callback configuration failed!",
	      this->get_string_error(),
	      "BufferedEventCounting::configure()",
	      err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
  }

  if (this->use_callback_)
  {
    //-------------------------reset acquisition buffer-------------------------------------------
    err = DAQmxResetBufInputBufSize(this->task_handle_);

    if (err < 0)
    {
	    throw ni660Xsl::DAQException("Buffer reset failed!",
	      this->get_string_error(),
	      "BufferedEventCounting::configure()",
	      err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
  }

  //-------------------------configure acquisition buffer-------------------------------------------
  if (this->use_callback_)
  {
    // In DMA mode, if samples number = total number of points, configure
    // input buffer size with 2 * total_nb_pts_, because input buffer size
    // should be strictly above total_nb_pts_ in callback mode
    if ((this->data_transfer_ == ni::dma) &&
        (this->nb_samples_ == this->total_nb_pts_))
    {
      err = DAQmxCfgInputBuffer(this->task_handle_, 2 * this->total_nb_pts_);
    }
    // If total number of points is null (means infinite acquisition):
    // set input buffer size with 2 * nb_samples_
    else if (this->total_nb_pts_ == 0)
    {
      err = DAQmxCfgInputBuffer(this->task_handle_, 2 * this->nb_samples_);
    }
    else
    {
      // Configure the input buffer size with the total number of point to be received
      err = DAQmxCfgInputBuffer(this->task_handle_, this->total_nb_pts_);
    }
  }
  else
  {
    err = DAQmxCfgInputBuffer(this->task_handle_, this->nb_samples_);
  }

  if (err < 0)
  {
	  throw ni660Xsl::DAQException("Buffer configuration failed!",
	    this->get_string_error(),
	    "BufferedEventCounting::configure()",
	    err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
  
  //---------------------configure read start point-----------------------------------
  err = DAQmxSetReadRelativeTo(this->task_handle_, this->start_point_);
  
  if (err < 0)
  {
    throw ni660Xsl::DAQException("Start point configuration failed!",
	    this->get_string_error(),
	    "BufferedEventCounting::configure()",
	    err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
    
  //-------------------------configure offset-------------------------------
  err = DAQmxSetReadOffset(this->task_handle_, this->offset_);

  if (err < 0)
  {
    throw ni660Xsl::DAQException("Offset configuration failed!",
	    this->get_string_error(),
	    "BufferedEventCounting::configure()",
	    err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
	
  //-------------------------configure tc------------------------------------------
	if( this->terminal_.size() != 0)
	{
		const char* term =  this->terminal_.c_str();
		err = DAQmxSetExportedCtrOutEventOutputTerm(this->task_handle_, term);
		
    if (err < 0)
		{
			throw ni660Xsl::DAQException("Output term configuration failed!",
				this->get_string_error(),
				"BufferedEventCounting::configure()",
        err);
		}
		else if (err > 0)
		{
			this->warn_ = this->get_string_error();
			this->warn_occured_ = true;
		}

		err = DAQmxSetExportedCtrOutEventOutputBehavior(this->task_handle_, this->behavior_);
		
    if (err < 0)
		{
			throw ni660Xsl::DAQException("Output behaviour configuration failed!",
				this->get_string_error(),
				"BufferedEventCounting::configure()",
        err);
		}
		else if (err > 0)
		{
			this->warn_ = this->get_string_error();
			this->warn_occured_ = true;
		}
	}

	//-------------------------configure start trigger------------------------------------------
  if (use_start_trig_)
  {
	  // configure trigger to be a "arm start trig"
	  int err = DAQmxSetArmStartTrigType(this->task_handle_, DAQmx_Val_DigEdge);

	  if(err < 0)
	  {
	    throw ni660Xsl::DAQException("Start trigger configuration failed!",
        this->get_string_error(),
        "BufferedEventCounting::configure()",
        err);
    }
    else if(err > 0)
    {
	    this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
    }
	
	  // configure trigger pin
	  const char* s =  this->start_trigger_source_.c_str();

	  err = DAQmxSetDigEdgeArmStartTrigSrc(this->task_handle_, s);

	  if(err < 0)
	  {
	    throw ni660Xsl::DAQException("Start trigger src configuration failed!",
        this->get_string_error(),
        "BufferedEventCounting::configure()",
        err);
    }
    else if (err > 0)
    {
	    this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
    }
    
    // configure edge 
    err = DAQmxSetDigEdgeArmStartTrigEdge(this->task_handle_, this->trig_active_edge_);
    
    if (err < 0)
    {
	    throw ni660Xsl::DAQException("Start trigger edge configuration failed!",
        this->get_string_error(),
        "BufferedEventCounting::configure()",
        err);
    }
    else if (err > 0)
    {
	    this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
    }
  }
  
  //---------------------configure autostart------------------------------------------------
  //to call get_buffer() does not start the acquisition.
  err = DAQmxSetReadAutoStart(this->task_handle_, FALSE);

  if (err < 0)
  {
    throw ni660Xsl::DAQException("Autostart configuration failed!",
	    this->get_string_error(),
	    "BufferedEventCounting::configure()",
	    err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
	
  //---------------------configure read mode-----------------------------------------------
	err = DAQmxSetReadWaitMode(this->task_handle_, this->read_mode_);
	
  if (err < 0)
  {
    throw ni660Xsl::DAQException("read mode configuration failed!",
	    this->get_string_error(),
	    "BufferedEventCounting::configure()");
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }

  //---------------------get task ready to start------------------------------------------------
  try
  {
    this->prepare_task();
  }
  catch (const DAQException&)
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }
  
  //std::cout<<<<"BufferedEventCounting::configure->"<<std::endl;
}

// ============================================================================
// BufferedEventCounting::start
// ============================================================================
void BufferedEventCounting::start(void)
  throw (ni660Xsl::DAQException)
{
  stop_flag_ = false;

  // first call mother class function to start acquisition
  CounterBasedOperation::start();

  // then, in case of callback mode enabled, start timeout pulsed object
  if (use_callback_)
  {
    tmo_caller_ = new TimeoutCallerEvt();
    if (tmo_caller_ == 0) 
    {
      throw ni660Xsl::DAQException("start failed",
	      "Failed to instanciate timeout caller in callback mode!",
	      "BufferedEventCounting::start");
    }
    tmo_caller_->start(this, (unsigned long)(timeout_ * 1000));
  }
}

// ============================================================================
// BufferedEventCounting::stop
// ============================================================================
void BufferedEventCounting::stop(void)
  throw (ni660Xsl::DAQException)
{
  // set flag to avoid race condition with callback
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, ace_mon, this->stop_lock_, 
    throw ni660Xsl::DAQException("Stop failed!",
				"Device is handling data from driver, cannot abort!",
				"BufferedEventCounting::stop",
        666));

  stop_flag_ = true;

  // first call mother class function to stop acquisition
  CounterBasedOperation::stop();

  // then, in case of callback mode enabled, stop timeout pulsed object
  if (use_callback_)
  {
    if (tmo_caller_) 
    {
      tmo_caller_->stop();
      delete tmo_caller_;
      tmo_caller_ = NULL;
    }
  }
}

// ============================================================================
// BufferedEventCounting::abort
// ============================================================================
void BufferedEventCounting::abort(void)
  throw (ni660Xsl::DAQException)
{
  // set flag to avoid race condition with callback
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, ace_mon, this->stop_lock_, 
    throw ni660Xsl::DAQException("Abort failed!",
				"Device is handling data from driver, cannot abort!",
				"BufferedEventCounting::abort",
        666));

  stop_flag_ = true;

  // first call mother class function to abort acquisition
  CounterBasedOperation::abort();

  // then, in case of callback mode enabled, stop timeout pulsed object
  if (use_callback_)
  {
    if (tmo_caller_) 
    {
      tmo_caller_->stop();
      delete tmo_caller_;
      tmo_caller_ = NULL;
    }
  }
}

// ============================================================================
// BufferedEventCounting::abort_and_release
// ============================================================================
void BufferedEventCounting::abort_and_release(void)
  throw (ni660Xsl::DAQException)
{
  // set flag to avoid race condition with callback
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, ace_mon, this->stop_lock_, 
    throw ni660Xsl::DAQException("Abort and release failed!",
				"Device is handling data from driver, cannot abort!",
				"BufferedEventCounting::abort_and_release",
        666));

  stop_flag_ = true;

  // first call mother class function to abort acquisition
  CounterBasedOperation::abort();

  // then, in case of callback mode enabled, stop timeout pulsed object
  if (use_callback_)
  {	
    if (tmo_caller_) 
    {
      tmo_caller_->stop();
      delete tmo_caller_;
      tmo_caller_ = NULL;
    }
  }

  // then release counter
  CounterBasedOperation::release();
}

// ============================================================================
// BufferedEventCounting::handle_tmo_cb
// ============================================================================
void BufferedEventCounting::handle_tmo_cb(void)
{
  // The DAQmx driver, in version 14.0 and above, manage the overrun error 
  // in the callback function => see get_raw_buffer()
  // We only need here to manage the timeout error
  this->handle_timeout();
}

}//namespace
