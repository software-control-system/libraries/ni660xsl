// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   PeriodMeasurement.cpp
//
// = AUTHORS
//    S. Minolli 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/PeriodMeasurement.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
  // ============================================================================
  // PeriodMeasurement::PeriodMeasurement
  // ============================================================================
  PeriodMeasurement::PeriodMeasurement()
    :InputOperation(),
    added_channels(0),
    chan_name_("/Dev1/ctr0"),
    edge_(ni::rising_edge),
    units_(ni::seconds),
    input_terminal_("")
  {
    //std::cout<<<<"PeriodMeasurement::PeriodMeasurement"<<std::endl;
  }
  // ============================================================================
  // PeriodMeasurement::~PeriodMeasurement
  // ============================================================================
  PeriodMeasurement::~PeriodMeasurement()
  {
    //std::cout<<<<"PeriodMeasurement::~PeriodMeasurement"<<std::endl;
  }
  // ============================================================================
  // PeriodMeasurement::add_input_channel
  // ============================================================================
  void PeriodMeasurement::add_input_channel(ni660Xsl::PeriodChan _chan)
    throw (ni660Xsl::DAQException)
  {
    //std::cout<<<<"PeriodMeasurement::add_input_channel<-"<<std::endl;
    //for a input task, it is only possible to add one channel

    /*  if(added_channels >= 1)

    {

    throw ni660Xsl::DAQException("add channel failed",
    "only one channel can be used",
    "PeriodMeasurement::add_input_channel");
    }*/
    added_channels++;

    this->chan_name_ = _chan.chan_name;
    this->edge_ = _chan.edge;
    this->units_ = _chan.units;
    this->input_terminal_ = _chan.input_terminal;
    //std::cout<<<<"PeriodMeasurement::add_input_channel->"<<std::endl;

  }
  // ============================================================================
  // PeriodMeasurement::configure_channel
  // ============================================================================
  void PeriodMeasurement::configure_channel(void) 
    throw (ni660Xsl::DAQException)
  {
    //std::cout<<<<"PeriodMeasurement::configure_channel<-"<<std::endl;
    const char* name =	this->chan_name_.c_str();
    double minVal = 0.0;
    double maxVal = 0.0;

    // set min & max expected values to measure
    if (this->units_ == ni::seconds)
    {
      // seconds:
      minVal = 12.6e-9;
      maxVal = 53.68;
    }
    else
    {
      // ticks:
      minVal = 26e-9;
      maxVal = 53.67;
    }

    int err = DAQmxCreateCIPeriodChan(this->task_handle_, 
                            name,
                            "",
                            minVal,
                            maxVal,
                            this->units_,
                            this->edge_,
									          DAQmx_Val_LowFreq1Ctr, // measure method
									          1, // measure time, not used for DAQmx_Val_LowFreq1Ctr method 
									          4, // divisor, not used for DAQmx_Val_LowFreq1Ctr method 
                            NULL); // custom scale name, not used if unit is specified

    if(err<0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "PeriodMeasurement::configure_channel",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }

    //change the default input terminal (the source pin)
    if(this->input_terminal_.size() != 0)
    {
      const char* term =  this->input_terminal_.c_str();
      err = DAQmxSetCICountEdgesTerm(this->task_handle_, name, term);
      if(err<0)
      {
        throw ni660Xsl::DAQException("configuration failed",
          this->get_string_error(),
          "PeriodMeasurement::configure_channel",
          err);
      }
      else if (err > 0)
      {
        this->warn_ = this->get_string_error();
        this->warn_occured_ = true;
      }
    }

    //-------------------------data transfer------------------------------------------
    err = DAQmxSetCIDataXferMech(this->task_handle_, name, this->data_transfer_);
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "PeriodMeasurement::configure_channel",
        err);
    }
    else if(err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }

    //std::cout<<<<"PeriodMeasurement::configure_channel->"<<std::endl;
  }

}//namespace
