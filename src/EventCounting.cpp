// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   EventCounting.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/EventCounting.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
  // ============================================================================
  // EventCounting::EventCounting
  // ============================================================================
  EventCounting::EventCounting()
    :InputOperation(),
    added_channels(0),
    chan_name_("/Dev1/ctr0"),
    edge_(ni::rising_edge),
    initial_count_(0),
    count_direction_(ni::count_up),
    input_terminal_("")
  {
    //std::cout<<<<"EventCounting::EventCounting"<<std::endl;
  }
  // ============================================================================
  // EventCounting::~EventCounting
  // ============================================================================
  EventCounting::~EventCounting()
  {
    //std::cout<<<<"EventCounting::~EventCounting"<<std::endl;
  }
  // ============================================================================
  // EventCounting::add_input_channel
  // ============================================================================
  void EventCounting::add_input_channel(ni660Xsl::EventCountChan _chan)
    throw (ni660Xsl::DAQException)
  {
    //std::cout<<<<"EventCounting::add_input_channel<-"<<std::endl;
    //for a input task, it is only possible to add one channel

    /*  if(added_channels >= 1)

    {

    throw ni660Xsl::DAQException("add channel failed",
    "only one channel can be used",
    "EventCounting::add_input_channel");
    }*/
    added_channels++;

    this->chan_name_ = _chan.chan_name;
    this->edge_ = _chan.edge;
    this->initial_count_ = _chan.initial_count;
    this->count_direction_ = _chan.count_direction;
    this->input_terminal_ = _chan.input_terminal;
    //std::cout<<<<"EventCounting::add_input_channel->"<<std::endl;

  }
  // ============================================================================
  // EventCounting::configure_channel
  // ============================================================================
  void EventCounting::configure_channel(void) throw (ni660Xsl::DAQException)
  {
    //std::cout<<<<"EventCounting::configure_channel<-"<<std::endl;
    const char* name =	this->chan_name_.c_str();

    int err = DAQmxCreateCICountEdgesChan(this->task_handle_, 
                                          name,
                                          "",
                                          this->edge_,
                                          this->initial_count_,
                                          this->count_direction_);
    if(err<0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "EventCounting::configure_channel",
        err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }

    //change the default input terminal (the source pin)
    if(this->input_terminal_.size() != 0)
    {
      const char* term =  this->input_terminal_.c_str();
      err = DAQmxSetCICountEdgesTerm(this->task_handle_, name, term);
      if(err<0)
      {
        throw ni660Xsl::DAQException("configuration failed",
          this->get_string_error(),
          "EventCounting::configure_channel",
          err);
      }
      else if (err > 0)
      {
        this->warn_ = this->get_string_error();
        this->warn_occured_ = true;
      }
    }
    //-------------------------data transfer------------------------------------------
    err = DAQmxSetCIDataXferMech(this->task_handle_, name, this->data_transfer_);
    if(err < 0)
    {
      throw ni660Xsl::DAQException("configuration failed",
        this->get_string_error(),
        "BufferedEventCounting::configure",
        err);
    }
    else if(err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }

    if (enable_min_pulse_width_)
    {
      err = DAQmxSetCICountEdgesDigFltrEnable(this->task_handle_, name, true);
      if(err < 0)
      {
        throw ni660Xsl::DAQException("configuration failed",
          this->get_string_error(),
          "BufferedEventCounting::configure",
          err);
      }
      else if(err > 0)
      {
        this->warn_ = this->get_string_error();
        this->warn_occured_ = true;
      }

      err =  DAQmxSetCICountEdgesDigFltrMinPulseWidth(this->task_handle_, name, this->min_pulse_width_seconds_);
      if(err < 0)
      {
        throw ni660Xsl::DAQException("configuration failed",
          this->get_string_error(),
          "BufferedEventCounting::configure",
          err);
      }
      else if(err > 0)
      {
        this->warn_ = this->get_string_error();
        this->warn_occured_ = true;
      }
    }

    //std::cout<<<<"EventCounting::configure_channel->"<<std::endl;
  }
}//namespace
