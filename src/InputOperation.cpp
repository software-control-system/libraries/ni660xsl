// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   InputOperation.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/InputOperation.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
// InputOperation::InputOperation
// ============================================================================
InputOperation::InputOperation()
:CounterBasedOperation(),
use_start_trig_(false),
start_trigger_source_("/Dev1/PFI38"),
trig_active_edge_(ni::rising_edge),
read_mode_(ni::yield),
terminal_(""),
behavior_(ni::pulse),
data_transfer_(ni::dma),
enable_min_pulse_width_(false),
min_pulse_width_seconds_(0)
{}
// ============================================================================
// InputOperation::~InputOperation
// ============================================================================
InputOperation::~InputOperation()    
{}
// ============================================================================
// InputOperation::set_start_trigger
// ============================================================================
void InputOperation::set_start_trigger(std::string _trigger_source, ni::EdgeType _active_edge)
    throw (ni660Xsl::DAQException)
{
    /*if(this->trig_mode_ == ni::pause_trigger)
    {
	throw ni660Xsl::DAQException("impossible to set start trigger",
	    "The trigger is already used as a pause trigger",
	    "InputOperation::set_start_trigger");
    }*/
    this->start_trigger_source_ = _trigger_source;
    this->trig_active_edge_ = _active_edge;
    this->use_start_trig_ = true;
}
// ============================================================================
// InputOperation::set_read_mode
// ============================================================================
void InputOperation::set_read_mode(ni::ReadModeType _read_mode)
{
	this->read_mode_ = _read_mode;
}
// ============================================================================
// InputOperation::set_terminal_count
// ============================================================================
void InputOperation::set_terminal_count(std::string _terminal, ni::BehaviorType _behavior)
{
	this->terminal_ = _terminal;
	this->behavior_ = _behavior;
}
// ============================================================================
// InputOperation::set_data_tranfer_mechanism
// ============================================================================
void InputOperation::set_data_tranfer_mechanism(ni::DataTranferType _data_transfer)
{
	this->data_transfer_ = _data_transfer;
}
// ============================================================================
// InputOperation::set_data_tranfer_mechanism
// ============================================================================
void InputOperation::set_min_pulse_width( double _min_pulse_width_seconds )
{
	this->min_pulse_width_seconds_ = _min_pulse_width_seconds;
  this->enable_min_pulse_width_ = true;
}
}//namespace
