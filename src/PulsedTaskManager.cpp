// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    PulsedTaskManager.cpp
//
// = AUTHOR
//    S. Minolli (fully inspired from N. Leclercq ASL PulseTaskManager class)
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <NI660Xsl/PulsedTaskManager.h>

namespace ni660Xsl {

// ============================================================================
// STATICS
// ============================================================================
PulsedTaskManager * PulsedTaskManager::manager = 0;

ACE_Recursive_Thread_Mutex PulsedTaskManager::instance_lock;

// ============================================================================
// PulsedTaskManager::PulsedTaskManager [STATIC]
// ============================================================================
PulsedTaskManager * PulsedTaskManager::instance ()
{
  if (PulsedTaskManager::manager == 0)
  {
    //- perform double-checked locking optimization.
    ACE_MT (ACE_GUARD_RETURN (ACE_Recursive_Thread_Mutex, 
                              ace_mon,
                              PulsedTaskManager::instance_lock, 
                              0));
    //- instanciate
    if (PulsedTaskManager::manager == 0)
    {
       ACE_NEW_RETURN (PulsedTaskManager::manager,
                       PulsedTaskManager,
                       0);

       ACE_REGISTER_FRAMEWORK_COMPONENT(PulsedTaskManager, 
                                        PulsedTaskManager::manager);

       PulsedTaskManager::manager->run();
    }
  }

  return PulsedTaskManager::manager;
}

// ============================================================================
// PulsedTaskManager::close_singleton [STATIC]
// ============================================================================
void PulsedTaskManager::close_singleton ()
{
  ACE_MT (ACE_GUARD (ACE_Recursive_Thread_Mutex, 
                     ace_mon,
                     PulsedTaskManager::instance_lock));

  if (PulsedTaskManager::manager) {
    delete PulsedTaskManager::manager;
    PulsedTaskManager::manager = 0;
  }
}

// ============================================================================
// PulsedTaskManager::PulsedTaskManager
// ============================================================================
PulsedTaskManager::PulsedTaskManager ()
{
  this->reactor_ = new ACE_Reactor();
  ACE_ASSERT(this->reactor_ != 0);
}

// ============================================================================
// PulsedTaskManager::~PulsedTaskManager
// ============================================================================
PulsedTaskManager::~PulsedTaskManager ()
{
  this->reactor_->end_reactor_event_loop();
  this->join();
  delete this->reactor_;
}

// ============================================================================
// PulsedTaskManager::svc
// ============================================================================
ACE_THR_FUNC_RETURN PulsedTaskManager::svc (void *arg)
{
  ACE_UNUSED_ARG(arg);
  this->reactor_->owner(this->id());
  this->reactor_->run_reactor_event_loop();  
  return 0;
}

// ============================================================================
// PulsedTaskManager::register_task
// ============================================================================
int PulsedTaskManager::register_task (PulsedTask * _t, 
                                  void* _arg, 
                                  ACE_Time_Value& _intv)
{
  return this->reactor_->schedule_timer(_t, _arg, _intv, _intv);  
}

// ============================================================================
// PulsedTaskManager::remove_task
// ============================================================================
int PulsedTaskManager::remove_task (PulsedTask * _t)
{
  this->reactor_->cancel_timer(_t);
  ACE_Reactor_Mask masks = ACE_Event_Handler::ALL_EVENTS_MASK 
                         | ACE_Event_Handler::DONT_CALL;
  this->reactor_->remove_handler(_t, masks); 
  return 0;
}

// ============================================================================
// PulsedTaskManager::dll_name
// ============================================================================
const ACE_TCHAR * PulsedTaskManager::dll_name ()
{
	return ACE_LIB_TEXT ("NI660XSL");
}

// ============================================================================
// PulsedTaskManager::name
// ============================================================================
const ACE_TCHAR * PulsedTaskManager::name ()
{
	return ACE_LIB_TEXT ("PulsedTaskManager");
}

} // namespace ni660Xsl

