// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    RetriggerablePulseTrainGeneration.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <iostream>

#include <NI660Xsl/RetriggerablePulseTrainGeneration.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
// RetriggerablePulseTrainGeneration::RetriggerablePulseTrainGeneration
// ============================================================================
RetriggerablePulseTrainGeneration::RetriggerablePulseTrainGeneration()
:OutputOperation(),
 nb_pulses_(10),
 trigger_source_("/Dev1/PFI38"),
 active_edge_(ni::rising_edge)
{
}
// ============================================================================
// RetriggerablePulseTrainGeneration::~RetriggerablePulseTrainGeneration
// ============================================================================
RetriggerablePulseTrainGeneration::~RetriggerablePulseTrainGeneration()
{
}
// ============================================================================
// RetriggerablePulseTrainGeneration::set_nb_pulses
// ============================================================================
void RetriggerablePulseTrainGeneration::set_nb_pulses(int _nb_pulses)
{
    this->nb_pulses_ = _nb_pulses;
}
// ============================================================================
// RetriggerablePulseTrainGeneration::set_trigger_source
// ============================================================================
void RetriggerablePulseTrainGeneration::set_trigger_source(std::string _trigger_source, ni::EdgeType _active_edge)
{
    this->trigger_source_ = _trigger_source;
	this->active_edge_ = _active_edge;
}
// ============================================================================
// RetriggerablePulseTrainGeneration::change_freq_values
// ============================================================================
void RetriggerablePulseTrainGeneration::change_freq_values(ni660Xsl::OutFreqChan _chan)
    throw (ni660Xsl::DAQException)
{
  const char* s =  _chan.chan_name.c_str();
  
  int err = DAQmxSetCOPulseDutyCyc(this->task_handle_, s, _chan.duty_cycle);
  if(err<0 || err>0)
  {
    this->state_ = UNKNOWN;
    throw ni660Xsl::DAQException("changing values failed",
      this->get_string_error(),
      "ContinuousPulseTrainGeneration::change_freq_values",
      err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
  err = DAQmxSetCOPulseFreq(this->task_handle_, s, _chan.frequency);
  if(err<0)
  {
    this->state_ = UNKNOWN;
    throw ni660Xsl::DAQException("changing values failed",
      this->get_string_error(),
      "ContinuousPulseTrainGeneration::change_freq_values",
      err);
  }
  else if (err > 0)
  {
    warn_ = this->get_string_error();
    this->warn_occured_ = true;
    
  }
}
// ============================================================================
// RetriggerablePulseTrainGeneration::change_time_values
// ============================================================================
void RetriggerablePulseTrainGeneration::change_time_values(ni660Xsl::OutTimeChan _chan)
throw (ni660Xsl::DAQException)
{
  
  std::cout<<"RetriggerablePulseTrainGeneration::change_time_values in"<<std::endl;
  //note the functions must be called in the following order
  const char* s =  _chan.chan_name.c_str();
  int err = DAQmxSetCOPulseHighTime(this->task_handle_, s, _chan.high_time);
  if(err<0)
  {
    this->state_ = UNKNOWN;
    throw ni660Xsl::DAQException("changing values failed",
      this->get_string_error(),
      "ContinuousPulseTrainGeneration::change_time_values",
      err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
  
  err = DAQmxSetCOPulseLowTime(this->task_handle_, s, _chan.low_time);
  if(err<0)
  {
    this->state_ = UNKNOWN;
    throw ni660Xsl::DAQException("changing values failed",
      this->get_string_error(),
      "ContinuousPulseTrainGeneration::change_time_values",
      err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
  std::cout<<"RetriggerablePulseTrainGeneration::change_time_values out"<<std::endl;
  
}
// ============================================================================
// RetriggerablePulseTrainGeneration::change_clock_ticks_values
// ============================================================================       
void RetriggerablePulseTrainGeneration::change_clock_ticks_values(ni660Xsl::OutClockTicksChan _chan)
throw (ni660Xsl::DAQException)
{
  const char* s =  _chan.chan_name.c_str();
  
  int err = DAQmxSetCOPulseHighTicks(this->task_handle_, s, _chan.high_ticks);
  if(err<0)
  {
    this->state_ = UNKNOWN;
    throw ni660Xsl::DAQException("changing values failed",
      this->get_string_error(),
      "ContinuousPulseTrainGeneration::change_clk_ticks_values",
      err);
  }
  else if (err > 0)
  {
    warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
  err = DAQmxSetCOPulseLowTicks(this->task_handle_, s, _chan.low_ticks);
  if(err<0)
  {
    this->state_ = UNKNOWN;
    throw ni660Xsl::DAQException("changing values failed",
      this->get_string_error(),
      "ContinuousPulseTrainGeneration::change_clk_ticks_values",
      err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
}
// ============================================================================
// RetriggerablePulseTrainGeneration::configure
// ============================================================================
void RetriggerablePulseTrainGeneration::configure(void) throw (ni660Xsl::DAQException)
{
  //std::cout<<<<"RetriggerablePulseTrainGeneration::configure<-"<<std::endl;
  
  //---------------------channels config---------------------------------
  try
  {
    this->configure_channels();
  }
  catch (const DAQException&)
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }
  
  //---------------------timing config-----------------------------------
  //std::cout<<<<"\t configure timing"<<std::endl;
  int err = DAQmxCfgImplicitTiming(this->task_handle_,ni::finite,this->nb_pulses_); 
  if(err<0)
  {
    throw ni660Xsl::DAQException("configuration failed",
      this->get_string_error(),
      "RetriggerablePulseTrainGeneration::configure",
      err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
  //---------------------trigger config--------------------------------
  // //std::cout<<<<"\t configure start trigger"<<std::endl;
  
  const char* s =  this->trigger_source_.c_str();
  err = DAQmxCfgDigEdgeStartTrig(this->task_handle_, s, this->active_edge_);
  if(err<0)
  {
    throw ni660Xsl::DAQException("configuration failed",
      this->get_string_error(),
      "RetriggerablePulseTrainGeneration::configure",
      err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
  
  //set retrig
  //std::cout<<"config retrig"<<std::endl;
  err = DAQmxSetStartTrigRetriggerable(this->task_handle_, 1);		
  if(err<0)
  {
    
    throw ni660Xsl::DAQException("configuration failed",
      this->get_string_error(),
      "RetriggerablePulseTrainGeneration::configure",
      err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
  //-----------get the current task ready to run-------------------------------
  try
  {
    this->prepare_task();
  }
  catch (const DAQException&)
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }
  
  //std::cout<<<<"RetriggerablePulseTrainGeneration::configure->"<<std::endl;
}

}//namespace
