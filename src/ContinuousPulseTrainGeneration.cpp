// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   ContinuousPulseTrainGeneration.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/ContinuousPulseTrainGeneration.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
// ============================================================================
// ContinuousPulseTrainGeneration::ContinuousPulseTrainGeneration
// ============================================================================
ContinuousPulseTrainGeneration::ContinuousPulseTrainGeneration()
:PulseTrainGeneration(),
pause_trigger_source_("/Dev1/PFI39"),
pause_when_(ni::high)
{
}
// ============================================================================
// ContinuousPulseTrainGeneration::~ContinuousPulseTrainGeneration
// ============================================================================
ContinuousPulseTrainGeneration::~ContinuousPulseTrainGeneration()
{
	//std::cout<<<<"ContinuousPulseTrainGeneration::~ContinuousPulseTrainGeneration"<<std::endl;
}
// ============================================================================
// ContinuousPulseTrainGeneration::set_pause_trigger
// ============================================================================
void ContinuousPulseTrainGeneration::set_pause_trigger(std::string _trigger_source, ni::LevelType _pause_when)
    throw (ni660Xsl::DAQException)
{
    //std::cout<<<<"ContinuousPulseTrainGeneration::set_pause_trigger<-"<<std::endl;
    
    if(this->trig_mode_ == ni::start_trigger)
    {
	throw ni660Xsl::DAQException("impossible to set pause trigger",
	    "The trigger is already used as a start trigger",
	    "ContinuousPulseTrainGeneration::set_pause_trigger");
    }
    
    this->trig_mode_ = ni::pause_trigger;
    this->pause_trigger_source_ = _trigger_source;
    this->pause_when_ = _pause_when;
    
    //std::cout<<<<"ContinuousPulseTrainGeneration::set_pause_trigger->"<<std::endl;
}
// ============================================================================
// ContinuousPulseTrainGeneration::change_freq_values
// ============================================================================
void ContinuousPulseTrainGeneration::change_freq_values(ni660Xsl::OutFreqChan _chan)
    throw (ni660Xsl::DAQException)
{
    const char* s =  _chan.chan_name.c_str();
    
    int err = DAQmxSetCOPulseDutyCyc(this->task_handle_, s, _chan.duty_cycle);
    if(err<0 || err>0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("changing values failed",
	    this->get_string_error(),
	    "ContinuousPulseTrainGeneration::change_freq_values",
	    err);
    }
    else if (err > 0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
    err = DAQmxSetCOPulseFreq(this->task_handle_, s, _chan.frequency);
    if(err<0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("changing values failed",
	    this->get_string_error(),
	    "ContinuousPulseTrainGeneration::change_freq_values",
	    err);
    }
    else if (err > 0)
    {
	warn_ = this->get_string_error();
	this->warn_occured_ = true;
	
    }
}
// ============================================================================
// ContinuousPulseTrainGeneration::change_time_values
// ============================================================================
void ContinuousPulseTrainGeneration::change_time_values(ni660Xsl::OutTimeChan _chan)
    throw (ni660Xsl::DAQException)
{
    
    //note the functions must be called in the following order
    const char* s =  _chan.chan_name.c_str();
    int err = DAQmxSetCOPulseHighTime(this->task_handle_, s, _chan.high_time);
    if(err<0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("changing values failed",
	    this->get_string_error(),
	    "ContinuousPulseTrainGeneration::change_time_values",
	    err);
    }
    else if (err > 0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
    
    err = DAQmxSetCOPulseLowTime(this->task_handle_, s, _chan.low_time);
    if(err<0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("changing values failed",
	    this->get_string_error(),
	    "ContinuousPulseTrainGeneration::change_time_values",
	    err);
    }
    else if (err > 0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
    
}
// ============================================================================
// ContinuousPulseTrainGeneration::change_clock_ticks_values
// ============================================================================       
void ContinuousPulseTrainGeneration::change_clock_ticks_values(ni660Xsl::OutClockTicksChan _chan)
    throw (ni660Xsl::DAQException)
{
    const char* s =  _chan.chan_name.c_str();
    
    int err = DAQmxSetCOPulseHighTicks(this->task_handle_, s, _chan.high_ticks);
    if(err<0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("changing values failed",
	    this->get_string_error(),
	    "ContinuousPulseTrainGeneration::change_clk_ticks_values",
	    err);
    }
    else if (err > 0)
    {
	warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
    err = DAQmxSetCOPulseLowTicks(this->task_handle_, s, _chan.low_ticks);
    if(err<0)
    {
	this->state_ = UNKNOWN;
	throw ni660Xsl::DAQException("changing values failed",
	    this->get_string_error(),
	    "ContinuousPulseTrainGeneration::change_clk_ticks_values",
	    err);
    }
    else if (err > 0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
}
// ============================================================================
// ContinuousPulseTrainGeneration::configure
// ============================================================================
void ContinuousPulseTrainGeneration::configure(void)   throw (ni660Xsl::DAQException)
{
    //std::cout<<<<"ContinuousPulseTrainGeneration::configure<-"<<std::endl;
    
    //---------------------channels config---------------------------------
    try
    {
	this->configure_channels();
    }
    catch (const DAQException&)
    {
	throw;
    }
    catch (...) 
    {
	throw DAQException();
    }
    
    //---------------------timing config-----------------------------------
    //std::cout<<<<"\t configure timing"<<std::endl;
    int err = DAQmxCfgImplicitTiming(this->task_handle_,ni::continuous,10); 
    if(err<0)
    {
	throw ni660Xsl::DAQException("configuration failed",
	    this->get_string_error(),
	    "ContinuousTrainGeneration::configure",
	    err);
    }
    else if (err > 0)
    {
	this->warn_ = this->get_string_error();
	this->warn_occured_ = true;
    }
    
    //---------------------trigger config----------------------------------
    if(this->trig_mode_ == ni::start_trigger)
    {
	//std::cout<<<<"\t configure start trigger"<<std::endl;
	const char* s =  this->start_trigger_source_.c_str();
	int err = DAQmxCfgDigEdgeStartTrig(this->task_handle_, s, this->active_edge_);
	if(err<0)
	{
	    throw ni660Xsl::DAQException("configuration failed",
		this->get_string_error(),
		"ContinuousTrainGeneration::configure",
		err);
	}
	else if (err > 0)
	{
	    this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
	}
    }
    else if(this->trig_mode_ == ni::pause_trigger)//pause trigger
    {
	//std::cout<<<<"\t configure pause trigger"<<std::endl;
	//config the trigger to be a digital pause trigger
	int err = DAQmxSetTrigAttribute(this->task_handle_,DAQmx_PauseTrig_Type,DAQmx_Val_DigLvl);
	if(err<0 )
	{
	    throw ni660Xsl::DAQException("configuration failed",
		this->get_string_error(),
		"ContinuousPulseTrainGeneration::configure",
		err);
	}
	else if (err > 0)
	{
	    this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
	}
	//config trigger src
	const char* s =  this->pause_trigger_source_.c_str();
	err = DAQmxSetTrigAttribute(this->task_handle_,DAQmx_DigLvl_PauseTrig_Src,s);
	if(err<0 )
	{
	    throw ni660Xsl::DAQException("configuration failed",
		this->get_string_error(),
		"ContinuousPulseTrainGeneration::configure",
		err);
	}
	else if (err > 0)
	{
	    warn_ = this->get_string_error();
	    this->warn_occured_ = true;
	}
	//set pause when
	err = DAQmxSetTrigAttribute (this->task_handle_, DAQmx_DigLvl_PauseTrig_When,this->pause_when_);
	if(err<0 )
	{
	    throw ni660Xsl::DAQException("configuration failed",
		this->get_string_error(),
		"ContinuousPulseTrainGeneration::configure",
		err);
	}
	else if (err > 0)
	{
	    this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
	}
    }
    
    //-----------get the current task ready to run-------------------------------
    try
    {
	this->prepare_task();
    }
    catch (const DAQException&)
    {
	throw;
    }
    catch (...) 
    {
	throw DAQException();
    }
    
    //std::cout<<<<"ContinuousPulseTrainGeneration::configure->"<<std::endl;
}

}//namespace
