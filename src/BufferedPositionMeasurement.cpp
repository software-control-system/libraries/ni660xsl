// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//   BufferedPositionMeasurement.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/BufferedPositionMeasurement.h>

namespace ni660Xsl
{

// ============================================================================
// scaled_buffer_callback_pos
//
// Callback function for position buffered acquisition.
// @param _taskHandle NI task handler.
// @param _evt_type Event type associated to the callback function.
// @param _samples Number of samples of the buffer.
// @param _data Callback data pointer.
// ============================================================================
int32 CVICALLBACK scaled_buffer_callback_pos(ni::DAQTaskHandle _taskHandle, 
                                         int32 _evt_type, 
                                         uInt32 _samples, 
                                         void *_data)
{
  //std::cout << "scaled_buffer_callback_pos() entering..." << std::endl;

  BufferedPositionMeasurement* buff_class_ = reinterpret_cast<BufferedPositionMeasurement*>(_data);

  if (buff_class_)
  {
    ACE_GUARD_RETURN(ACE_SYNCH_MUTEX, ace_mon, buff_class_->stop_lock_, 0);
	
	  // If stop in progress, do not read data
	  if (!buff_class_->stop_flag_)
	  {
      //std::cout << "scaled_buffer_callback_pos() no stop in progress ****" << std::endl;
      // restart timeout
      if (buff_class_->tmo_caller_)
      {
        buff_class_->tmo_caller_->stop();
        buff_class_->tmo_caller_->start(buff_class_, 
          (unsigned long)(buff_class_->get_timeout() * 1000));
      }

      // call internal function to read data from board
      buff_class_->get_scaled_buffer();
	  }
  }

  return 0;
}

// ============================================================================
// BufferedPositionMeasurement::BufferedPositionMeasurement
// ============================================================================
BufferedPositionMeasurement::BufferedPositionMeasurement()
:PositionMeasurement(),
BufferedAcquisition()
{
  tmo_caller_ = NULL;
}

// ============================================================================
// BufferedPositionMeasurement::~BufferedPositionMeasurement
// ============================================================================
BufferedPositionMeasurement::~BufferedPositionMeasurement()
{
  if (tmo_caller_)
  {
    tmo_caller_->stop();
    delete tmo_caller_;
    tmo_caller_ = NULL;
  }
}

// ============================================================================
// BufferedPositionMeasurement::configure
// ============================================================================
void BufferedPositionMeasurement::configure (void) 
  throw (ni660Xsl::DAQException)
{ 
  //std::cout<<<<"BufferedPositionMeasurement::configure<-"<<std::endl;
  
  //-------------------------configure channel------------------------------------------
  try
  {
    this->configure_channel();
  }
  catch (const DAQException&)
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }
  
  //-------------------------configure timing------------------------------------------
  //std::cout<<<<"BufferedPositionMeasurement::config timing"<<std::endl;
  const char* clk =  this->sample_clk_.c_str();

  int err = DAQmxCfgSampClkTiming(this->task_handle_, clk, this->max_rate_, 
    this->active_edge_, this->mode_, this->nb_samples_);
  
  if (err < 0)
  {
    throw ni660Xsl::DAQException("Timing configuration failed!",
	    this->get_string_error(),
	    "BufferedPositionMeasurement::configure()",
	    err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }

  if (this->use_callback_)
  {
    //-------------------------Register callback event-------------------------------
    //- Register a callback which will be called when the number of samples will be acquired.
    err = DAQmxRegisterEveryNSamplesEvent(this->task_handle_, ni::intoBuffer, 
                this->nb_samples_, 0, scaled_buffer_callback_pos, 
                static_cast<void*> (this));
    
    if (err < 0)
    {
	    throw ni660Xsl::DAQException("callback configuration failed!",
	      this->get_string_error(),
	      "BufferedPositionMeasurement::configure()",
	      err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
  }

  if (this->use_callback_)
  {
    //-------------------------reset acquisition buffer-------------------------------------------
    err = DAQmxResetBufInputBufSize(this->task_handle_);

    if (err < 0)
    {
	    throw ni660Xsl::DAQException("Buffer reset failed!",
	      this->get_string_error(),
	      "BufferedPositionMeasurement::configure()",
	      err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
      this->warn_occured_ = true;
    }
  }

  //-------------------------configure buffer-------------------------------------------
	//std::cout<<<<"BufferedPositionMeasurement::config buffer"<<std::endl;
  if (this->use_callback_)
  {
    // In DMA mode, if samples number = total number of points, configure
    // input buffer size with 2 * total_nb_pts_, because input buffer size
    // should be strictly above total_nb_pts_ in callback mode
    if ((this->data_transfer_ == ni::dma) &&
        (this->nb_samples_ == this->total_nb_pts_))
    {
      err = DAQmxCfgInputBuffer(this->task_handle_, 2 * this->total_nb_pts_);
    }
    // If total number of points is null (means infinite acquisition):
    // set input buffer size with 2 * nb_samples_
    else if (this->total_nb_pts_ == 0)
    {
      err = DAQmxCfgInputBuffer(this->task_handle_, 2 * this->nb_samples_);
    }
    else
    {
      // Configure the input buffer size with the total number of point to be received
      err = DAQmxCfgInputBuffer(this->task_handle_, this->total_nb_pts_);
    }
  }
  else
  {
    err = DAQmxCfgInputBuffer(this->task_handle_, this->nb_samples_);
  }
  
  if(err < 0)
  {
    throw ni660Xsl::DAQException("Buffer configuration failed",
	    this->get_string_error(),
	    "BufferedPositionMeasurement::configure()",
	    err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
	
  //-------------------------configure start trigger------------------------------------------
  if (use_start_trig_)
  {
	  //config trigger to be a "arm start trig"
	  int err = DAQmxSetArmStartTrigType(this->task_handle_, DAQmx_Val_DigEdge);
	
    if (err < 0)
	  {
      throw ni660Xsl::DAQException("Start trigger configuration failed!",
        this->get_string_error(),
		    "BufferedPositionMeasurement::configure()",
		    err);
	  }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
    }
	
	  // configure trigger pin
	  const char* s =  this->start_trigger_source_.c_str();
	  err = DAQmxSetDigEdgeArmStartTrigSrc(this->task_handle_, s);
	  if(err < 0)
	  {
	    throw ni660Xsl::DAQException("Start trigger src configuration failed!",
          this->get_string_error(),
		      "BufferedPositionMeasurement::configure()",
		      err);
    }
    else if (err > 0)
    {
      this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
    }
	
	  // configure edge 
	  err = DAQmxSetDigEdgeArmStartTrigEdge(this->task_handle_, this->trig_active_edge_);
	  
    if (err < 0)
    {
	    throw ni660Xsl::DAQException("Start trigger edge configuration failed!",
		      this->get_string_error(),
		      "BufferedPositionMeasurement::configure()",
		      err);
    }
    else if (err > 0)
    {
	    this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
    }
  }
  
  //---------------------configure read start point-----------------------------------
  err = DAQmxSetReadRelativeTo(this->task_handle_, this->start_point_);

  if (err < 0)
  {
    throw ni660Xsl::DAQException("Start point configuration failed!",
	    this->get_string_error(),
	    "BufferedPositionMeasurement::configure()",
	    err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
    
  //---------------------configure offset-----------------------------------
  err = DAQmxSetReadOffset(this->task_handle_, this->offset_);
  
  if (err < 0)
  {
    throw ni660Xsl::DAQException("Offset configuration failed!",
	    this->get_string_error(),
	    "BufferedPositionMeasurement::configure()",
	    err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
	
  //-------------------------configure tc------------------------------------------
	if( this->terminal_.size() != 0)
	{ 
		const char* term =  this->terminal_.c_str();
		err = DAQmxSetExportedCtrOutEventOutputTerm(this->task_handle_, term);

		if (err < 0)
		{
			throw ni660Xsl::DAQException("Term configuration failed!",
				this->get_string_error(),
				"BufferedPositionMeasurement::configure()",
        err);
		}
		else if (err > 0)
		{
			this->warn_ = this->get_string_error();
			this->warn_occured_ = true;
		}

		err = DAQmxSetExportedCtrOutEventOutputBehavior(this->task_handle_, this->behavior_);

		if (err < 0)
		{
			throw ni660Xsl::DAQException("term behaviour configuration failed()",
				this->get_string_error(),
				"BufferedPositionMeasurement::configure",
        err);
		}
		else if (err > 0)
		{
			this->warn_ = this->get_string_error();
			this->warn_occured_ = true;
		}

	/*	err = DAQmxSetExportedCtrOutEventPulsePolarity(this->task_handle_, DAQmx_Val_ActiveHigh);
		if(err < 0)
		{
			throw ni660Xsl::DAQException("configuration failed",
				this->get_string_error(),
				"BufferedPositionMeasurement::configure");
		}
		else if(err > 0)
		{
			this->warn_ = this->get_string_error();
			this->warn_occured_ = true;
		}*/

	}
   
  //---------------------configure autostart------------------------------------------

  //to call get_buffer() does not start the acquisition.
  err = DAQmxSetReadAutoStart(this->task_handle_, FALSE);
  
  if (err < 0)
  {
    throw ni660Xsl::DAQException("Autostart configuration failed!",
	    this->get_string_error(),
	    "BufferedPositionMeasurement::configure()",
	    err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
	
  //---------------------configure read mode-----------------------------------------------
	err = DAQmxSetReadWaitMode(this->task_handle_, this->read_mode_);

	if (err < 0)
  {
    throw ni660Xsl::DAQException("Read mode configuration failed!",
	    this->get_string_error(),
	    "BufferedEventCounting::configure()",
	    err);
  }
  else if (err > 0)
  {
    this->warn_ = this->get_string_error();
    this->warn_occured_ = true;
  }
  
  //---------------------get task ready to start------------------------------------------------
  try
  {
    this->prepare_task();
  }
  catch (const DAQException&)
  {
    throw;
  }
  catch (...) 
  {
    throw DAQException();
  }

  //std::cout<<<<"BufferedPositionMeasurement::configure->"<<std::endl;
}

// ============================================================================
// BufferedPositionMeasurement::start
// ============================================================================
void BufferedPositionMeasurement::start(void)
  throw (ni660Xsl::DAQException)
{
  stop_flag_ = false;

  // first call mother class function to start acquisition
  CounterBasedOperation::start();

  // then, in case of callback mode enabled, start timeout pulsed object
  if (use_callback_)
  {	
    tmo_caller_ = new TimeoutCallerPos();
    if (tmo_caller_ == 0) 
    {
      throw ni660Xsl::DAQException("start failed",
	      "Failed to instanciate timeout caller in callback mode!",
	      "BufferedPositionMeasurement::start");
    }
    tmo_caller_->start(this, (unsigned long)(timeout_ * 1000));
  }
}

// ============================================================================
// BufferedPositionMeasurement::stop
// ============================================================================
void BufferedPositionMeasurement::stop(void)
  throw (ni660Xsl::DAQException)
{
  // set flag to avoid race condition with callback
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, ace_mon, this->stop_lock_, 
    throw ni660Xsl::DAQException("Stop failed!",
				"Device is handling data from driver, cannot stop!",
				"BufferedPositionMeasurement::stop",
        666));

  stop_flag_ = true;

  // first call mother class function to stop acquisition
  CounterBasedOperation::stop();

  // then, in case of callback mode enabled, stop timeout pulsed object
  if (use_callback_)
  {	
    if (tmo_caller_) 
    {
      tmo_caller_->stop();
      delete tmo_caller_;
      tmo_caller_ = NULL;
    }
  }
}

// ============================================================================
// BufferedPositionMeasurement::abort
// ============================================================================
void BufferedPositionMeasurement::abort(void)
  throw (ni660Xsl::DAQException)
{
  // set flag to avoid race condition with callback
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, ace_mon, this->stop_lock_, 
    throw ni660Xsl::DAQException("Abort failed!",
				"Device is handling data from driver, cannot abort!",
				"BufferedPositionMeasurement::abort",
        666));

  stop_flag_ = true;

  // first call mother class function to abort acquisition
  CounterBasedOperation::abort();

  // then, in case of callback mode enabled, stop timeout pulsed object
  if (use_callback_)
  {	
    if (tmo_caller_) 
    {
      tmo_caller_->stop();
      delete tmo_caller_;
      tmo_caller_ = NULL;
    }
  }
}

// ============================================================================
// BufferedPositionMeasurement::abort_and_release
// ============================================================================
void BufferedPositionMeasurement::abort_and_release(void)
  throw (ni660Xsl::DAQException)
{
  // set flag to avoid race condition with callback
  ACE_GUARD_REACTION(ACE_SYNCH_MUTEX, ace_mon, this->stop_lock_, 
    throw ni660Xsl::DAQException("Abort and release failed!",
				"Device is handling data from driver, cannot abort!",
				"BufferedPositionMeasurement::abort_and_release",
        666));

  stop_flag_ = true;

  // first call mother class function to abort acquisition
  CounterBasedOperation::abort();

  // then, in case of callback mode enabled, stop timeout pulsed object
  if (use_callback_)
  {	
    if (tmo_caller_) 
    {
      tmo_caller_->stop();
      delete tmo_caller_;
      tmo_caller_ = NULL;
    }
  }

  // then release counter
  CounterBasedOperation::release();
}

// ============================================================================
// BufferedPositionMeasurement::handle_tmo_cb
// ============================================================================
void BufferedPositionMeasurement::handle_tmo_cb(void)
{
  // The DAQmx driver, in version 14.0 and above, manage the overrun error 
  // in the callback function => see get_raw_buffer()
  // We only need here to manage the timeout error
  this->handle_timeout();
}

}//namespace
