// ============================================================================
//
// = CONTEXT
//    TANGO Project - ni660X Support Library
//
// = FILENAME
//    OutputOperation.h
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
//#include <iostream>

#include <NI660Xsl/OutputOperation.h>
#include <NI660Xsl/Exception.h>

namespace ni660Xsl
{
    
    OutputOperation::OutputOperation()
	:CounterBasedOperation()
    { 
	//std::cout<<<<"OutputOperation::OutputOperation"<<std::endl;
    }
    OutputOperation::~OutputOperation()
    {	
	//std::cout<<<<"OutputOperation::~OutputOperation"<<std::endl;
    }
    // ============================================================================
    // OutputOperation::add_frequency_channel
    // ============================================================================
    void OutputOperation::add_frequency_channel(ni660Xsl::OutFreqChan _chan)
    {
	//std::cout<<<<"OutputOperation::add_frequency_channel<-"<<std::endl;
	//add the chan to the vector
	this->freq_chans_.push_back(_chan);
	 //std::cout<<<<"OutputOperation::add_frequency_channel->"<<std::endl;
    }
	// ============================================================================
    // OutputOperation::remove_last_added_frequency_channel
    // ============================================================================
	void OutputOperation::remove_last_added_frequency_channel()
	{
		if(!this->freq_chans_.empty())
			this->freq_chans_.pop_back();
	}
    // ============================================================================
    // OutputOperation::add_time_channel
    // ============================================================================	
    void OutputOperation::add_time_channel(ni660Xsl::OutTimeChan _chan)
    {
	//std::cout<<<<"OutputOperation::add_time_channel<-"<<std::endl;
	//add the chan to the vector
	this->time_chans_.push_back(_chan);
	//std::cout<<<<"OutputOperation::add_time_channel->"<<std::endl;
    }
	// ============================================================================
    // OutputOperation::remove_last_added_time_channel
    // ============================================================================
	void OutputOperation::remove_last_added_time_channel()
	{
		if(!this->time_chans_.empty())
			this->time_chans_.pop_back();
	}
    // ============================================================================
    // OutputOperation::add_clock_ticks_channel
    // ============================================================================	
    void OutputOperation::add_clock_ticks_channel(ni660Xsl::OutClockTicksChan _chan)
    {
	//std::cout<<<<"OutputOperation::add_clock_ticks_channel<-"<<std::endl;
	//add the chan to the vector	
       this->clk_ticks_chans_.push_back(_chan);
       //std::cout<<<<"OutputOperation::add_clock_ticks_channel->"<<std::endl;
    }
	// ============================================================================
    // OutputOperation::remove_last_added_clock_ticks_channel
    // ============================================================================
	void OutputOperation::remove_last_added_clock_ticks_channel()
	{
		if(!this->clk_ticks_chans_.empty())
			this->clk_ticks_chans_.pop_back();
	}
 // ============================================================================
    // OutputOperation::wait_finished
    // ============================================================================
    bool OutputOperation::wait_finished(double _time_to_wait)
	throw (ni660Xsl::DAQException)
    {
	bool rtn =false;
	
	int err = DAQmxWaitUntilTaskDone(this->task_handle_, _time_to_wait);
	if(err = -200560) //the function has returned before task has finished
	{
	    rtn = true;
	}
	else if(err < 0)
	{
	    throw ni660Xsl::DAQException("wait failed",
		this->get_string_error(),
		"CounterBasedOperation::wait_finished",
		err);
	}
	else if (err > 0)
	{
	    this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
	}
	return rtn;
    }
// ============================================================================
    // OutputOperation::is_done
    // ============================================================================
bool OutputOperation::is_done(bool& _is_done) throw (ni660Xsl::DAQException)
	{
		 	bool rtn =false;
	unsigned long done;
	int err = DAQmxIsTaskDone(this->task_handle_, &done);
	if(done == 0)
		_is_done = false;
	else
		_is_done = true;
/*	if(err = -200560) //the function has returned before task has finished
	{
	    rtn = true;
			std::cout<<<<"OutputOperation::is_done - "<<std::endl;
	}*/
	if(err < 0)
	{
	    throw ni660Xsl::DAQException("wait failed",
		this->get_string_error(),
		"CounterBasedOperation::wait_finished",
		err);
	}
	else if (err > 0)
	{
	    this->warn_ = this->get_string_error();
	    this->warn_occured_ = true;
	}
	return rtn;
	}
    // ============================================================================
    // OutputOperation::configure_channels
    // ============================================================================
    void OutputOperation::configure_channels(void)
	throw (ni660Xsl::DAQException)
    {
	//std::cout<<<<"OutputOperation::configure_channels<-"<<std::endl;
	
	//------------------freq channels config-----------------------
    size_t i;
	for(i = 0; i<this->freq_chans_.size(); i++)
	{
	    //std::cout<<<<"\t config freq"<<std::endl;
	    const char* name = this->freq_chans_[i].chan_name.c_str();
	    int err = DAQmxCreateCOPulseChanFreq(this->task_handle_, 
		name,
		"",
		ni::hertz,
		this->freq_chans_[i].idle_state,
		this->freq_chans_[i].initial_delay,
		this->freq_chans_[i].frequency,
		this->freq_chans_[i].duty_cycle);
	    if(err < 0)
	    {
		throw ni660Xsl::DAQException("configuration failed",
		    this->get_string_error(),
		    "OutputOperation::configure_channels",
		    err);
	    }
	    else if (err > 0)
	    {
		this->warn_ = this->get_string_error();
		this->warn_occured_ = true;
	    }
	    //configure the output terminal 
	    if(this->freq_chans_[i].output_terminal.size() != 0)
	    {
		const char* s1 = this->freq_chans_[i].chan_name.c_str();
		const char* s2 = this->freq_chans_[i].output_terminal.c_str();
		int err = DAQmxSetCOPulseTerm(this->task_handle_, s1, s2);
		if(err < 0)
		{
		    throw ni660Xsl::DAQException("configuration failed",
			this->get_string_error(),
			"OutputOperation::configure_channels",
			err);
		}
		else if (err > 0)
		{
		    this->warn_ = this->get_string_error();
		    this->warn_occured_ = true;
		}
	    }
	}
	
	//------------------time channels config-----------------------
	for(i = 0; i<time_chans_.size(); i++)
	{
	    //std::cout<<<<"\t config time"<<std::endl;
	    const char* name = this->time_chans_[i].chan_name.c_str();
	    int err = DAQmxCreateCOPulseChanTime(this->task_handle_, 
		name,
		"",
		ni::seconds,
		this->time_chans_[i].idle_state,
		this->time_chans_[i].initial_delay,
		this->time_chans_[i].low_time,
		this->time_chans_[i].high_time);
	    if(err<0)
	    {
		throw ni660Xsl::DAQException("configuration failed",
		    this->get_string_error(),
		    "OutputOperation::configure_channels",
		    err);
	    }
	    else if (err > 0)
	    {
		this->warn_ = this->get_string_error();
		this->warn_occured_ = true;
	    }
	    //configure the output terminal 
	    if(this->time_chans_[i].output_terminal.size() != 0)
	    {
		//std::cout<<<<"\t config output_terminal"<<std::endl;
		const char* s1 = this->time_chans_[i].chan_name.c_str();
		const char* s2 = this->time_chans_[i].output_terminal.c_str();
		int err = DAQmxSetCOPulseTerm(this->task_handle_, s1, s2);
		if(err<0 )
		{
		    throw ni660Xsl::DAQException("configuration failed",
			this->get_string_error(),
			"OutputOperation::configure_channels",
			err);
		}
		else if (err > 0)
		{
		    this->warn_ = this->get_string_error();
		    this->warn_occured_ = true;
		}
	    }
	}
	
	//------------------clk ticks channels config-----------------------
	for(i = 0; i<clk_ticks_chans_.size(); i++)
	{
	    //std::cout<<<<" \t config clk_ticks"<<std::endl;
	    
	    const char* name = this->clk_ticks_chans_[i].chan_name.c_str();
	    const char* clk = this->clk_ticks_chans_[i].clk_source.c_str();
	    int err = DAQmxCreateCOPulseChanTicks(this->task_handle_, 
		name,
		"",
		clk,
		this->clk_ticks_chans_[i].idle_state,
		this->clk_ticks_chans_[i].initial_delay,
		this->clk_ticks_chans_[i].low_ticks,
		this->clk_ticks_chans_[i].high_ticks);
	    if(err<0)
	    {
		throw ni660Xsl::DAQException("configuration failed",
			this->get_string_error(),
			"OutputOperation::configure_channels",
			err);
	    }
	    else if (err > 0)
	    {
		this->warn_ = this->get_string_error();
		this->warn_occured_ = true;
	    }
	    //configure the output terminal 
	    if(this->clk_ticks_chans_[i].output_terminal.size() != 0)
	    {
		//std::cout<<<<"\t config output_terminal"<<std::endl;
		const char* s1 = this->clk_ticks_chans_[i].chan_name.c_str();
		const char* s2 = this->clk_ticks_chans_[i].output_terminal.c_str();
		int err = DAQmxSetCOPulseTerm(this->task_handle_, s1, s2);
		if(err < 0)
		{
		    throw ni660Xsl::DAQException("configuration failed",
			this->get_string_error(),
			"OutputOperation::configure_channels",
			err);
		}
		else if (err > 0)
		{
		   this->warn_ = this->get_string_error();
		   this->warn_occured_ = true;
		}
	    }
	}
	
	//std::cout<<<<"OutputOperation::configure_channels->"<<std::endl;
	
    }
    
}//namesapce	
