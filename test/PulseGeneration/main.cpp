// ============================================================================
//
// = CONTEXT
//    TANGO Project - NI660X Support Library
//      test Finite Pulse Generation and Continuous Pulse Generation.
//
// = FILENAME
//   main.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================
#include <iostream>
#include <string>
#include "Exception.h"
#include <ace/ACE.h>
#include "ContinuousPulseTrainGeneration.h"
#include "FinitePulseTrainGeneration.h"

int main (int, char**)
{
    ni660Xsl::ContinuousPulseTrainGeneration* cont_train = 0; 
    ni660Xsl::FinitePulseTrainGeneration* finite_train =  0;
    
    for(int i = 0; i<3; i++)
    {
        try
        {
            std::cout<<"===================test 6602============================="<<std::endl;
            if(ni660Xsl::SystemProperties::warn_occured())
            {
                std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
            }
            std::cout<<"driver version: "<<ni660Xsl::SystemProperties::get_driver_version()<<std::endl;
            if(ni660Xsl::SystemProperties::warn_occured())
            {
                std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
            }
            std::cout<<"product type:"<<ni660Xsl::SystemProperties::get_product_type("Dev1")<<std::endl;
            if(ni660Xsl::SystemProperties::warn_occured())
            {
                std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
            }
            std::cout<<"devices: "<<ni660Xsl::SystemProperties::get_all_devices_names()<<std::endl;
            if(ni660Xsl::SystemProperties::warn_occured())
            {
                std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
            }
            
            std::cout<<"========================================================="<<std::endl;
            
            //--------------------reset the hw----------------------------------------
            ni660Xsl::SystemProperties::reset_device("Dev1");
            if(ni660Xsl::SystemProperties::warn_occured())
            {
                std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
            }
            
            
            //--------------------create operations----------------------------------------
            cont_train = new ni660Xsl::ContinuousPulseTrainGeneration();
            if(cont_train == 0)
            {
                std::cout<<"out mem"<<std::endl;
            }
            finite_train = new ni660Xsl::FinitePulseTrainGeneration();
            if(finite_train == 0)
            {
                std::cout<<"out mem"<<std::endl;
            }
            
            
            //-------------------config one channel------------------------------------------ 
            ni660Xsl::OutTimeChan chan;
            
            chan.chan_name = "/Dev1/ctr0";
            chan.idle_state = ni::low;
            chan.initial_delay = 0.0;//secs
            chan.low_time = 0.0002;//secs
            chan.high_time = 0.0001;//secs
            //chan.output_terminal = "/Dev1/PFI24";
            
            cont_train->add_time_channel(chan);
            
            //-------------------config one channel------------------------------------------
            ni660Xsl::OutClockTicksChan chan2;
            
            chan2.chan_name = "/Dev1/ctr3";
            chan2.clk_source = "/Dev1/PFI39";
            chan2.idle_state = ni::low;
            chan2.initial_delay = 4;//clk ticks
            chan2.low_ticks = 2;//clk ticks
            chan2.high_ticks = 2;//clk ticks
            //chan2.output_terminal = "/Dev1/PFI20";
            
            cont_train->add_clock_ticks_channel(chan2);
            
            //-------------------config one channel------------------------------------------
            ni660Xsl::OutFreqChan chan3;
            
            chan3.chan_name= "/Dev1/ctr4";
            chan3.idle_state = ni::low;
            chan3.initial_delay = 0.0;//secs
            chan3.frequency = 10;
            chan3.duty_cycle = 0.5;
            //chan.output_terminal = "/Dev1/PFI24";
            
            finite_train->add_frequency_channel(chan3);
            
            //generate 150 pulses
            finite_train->set_nb_pulses(150);
            
            //-------------------config triggering -----------------------------------------
            
            //cont_train->set_pause_trigger("/Dev1/PFI38", ni::low);
            //or
            //cont_train->set_start_trigger("/Dev1/PFI38",ni::rising_edge);
            
            //-------------------init and config-----------------------------------------
            finite_train->init();
            if(finite_train->warn_occured())
            {
            std::cout<<finite_train->get_warn_string()<<std::endl;
            }
            finite_train->configure();
            if(finite_train->warn_occured())
            {
            std::cout<<finite_train->get_warn_string()<<std::endl;
            }
            
            cont_train->init();
            if(cont_train->warn_occured())
            {
                std::cout<<cont_train->get_warn_string()<<std::endl;
            }
            cont_train->configure();  
            if(cont_train->warn_occured())
            {
                std::cout<<cont_train->get_warn_string()<<std::endl;
            }
            
            //-------------------start generations-----------------------------------------
            cont_train->start();
            if(cont_train->warn_occured())
            {
                std::cout<<cont_train->get_warn_string()<<std::endl;
            }
            finite_train->start();
            if(finite_train->warn_occured())
            {
                std::cout<<finite_train->get_warn_string()<<std::endl;
            }
            
            //------------------------------------------------------------------------------
            //wait until finite generation is finished
            bool not_finished = finite_train->wait_finished(-1);
            if(not_finished)
                std::cout<<"generation was not finished"<<std::endl;
            if(finite_train->warn_occured())
            {
                std::cout<<finite_train->get_warn_string()<<std::endl;
            }
            
            chan.low_time = 0.1;//secs
            chan.high_time = 0.1;//secs
            
            cont_train->change_time_values(chan);
            if(cont_train->warn_occured())
            {
                std::cout<<cont_train->get_warn_string()<<std::endl;
            }
            
            chan2.low_ticks = 100;//clk ticks
            chan2.high_ticks = 100;//clk ticks
            
            cont_train->change_clock_ticks_values(chan2);
            if(cont_train->warn_occured())
            {
                std::cout<<cont_train->get_warn_string()<<std::endl;
            }
            
            //wait for 2 secs
            not_finished = cont_train->wait_finished(3);
            if(not_finished)
                std::cout<<"generation was not finished"<<std::endl;
            if(cont_train->warn_occured())
            {
                std::cout<<cont_train->get_warn_string()<<std::endl;
            }
            
            //-------------------stop generations-----------------------------------------
            finite_train->stop();
            if(finite_train->warn_occured())
            {
                std::cout<<finite_train->get_warn_string()<<std::endl;
            }
            cont_train->stop();
            if(cont_train->warn_occured())
            {
                std::cout<<cont_train->get_warn_string()<<std::endl;
            }
            
            //-------------------release--------------------------------------------------
            cont_train->release();
            if(cont_train->warn_occured())
            {
                std::cout<<cont_train->get_warn_string()<<std::endl;
            }
            finite_train->release();
            if(finite_train->warn_occured())
            {
                std::cout<<finite_train->get_warn_string()<<std::endl;
            }
            
        }catch(const ni660Xsl::DAQException &e)
        {
            std::cout<<"*************EXCEPTION**************"<<std::endl;
            for (int i = 0; i < e.errors.size(); i++) 
            {
                std::cout<<"- reason:\n \t"<< e.errors[i].reason.c_str()<<std::endl; 
                std::cout<<"- desc:\n"<< e.errors[i].desc.c_str()<<std::endl; 
                std::cout<<"- origin:\n \t"<< e.errors[i].origin.c_str()<<std::endl; 
                //	std::cout<<"- code:\n \t"<< e.errors[i].code<<std::endl;
                //	std::cout<<"- severity:\n \t"<< e.errors[i].severity<<std::endl; 
            }
            std::cout<<"************************************"<<std::endl;
        
            //cont_train->release();
            //	finite_train->release();
        }
    
        if(finite_train)
        {
            delete finite_train;
            finite_train = 0;
        }
        if(cont_train)
        {
            delete cont_train;
            cont_train = 0;
        }
    
    }
    return 0;
}
