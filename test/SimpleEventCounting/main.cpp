// ============================================================================
//
// = CONTEXT
//    TANGO Project - NI660X Support Library
//      test Simple event counting and Finite Pulse Generation.
//
// = FILENAME
//   main.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================
#include <ace/ACE.h>
#include <iostream>

#include "NI660Xsl/Exception.h"
#include "NI660Xsl/SimpleEventCounting.h"
#include "NI660Xsl/FinitePulseTrainGeneration.h"

int main(int argc,char *argv[])
{    
    ni660Xsl::SimpleEventCounting* count = 0;
    ni660Xsl::FinitePulseTrainGeneration* finite_train = 0;
    
    try
    {    
        //---------------------reset device--------------------------------------
        //note: device must be reset to remove routes
        ni660Xsl::SystemProperties::reset_device("Dev1");
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
               
        //---------------------create  new operations on device-----------------------
        count =  new ni660Xsl::SimpleEventCounting();
        if(count == 0)
        {
            std::cout<<"out mem"<<std::endl;
        }
        
        finite_train =  new ni660Xsl::FinitePulseTrainGeneration();
        if(finite_train == 0)
        {
            std::cout<<"out mem"<<std::endl;
        }
        
        /*
        //---------------------route signals--------------------------------------
        ni660Xsl::SystemProperties::route_terminals("/Dev1/Ctr0InternalOutput", "/Dev1/Ctr7Gate", false) ;
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        */

        //---------------------config pulse generation chan------------------------
        ni660Xsl::OutTimeChan chan2;
        chan2.chan_name = "/Dev1/ctr0";
        chan2.idle_state = ni::low;
        chan2.initial_delay = 0.0;//secs
        chan2.low_time = 1;//secs
        chan2.high_time = 1;//secs
        //chan2.output_terminal = "RTSI0";
        
        finite_train->add_time_channel(chan2);
        
        //generate one pulse after each trigger
        finite_train->set_nb_pulses(1);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
        
        //---------------------config event counting chan----------------------------
        ni660Xsl::EventCountChan chan;
        chan.chan_name = "/Dev1/ctr1";
        chan.edge = ni::rising_edge;
        chan.initial_count = 0;
        chan.count_direction = ni::count_up;
        
        count->add_input_channel(chan);
        count->set_timeout(3.0);
        //count->set_pause_trigger("/Dev1/Ctr3InternalOutput", ni::low);
        
        //------------------init all tasks--------------------------------------------
        count->init();
        if(count->warn_occured())
        {
            std::cout<<count->get_warn_string()<<std::endl;
        }
        
        finite_train->init();
        if(finite_train->warn_occured())
        {
            std::cout<<finite_train->get_warn_string()<<std::endl;
        }
        
        //------------------config hw for each task-------------------------------------
        finite_train->configure();
        if(finite_train->warn_occured())
        {
            std::cout<<finite_train->get_warn_string()<<std::endl;
        }
        
        count->configure();
        if(count->warn_occured())
        {
            std::cout<<count->get_warn_string()<<std::endl;
        }
        
        //------------------start event counting-------------------------------------
        count->start();
        if(count->warn_occured())
        {
            std::cout<<count->get_warn_string()<<std::endl;
        }
        
        //------------------start pulse train----------------------------------------
        finite_train->start();
        if(finite_train->warn_occured())
        {
            std::cout<<finite_train->get_warn_string()<<std::endl;
        }
        
        //--------wait for the pulse generation to be finished-----------------------
        finite_train->wait_finished(-1);
        if(finite_train->warn_occured())
        {
            std::cout<<finite_train->get_warn_string()<<std::endl;
        }
        
        //-----------get the current value of the counter----------------------------
        std::cout<<count->get_current_raw_value()<<std::endl;
        
        finite_train->stop();
        if(finite_train->warn_occured())
        {
            std::cout<<finite_train->get_warn_string()<<std::endl;
        }
        //-----------------stop all tasks--------------------------------------------
        count->stop();
        if(count->warn_occured())
        {
            std::cout<<count->get_warn_string()<<std::endl;
        }
        //---------------------reset device--------------------------------------
        ni660Xsl::SystemProperties::reset_device("Dev1");
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        
        count->release();
        finite_train->release();
        
    }catch(const ni660Xsl::DAQException &e)
    {
        std::cout<<"*************EXCEPTION**************"<<std::endl;
        for (int i = 0; i < e.errors.size(); i++) 
        {
            std::cout<<"- reason:\n \t"<< e.errors[i].reason.c_str()<<std::endl; 
            std::cout<<"- desc:\n"<< e.errors[i].desc.c_str()<<std::endl; 
            std::cout<<"- origin:\n \t"<< e.errors[i].origin.c_str()<<std::endl; 
            //	std::cout<<"- code:\n \t"<< e.errors[i].code<<std::endl;
            //	std::cout<<"- severity:\n \t"<< e.errors[i].severity<<std::endl; 
        }
        std::cout<<"************************************"<<std::endl;
    }
    
    if(count) 
    {
        delete count;
        count = 0;
    }
    if(finite_train)
    {
        delete finite_train;
        finite_train = 0;
    }

    return 0;
}
