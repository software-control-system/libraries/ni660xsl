// ============================================================================
//
// = CONTEXT
//    TANGO Project - NI660X Support Library
//      test Buffered event counting.
//
// = FILENAME
//   main.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================
#include <iostream>
#include <ace/ACE.h>

#include "BufferedEventCounting.h"
#include "Exception.h"

//===========================================================================================
//  class MyMeasurement
//===========================================================================================
class MyMeasurement: public ni660Xsl::BufferedEventCounting
{
public:
    MyMeasurement(void)
        :ni660Xsl::BufferedEventCounting(),
        overrun(0),
        timeout(0)
    {};
    virtual ~MyMeasurement(void)
    {};
    /**
    * count the number of overrun
    */
    void handle_data_lost(void)
    {
        overrun++;
        std::cout<<"overrun nb :"<<overrun<<std::endl;
    };
    /**
    * count the number of timeout
    */
    void handle_timeout(void)
    {
        timeout++;
        std::cout<<"timeout nb :"<<timeout<<std::endl;
        
    };
    void handle_raw_buffer(ni660Xsl::InRawBuffer* buffer, long& _samples_read)
    {
        std::cout<<"raw"<<std::endl;
        for (int j =0; j <buffer->depth(); j = j+10)
            std::cout<<"val["<<j<<"]: "<<(*buffer)[j]<<std::endl;
        delete buffer;
    };
    void handle_scaled_buffer(ni660Xsl::InScaledBuffer* buffer, long& _samples_read)
    {
        std::cout<<"scaled"<<std::endl;
        for (int j =0; j <buffer->depth(); j = j+10)
            std::cout<<"val["<<j<<"]: "<<(*buffer)[j]<<std::endl;
        delete buffer;
    };
private:
    int overrun;
    int timeout;
    
};
//===========================================================================================
//  main
//===========================================================================================
int main (int, char**) 
{
    MyMeasurement* count =  0; 
    
    try
    {
        std::cout<<"===================test buffered event counting 6602========================"<<std::endl;	
        std::cout<<"driver version: "<<ni660Xsl::SystemProperties::get_driver_version()<<std::endl;
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        std::cout<<"product type of Dev1:"<<ni660Xsl::SystemProperties::get_product_type("Dev1")<<std::endl;
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        std::cout<<"devices in the crate: "<<ni660Xsl::SystemProperties::get_all_devices_names()<<std::endl;
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        
        std::cout<<"============================================================================"<<std::endl;
        
        ni660Xsl::SystemProperties::reset_device("Dev1");
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        
        //---------------create a Buffered Event Counting operation----------------------
        count = new MyMeasurement();
        if(count == 0)
        {
            std::cout<<"out mem"<<std::endl;
        }
        
        //---------------config channel-------------------------------
        ni660Xsl::EventCountChan chan;
        
        chan.chan_name = "/Dev1/ctr0";
        chan.edge = ni::rising_edge;
        chan.initial_count = 0;
        chan.count_direction = ni::count_up;
        
        count->add_input_channel(chan);
        
        //---------------terminal count-------------------------------
        //count->set_terminal_count("/Dev1/PFI36", ni::toggle);
        //count->set_start_trigger("/Dev1/PFI9", ni::rising_edge);
        //---------------config timeout-------------------------------
        count->set_timeout(1.0);
        count->set_overrun_strategy(ni::abort);
        //---------------config buffer-------------------------------
        count->set_buffer_depth(20);
        
        //---------------config sample clk (gate of counter)--------------
        count->set_sample_clock("/Dev1/PFI38", ni::rising_edge, 15000000);
        
        //---------------init-------------------------------
        count->init();
        if(count->warn_occured())
        {
            std::cout<<count->get_warn_string()<<std::endl;
        }
        //---------------config hw-------------------------------
        count->configure();
        if(count->warn_occured())
        {
            std::cout<<count->get_warn_string()<<std::endl;
        }
        //---------------start acq-------------------------------
        count->start();
        if(count->warn_occured())
        {
            std::cout<<count->get_warn_string()<<std::endl;
        }
        
        //---------------read input data------------------------------        
        for (int i =0; i <100; i++)
        {
            count->get_raw_buffer();
            count->get_scaled_buffer();
            if(count->warn_occured())
            {
                std::cout<<count->get_warn_string()<<std::endl;
            }
            
        }
        //---------------stop-------------------------------
        count->stop();
        if(count->warn_occured())
        {
            std::cout<<count->get_warn_string()<<std::endl;
        }
        
        //---------------release hw-------------------------------
        count->release();  
        if(count->warn_occured())
        {
            std::cout<<count->get_warn_string()<<std::endl;
        }
        
    }catch(const ni660Xsl::DAQException &e)
    {
        std::cout<<"*************EXCEPTION**************"<<std::endl;
        for (int i = 0; i < e.errors.size(); i++) 
        {
            std::cout<<"- reason:\n \t"<< e.errors[i].reason.c_str()<<std::endl; 
            std::cout<<"- desc:\n"<< e.errors[i].desc.c_str()<<std::endl; 
            std::cout<<"- origin:\n \t"<< e.errors[i].origin.c_str()<<std::endl; 
            //	std::cout<<"- code:\n \t"<< e.errors[i].code<<std::endl;
            //	std::cout<<"- severity:\n \t"<< e.errors[i].severity<<std::endl; 
        }
        std::cout<<"************************************"<<std::endl;
        count->release();  
        if(count->warn_occured())
        {
            std::cout<<count->get_warn_string()<<std::endl;
        }
    }
    if (count)
    {
        delete count;
        count = 0;
    }
    return 0;
}
