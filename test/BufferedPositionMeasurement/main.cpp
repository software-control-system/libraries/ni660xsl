// ============================================================================
//
// = CONTEXT
//    TANGO Project - NI660X Support Library
//      test Buffered position measurement.
//
// = FILENAME
//   main.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================
#include <iostream>
#include <string>
#include <ace/ACE.h>
#include "Exception.h"
#include "BufferedPositionMeasurement.h"

//======================================================================================
//  class MyMeasurement
//======================================================================================
class MyMeasurement: public ni660Xsl::BufferedPositionMeasurement
{
public:
    MyMeasurement(void)
        :ni660Xsl::BufferedPositionMeasurement(),
        overrun(0),
        timeout(0)
    {};
    virtual ~MyMeasurement(void)
    {};
    void handle_data_lost(void)
    {
        overrun++;
        std::cout<<"overrun nb :"<<overrun<<std::endl;
    };
    void handle_timeout(void)
    {
        timeout++;
        std::cout<<"timeout nb :"<<timeout<<std::endl;
        
    };
    void handle_raw_buffer(ni660Xsl::InRawBuffer* buffer, long& _samples_read)
    {
        std::cout<<"raw"<<std::endl;
        for (int j =0; j <buffer->depth(); j = j+100)
            std::cout<<"val["<<j<<"]: "<<(*buffer)[j]<<std::endl;
        delete buffer;
    };
    void handle_scaled_buffer(ni660Xsl::InScaledBuffer* buffer, long& _samples_read)
    {
        std::cout<<"scaled"<<std::endl;
        for (int j =0; j <buffer->depth(); j = j+100)
            std::cout<<"val["<<j<<"]: "<<(*buffer)[j]<<std::endl;
        delete buffer;
    };
private:
    int overrun;
    int timeout;
    
};


//===========================================================================================
//  main
//===========================================================================================
int main (int, char**)
{    
    
    MyMeasurement* position = 0;
    
    try
    {
        std::cout<<"===================test buffered position 6602========================"<<std::endl;	
        std::cout<<"driver version: "<<ni660Xsl::SystemProperties::get_driver_version()<<std::endl;
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        std::cout<<"product type of Dev1:"<<ni660Xsl::SystemProperties::get_product_type("Dev1")<<std::endl;
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        std::cout<<"devices in the crate: "<<ni660Xsl::SystemProperties::get_all_devices_names()<<std::endl;
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        std::cout<<"============================================================================"<<std::endl;
        
        //---------------------reset device--------------------------------------
        ni660Xsl::SystemProperties::reset_device("Dev1");
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }


        position = new MyMeasurement();
        if(position == 0)
        {
            std::cout<<"out mem"<<std::endl;
        }
           
        
        //--------------------config chan----------------------------------------
        ni660Xsl::AngularEncoderChan chan;
        chan.chan_name = "/Dev1/ctr0";
        chan.decoding_type = ni::two_pulse;
        chan.initial_angle = 0.0;
        chan.pulse_per_revolution = 24;
        chan.z_idx_enable = false;
        chan.z_idx_phase = ni::AHigh_BLow;
        chan.z_idx_val = 0;
        chan.units = ni::degrees;
        
        ni660Xsl::LinearEncoderChan chan2;
        chan2.chan_name = "/Dev1/ctr0";
        chan2.decoding_type = ni::x1;
        chan2.distance_per_pulse = 1.0;
        chan2.initial_position =0;
        chan2.units = ni::meters;
        
        //position->add_linear_encoder(chan2);
        
        position->add_angular_encoder(chan);
        
        position->set_buffer_depth(1000);
        
        position->set_sample_clock("/Dev1/PFI33", ni::rising_edge, 150000000);
        
        position->set_overrun_strategy(ni::abort);
        
        position->set_timeout(1.0);
        
        //----------------optional-------------------------------------
        //position->set_start_trigger("/Dev1/PFI26", ni::rising_edge);
        position->set_data_tranfer_mechanism(ni::dma);
        //position->set_terminal_count("/Dev1/PFI36", ni::toggle);
        //position->set_read_mode(ni::interrupt);
        
        //------------------init task--------------------------------------------
        position->init();
        if(position->warn_occured())
        {
            std::cout<<position->get_warn_string()<<std::endl;
        }
        //------------------config hw -------------------------------------------
        position->configure();
        if(position->warn_occured())
        {
            std::cout<<position->get_warn_string()<<std::endl;
        }
        //------------------start measuring position--------------------------------
        position->start();
        if(position->warn_occured())
        {
            std::cout<<position->get_warn_string()<<std::endl;
        }
        //------------------read counter-------------------------------------------
        for (int i =0; i <10; i++)
        {
            position->get_raw_buffer();
            if(position->warn_occured())
            {
                std::cout<<position->get_warn_string()<<std::endl;
            }
        }		
        //-----------------stop task--------------------------------------------
        position->stop();
        if(position->warn_occured())
        {
            std::cout<<position->get_warn_string()<<std::endl;
        }
        
        position->release();
        
    }catch(const ni660Xsl::DAQException &e)
    {
        std::cout<<"*************EXCEPTION**************"<<std::endl;
        for (int i = 0; i < e.errors.size(); i++) 
        {
            std::cout<<"- reason:\n \t"<< e.errors[i].reason.c_str()<<std::endl; 
            std::cout<<"- desc:\n"<< e.errors[i].desc.c_str()<<std::endl; 
            std::cout<<"- origin:\n \t"<< e.errors[i].origin.c_str()<<std::endl; 
            //	std::cout<<"- code:\n \t"<< e.errors[i].code<<std::endl;
            //	std::cout<<"- severity:\n \t"<< e.errors[i].severity<<std::endl; 
        }
        std::cout<<"************************************"<<std::endl;
        position->release();
        
    }
    
    if(position)
    {
        delete position;
        position = 0;
    }
    
    return 0;
    
}
