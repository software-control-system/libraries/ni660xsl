// ============================================================================
//
// = CONTEXT
//    TANGO Project - NI660X Support Library
//      test Simple Position Measurement.
//
// = FILENAME
//   main.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================
#include <iostream>
#include <ace/ACE.h>
#include "SimplePositionMeasurement.h"
#include "Exception.h"

int main (int, char**)
{    
    
    ni660Xsl::SimplePositionMeasurement* position = 0;
    
    try
    {
        std::cout<<"===================test simple position 6602============================="<<std::endl;
    
    /*	std::cout<<"driver version: "<<ni660Xsl::SystemProperties::get_driver_version()<<std::endl;
    std::cout<<"product type of Dev1:"<<ni660Xsl::SystemProperties::get_product_type("Dev1")<<std::endl;
    std::cout<<"devices in the crate: "<<ni660Xsl::SystemProperties::get_all_devices_names()<<std::endl;
    
      std::cout<<"============================================================================"<<std::endl;
    */
        position = new ni660Xsl::SimplePositionMeasurement();
        if(position == 0)
        {
            std::cout<<"out mem"<<std::endl;
        }
        //---------------------reset device--------------------------------------
        //note: device must be reset to remove routes
        ni660Xsl::SystemProperties::reset_device("Dev1");
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        
        //-------------------config chan----------------------------------------
        ni660Xsl::AngularEncoderChan chan;
        chan.chan_name = "/Dev1/ctr0";
        chan.decoding_type = ni::x4;
        chan.initial_angle = 0.0;
        chan.pulse_per_revolution = 24;
        chan.z_idx_enable = false;
        chan.z_idx_phase = ni::AHigh_BHigh;
        chan.z_idx_val = 0;
        chan.units = ni::degrees;
        
        position->add_angular_encoder(chan);
        
        //--------------------optional-------------------------------------------
        //position->set_terminal_count("/Dev1/PFI36", ni::toggle);
        position->set_start_trigger("/Dev1/PFI33", ni::rising_edge);
        position->set_timeout(1.0);
        
        //------------------init task--------------------------------------------
        position->init();
        if(position->warn_occured())
        {
            std::cout<<position->get_warn_string()<<std::endl;
        }
        //------------------config hw -------------------------------------------
        position->configure();
        if(position->warn_occured())
        {
            std::cout<<position->get_warn_string()<<std::endl;
        }
        //------------------start measuring position--------------------------------
        position->start();
        if(position->warn_occured())
        {
            std::cout<<position->get_warn_string()<<std::endl;
        }
        //------------------read counter-------------------------------------------
        for (int i= 0; i<100; i++)
        {
            double p = position->get_current_scaled_value();
            if(position->warn_occured())
            {
                std::cout<<position->get_warn_string()<<std::endl;
            }
            std::cout<<"read: "<<p<<std::endl;
        }
        
        //-----------------stop task--------------------------------------------
        position->stop();
        if(position->warn_occured())
        {
            std::cout<<position->get_warn_string()<<std::endl;
        }
        
        position->release();
        
    }catch(const ni660Xsl::DAQException &e)
    {
        std::cout<<"*************EXCEPTION**************"<<std::endl;
        for (int i = 0; i < e.errors.size(); i++) 
        {
            std::cout<<"- reason:\n \t"<< e.errors[i].reason.c_str()<<std::endl; 
            std::cout<<"- desc:\n"<< e.errors[i].desc.c_str()<<std::endl; 
            std::cout<<"- origin:\n \t"<< e.errors[i].origin.c_str()<<std::endl; 
            //	std::cout<<"- code:\n \t"<< e.errors[i].code<<std::endl;
            //	std::cout<<"- severity:\n \t"<< e.errors[i].severity<<std::endl; 
        }
        std::cout<<"************************************"<<std::endl;
        position->release();
    }
    
    if(position)
    {
        delete position;
        position = 0;
    }
    
    return 0;
    
}
