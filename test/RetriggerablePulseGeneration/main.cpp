// ============================================================================
//
// = CONTEXT
//    TANGO Project - NI660X Support Library
//      test Retriggerable Pulse Generation.
//
// = FILENAME
//   main.cpp
//
// = AUTHORS
//    G.Abeille 
//
// ============================================================================
#include <iostream>
#include <string>

#include "NI660Xsl/Exception.h"
#include "NI660Xsl/RetriggerablePulseTrainGeneration.h"
//#include "SystemProperties.h"

void main()
{
    
    
    ni660Xsl::RetriggerablePulseTrainGeneration* retrig_train =  0;
    ni660Xsl::RetriggerablePulseTrainGeneration retrig_train2;
	try
    {
        std::cout<<"===================test 6602============================="<<std::endl;
        
        std::cout<<"driver version: "<<ni660Xsl::SystemProperties::get_driver_version()<<std::endl;
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        
        std::cout<<"product type of Dev1:"<<ni660Xsl::SystemProperties::get_product_type("Dev1")<<std::endl;
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        
        std::cout<<"devices in the crate: "<<ni660Xsl::SystemProperties::get_all_devices_names()<<std::endl;
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }
        
        std::cout<<"========================================================="<<std::endl;
        
        //--------------------reset the hw----------------------------------------
        std::cout<<"resetting hw..."<<std::endl;
		ni660Xsl::SystemProperties::reset_device("Dev1");
        if(ni660Xsl::SystemProperties::warn_occured())
        {
            std::cout<<ni660Xsl::SystemProperties::get_warn_string()<<std::endl;
        }

		std::cout<<"new train"<<std::endl;
        retrig_train = new ni660Xsl::RetriggerablePulseTrainGeneration();
        if(retrig_train == 0)
        {
            std::cout<<"out of memory"<<std::endl;
        }
        
        //-------------------config one channel------------------------------------------ 
        ni660Xsl::OutTimeChan chan;
        
        chan.chan_name = "/Dev1/ctr0";
        chan.idle_state = ni::low;
        chan.initial_delay = 0.0;//secs
        chan.low_time = 0.05;//secs
        chan.high_time = 0.05;//secs
        //chan.output_terminal = "/Dev1/PFI24";
     
        
		   std::cout<<"add channel"<<std::endl;
        retrig_train->add_time_channel(chan);
		 
        retrig_train->set_trigger_source("/Dev1/PFI38", ni::rising_edge);
        retrig_train->set_nb_pulses(1);
        
        //--------------------init, config, start hw------------------------------------
	    	std::cout<<"init"<<std::endl;
        retrig_train->init();
        if(retrig_train->warn_occured())
        {
            std::cout<<retrig_train->get_warn_string()<<std::endl;
        }
        retrig_train->configure(); 
        if(retrig_train->warn_occured())
        {
            std::cout<<retrig_train->get_warn_string()<<std::endl;
        }
	
        retrig_train->start();
        if(retrig_train->warn_occured())
        { 
            std::cout<<retrig_train->get_warn_string()<<std::endl;
        }
	
	

        //----------------------wait----------------------------------------------------
      
        for(int i=0;i<300000;i++)
          std::cout<<".";

        //----------------------change time values----------------------------------------------------
        chan.low_time = 0.02;//secs
        chan.high_time = 0.3;//secs
        std::cout<<"*******change_time_values***********"<<std::endl;
        retrig_train->change_time_values(chan);
        if(retrig_train->warn_occured())
        {
          std::cout<<retrig_train->get_warn_string()<<std::endl;
        }
         
        //----------------------wait----------------------------------------------------
        for(i=0;i<300000;i++)
          std::cout<<"*";
        
      
        //----------------------stop---------------------------------------------------
        retrig_train->stop();
        if(retrig_train->warn_occured())
        {
          std::cout<<"*************WARN**************"<<std::endl;
          std::cout<<retrig_train->get_warn_string()<<std::endl;
        }
        retrig_train->release();
        if(retrig_train->warn_occured())
        {
          std::cout<<retrig_train->get_warn_string()<<std::endl;
        }
        
        
    }catch(const ni660Xsl::DAQException &e)
    {
        std::cout<<"*************EXCEPTION**************"<<std::endl;
        for (int i = 0; i < e.errors.size(); i++) 
        {
            std::cout<<"- reason:\n \t"<< e.errors[i].reason.c_str()<<std::endl; 
            std::cout<<"- desc:\n"<< e.errors[i].desc.c_str()<<std::endl; 
            std::cout<<"- origin:\n \t"<< e.errors[i].origin.c_str()<<std::endl; 
            //	std::cout<<"- code:\n \t"<< e.errors[i].code<<std::endl;
            //	std::cout<<"- severity:\n \t"<< e.errors[i].severity<<std::endl; 
        }
        std::cout<<"************************************"<<std::endl;
        retrig_train->release();
        
    }
    
    /*if(retrig_train)
        delete retrig_train;*/
    
}//main
