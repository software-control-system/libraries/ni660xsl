# Microsoft Developer Studio Project File - Name="NI660Xsl_Shared" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=NI660Xsl_Shared - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "NI660Xsl_Shared.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "NI660Xsl_Shared.mak" CFG="NI660Xsl_Shared - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "NI660Xsl_Shared - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "NI660Xsl_Shared - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "NI660Xsl_Shared - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "NI660Xsl_Shared___Win32_Release"
# PROP BASE Intermediate_Dir "NI660Xsl_Shared___Win32_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "NI660Xsl\shared\release"
# PROP Intermediate_Dir "NI660Xsl\shared\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "NI660Xsl_BUILD" /D "NI660Xsl_DLL" /YX /FD /c
# ADD CPP /MD /W3 /GR /GX /Z7 /O2 /I "..\..\include" /I "..\..\src" /I "$(SOLEIL_ROOT)\hw-support\ace\include" /I "C:\Program Files\National Instruments\NI-DAQ\DAQmx ANSI C Dev\include" /D "ACE_HAS_DLL" /D "NDEBUG" /D "NI660XSL_HAS_DLL" /D "NI660XSL_BUILD" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "NI660Xsl_BUILD" /D "NI660Xsl_DLL" /D "_EVAL_PERF_" /FR /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40c /d "NDEBUG"
# ADD RSC /l 0x40c /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 nidaqmx.lib ace.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib /dll /pdb:"../../bin/msvc-6.0/libNI660Xsl.pdb" /machine:I386 /out:"../../bin/msvc-6.0/libNI660Xsl.dll" /implib:"../../lib/shared/msvc-6.0/libNI660Xsl.lib" /libpath:"$(SOLEIL_ROOT)\hw-support\ace\lib" /libpath:"C:\Program Files\National Instruments\NI-DAQ\DAQmx ANSI C Dev\lib\msvc"
# SUBTRACT LINK32 /verbose /pdb:none

!ELSEIF  "$(CFG)" == "NI660Xsl_Shared - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "NI660Xsl\shared\debug"
# PROP Intermediate_Dir "NI660Xsl\shared\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "NI660Xsl_BUILD" /D "NI660Xsl_DLL" /D "NI660Xsl_ENABLE_LOG" /YX /FD /GZ /c
# ADD CPP /MDd /W3 /GR /GX /Z7 /Od /I "..\..\include" /I "..\..\src" /I "$(SOLEIL_ROOT)\hw-support\ace\include" /I "C:\Program Files\National Instruments\NI-DAQ\DAQmx ANSI C Dev\include" /D "ACE_HAS_DLL" /D "_DEBUG" /D "NI660Xsl_ENABLE_LOG" /D "NI660XSL_HAS_DLL" /D "NI660XSL_BUILD" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "NI660Xsl_BUILD" /D "NI660Xsl_DLL" /D "_EVAL_PERF_" /FR /FD /GZ /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40c /d "_DEBUG"
# ADD RSC /l 0x40c /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 nidaqmx.lib ace.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib /dll /incremental:no /pdb:"../../bin/msvc-6.0/libNI660Xsld.pdb" /debug /machine:I386 /out:"../../bin/msvc-6.0/libNI660Xsld.dll" /implib:"../../lib/shared/msvc-6.0/libNI660Xsld.lib" /pdbtype:sept /libpath:"$(SOLEIL_ROOT)\hw-support\ace\lib" /libpath:"C:\Program Files\National Instruments\NI-DAQ\DAQmx ANSI C Dev\lib\msvc"
# SUBTRACT LINK32 /verbose /pdb:none

!ENDIF 

# Begin Target

# Name "NI660Xsl_Shared - Win32 Release"
# Name "NI660Xsl_Shared - Win32 Debug"
# Begin Group "src"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\src\BufferedAcquisition.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\BufferedEventCounting.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\BufferedPositionMeasurement.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\ContinuousPulseTrainGeneration.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\CounterBasedOperation.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\Data.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\EventCounting.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\Exception.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\FinitePulseTrainGeneration.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\InputOperation.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\OutputOperation.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\PositionMeasurement.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\PulseTrainGeneration.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\RetriggerablePulseTrainGeneration.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\SimpleEventCounting.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\SimplePositionMeasurement.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\SystemProperties.cpp
# End Source File
# Begin Source File

SOURCE=..\..\src\Thread.cpp
# End Source File
# End Group
# Begin Group "include"

# PROP Default_Filter ""
# Begin Group "NI660Xsl"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\include\NI660Xsl\BufferedAcquisition.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\BufferedEventCounting.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\BufferedPositionMeasurement.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\Channels.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\ContinuousPulseTrainGeneration.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\CounterBasedOperation.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\Data.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\Data_T.cpp
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\Data_T.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\EventCounting.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\Exception.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\Export.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\FinitePulseTrainGeneration.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\InData.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\InputOperation.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\NI.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\NI660XSLConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\NI660XslExceptionsHandler.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\OutputOperation.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\PositionMeasurement.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\PulseTrainGeneration.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\RetriggerablePulseTrainGeneration.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\SimpleEventCounting.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\SimplePositionMeasurement.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\SystemProperties.h
# End Source File
# Begin Source File

SOURCE=..\..\include\NI660Xsl\Thread.h
# End Source File
# End Group
# End Group
# End Target
# End Project
