
The html documentation must be generated with doxygen (www.doxygen.org).



To generate the documentation:

- Open the file doxygen_config with doxywizard and put the paths for input (tab Input) and ouput (tab Project) files 
corresponding to your local paths.

- click on Run.